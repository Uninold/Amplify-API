<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('encrypt'))
{
	function encrypt($string)
	{
		$ci = &get_instance();
		$ci->load->library('MY_Encrypt');
		return $ci->my_encrypt->encode($string);
	}
}

if(!function_exists('decrypt'))
{
	function decrypt($string)
	{
		$ci = &get_instance();
		$ci->load->library('MY_Encrypt');
		return $ci->my_encrypt->decode($string);
	}
}

if(!function_exists('print_me'))
{
	function print_me($arr)
	{
		echo "<pre>"; 
		if(is_array($arr))
		{
			print_r($arr);
		}else
		{
			echo $arr;
		}
		echo "</pre>";
	}
}

if(!function_exists('profile_picture'))
{
	function profile_picture($filename)
	{
		if($filename!="")
		{
			$ci = & get_instance();
			if(file_exists($ci->config->item("profile_picture_path").$filename))
			{
				$filename = base_url("uploads/profile/".$filename);
			}
		}
		return $filename;
	}
}


if(!function_exists('get_curl_json'))
{
	function get_curl_json($url)
	{
		$curl = curl_init($url); 
		curl_setopt ($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_URL, $url);
		$response = curl_exec($curl);
		curl_close($curl);
		return json_decode($response,false);
	}
}




if(!function_exists('curl_download'))
{
	function curl_download($Url)
	{
	    if (!function_exists('curl_init'))
	    {
	        die('cURL is not installed. Install and try again.');
	    }
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $Url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $output = curl_exec($ch);
	    curl_close($ch);
	  
	    return $output;
	}
}

if(!function_exists('profile_picture'))
{
	function profile_picture($filename)
	{
		if($filename!="")
		{
			$ci = & get_instance();
			if(file_exists($ci->config->item("profile_picture_path").$filename))
			{
				$filename = base_url("uploads/profile/".$filename);
			}
		}
		return $filename;
	}
}

//turn base64 to image
if (!function_exists('base64ToImage')) {
    function base64ToImage($imageData)
    {
        list($type, $imageData) = explode(';', $imageData);
        list(, $extension) = explode('/', $type);
        list(, $imageData)  = explode(',', $imageData);
        $fileName = uniqid().'.'.$extension;
        if ($imageData = base64_decode($imageData)) {
            return array(
                "file_name" => $fileName,
                "data" => $imageData
            );
        }
        return false;
    }
}





