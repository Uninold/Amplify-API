<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'libraries/JWT.php';

class Authorization
{
    public static function validateToken()
    {
    	$response = array(
    		"error" => 1,
            "message" => ""
    	);

    	$CI =& get_instance();

        $headers = $CI->input->request_headers();
        if(isset($headers['authorization'])){
            $headers['Authorization'] = $headers['authorization'];
            unset($headers['authorization']);
        }
        if((array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])))
        {
        	$token = self::getBearerToken($headers['Authorization']);
	        $key = $CI->config->item('jwt_key');
	        $algorithm = $CI->config->item('jwt_algorithm');
	        
	        try{
	        	$response['error'] = 0;
	        	$response['token'] = JWT::decode($token, $key, array($algorithm));
	        }catch(InvalidArgumentException $e)
	        {
                $response['error'] = 1;
	        	$response['message'] = $e->getMessage();
	        }catch(UnexpectedValueException $e)
            {
                $response['error'] = 1;
                $response['message'] = $e->getMessage();
            }catch(SignatureInvalidException $e)
            {
                $response['error'] = 1;
                $response['message'] = $e->getMessage();
            }catch(BeforeValidException $e)
            {
                $response['error'] = 1;
                $response['message'] = $e->getMessage();
            }catch(ExpiredException $e)
            {
                $response['error'] = 1;
                $response['message'] = $e->getMessage();
            }
        }else
        {
        	$response['message'] = 'Authorization header not set';

        }
        return (array)$response;
    }

    public static function getBearerToken($authorization) 
    {
        if(!empty($authorization)) 
        {
            if(preg_match('/Bearer\s(\S+)/', $authorization, $matches)) 
            {
                return $matches[1];
            }
        }
        return null;
    }

    public static function generateToken($data)
    {
        $CI =& get_instance();
        $key = $CI->config->item('jwt_key');
        $algorithm = $CI->config->item('jwt_algorithm');
        return JWT::encode($data, $key);
    }
}
