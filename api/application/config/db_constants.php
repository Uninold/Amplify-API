<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**********************************************
*  DATABASE TABLE CONSTANTS
***********************************************/

define('ACTIVITY_LOG_BUSINESSMAN', 'activity_log_businessman');
define('ACCEPTED_INTEREST', 'accepted_interest');
define('ADMIN', 'admin');
define('BUSINESSMAN', 'businessman');
define('BUSINESSMAN_RATING', 'businessman_rating');
define('YOUTUBER', 'youtuber');
define('MEDIAKIT', 'mediakit');
define('MEDIAKIT_INTEREST', 'mediakit_interest');
define('NOTIFICATION', 'notification');
define('CAMPAIGN', 'campaign');
define('CAMPAIGN_PHOTO', 'campaign_photo');
define('CAMPAIGN_STATUS', 'campaign_status');
define('CATEGORY', 'category');
define('ADVERTISING_DETAILS', 'advertising_details');
define('YOUTUBER_INTEREST', 'youtuber_interest');
define('YOUTUBE_DATA', 'youtube_data');
define('VIDEO', 'video');
define('YOUTUBER_CAMPAIGN', 'youtuber_campaign');
define('SUBMISSION_STATUS', 'submission_status');
define('VIDEO_STATISTICS', 'video_statistics');
define('RECOMMENDATION', 'recommendation');
define('CRITERIA', 'criteria');
define('YOUTUBER_REFER', 'youtuber_refer');


// For Presentation
define('V2_ACCEPTED_INTEREST', 'v2_accepted_interest');
define('V2_BUSINESSMAN_RATING', 'v2_businessman_rating');
define('V2_VIDEO', 'v2_video');
define('V2_YOUTUBE_DATA', 'v2_youtube_data');

