<?php defined('BASEPATH') OR exit('No direct script access allowed');

$config['jwt_key'] = hex2bin('d86f8b2f940f22a2d2c829ef806e68ba9ecb62f41aad4c58ad5c480cd9844057');

$config['jwt_algorithm'] = 'HS256';
