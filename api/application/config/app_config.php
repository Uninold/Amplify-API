<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**********************************************
*  GENERAL CONFIGURATIONS
***********************************************/

$config['app_name']				= "Amplify";
$config['app_description'] 		= "";
$config['app_version'] 			= "2.0.0";
$config['timezone']				= "Asia/Manila";

/* Google API Keys */
/*
*  LOCAL
*/
$config['geocode_api_key']      = "AIzaSyAu0y98M0U0E2n_zTrWLi3l0kQFkrsoCtk";
$config['timezone_api_key']     = "AIzaSyAE4XNjMl6twWw6da8xnMjGz9HDQ2Y27tQ";
$config['map_api_key']     		= "AIzaSyDJZsOZrnP-TuCYYwy8a7xZ05IiU-s0394";
/*
*  LIVE
*/
// $config['geocode_api_key']      = "AIzaSyBJmkzZmd2Lop3Hmf6Y1zFDhX-RGIkmqd4";
// $config['timezone_api_key']     = "AIzaSyDPpXvJuRmu1Ikz1XRMbmepiuEWbfp1JU0";
// $config['map_api_key']     		= "AIzaSyC0tH9AEUn39N4BJ3ZRDUGUhr75h8pjcSc";

/* Upload Paths */
$config['profile_picture_path'] = "./uploads/profile/";

/* Profile Picture Upload */
$config['profile_pic_upload'] 	= array(
	"upload_path" 	=> "./uploads/profile/",
	"allowed_types" => "gif|png|jpg|jpeg",
	"max_size" 		=> 100,
	"max_width" 	=> 1024,
	"max_height" 	=> 768
);
