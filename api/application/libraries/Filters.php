<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Filters {

    public $CI;

    public function __construct() 
    {
        $this->CI =& get_instance();
    }
    
    public function detect($model,$filter_array=array(),$is_module=false)
    {
        if($model!=false && $filter_array!=false && !empty($filter_array))
        {
            $model       = ucfirst($model);
            $filter_path = APPPATH."models/filters/".$model.".php";
            $module_name = "";
            $filter_name = $model;
            if($is_module)
            {
                $exploded_model = array();
                $exploded_model = explode('/',$model);
                $filter_path    = APPPATH."modules/".$exploded_model[0]."/models/filters/".ucfirst($exploded_model[1]).".php";
                $module_name    = $exploded_model[0]."/";
                $filter_name    = $exploded_model[1];
            }
            if(file_exists($filter_path))
            {
                if(!empty($filter_array) && is_array($filter_array))
                {
                    $this->CI->load->model($module_name."filters/".$filter_name);
                    foreach ($filter_array as $method_name => $method_value) 
                    {
                        if(method_exists($this->CI->$filter_name, $method_name))
                        {
                            $this->CI->$filter_name->$method_name($method_value);
                        }
                    }
                }
            }
        }
    }
}