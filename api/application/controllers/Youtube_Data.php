<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';

class Youtube_Data extends REST_Controller {

    var $response_data = array();

    public function __construct()
    {
        parent::__construct();

        // init response data
        $this->response_data = array(
            'error' => 1,
            'data'  => array(),
            'message' => 'Failed on processing.'
        );

        $this->load->model('mdyoutube_data');
    }

    public function add_sync_youtube_data_post()
    {
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $_POST = (array)json_decode($post_json, TRUE);
            $post_data = $this->input->post();

            // Validate post data
            $this->load->library("form_validation");

            $this->form_validation->set_rules("videoId","VideoId","required");
            $this->form_validation->set_rules("category_id","CategoryId","required");

            if($this->form_validation->run())
            {
                $this->load->library("bcrypt");
                //insert data_user_businessman
                $youtube_data = array(
                            "videoId"           => $post_data['videoId'],
                            "category_id"       => $post_data['category_id'],
                            "likeCount"         => $post_data['likeCount'],
                            "dislikeCount"      => $post_data['dislikeCount'],
                            "commentCount"      => $post_data['commentCount'],
                            "viewCount"         => $post_data['viewCount']
                        );

                $this->mdyoutube_data->sync_youtube_data($youtube_data);
                $this->response_data['error']   = 0;
                $this->response_data['message'] = 'Youtube Data has been successfully synced!'; 
            } else{
                $this->response_data['message'] = validation_errors();
            }

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function add_youtuber_video_post() // Video entry
    {
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $_POST = (array)json_decode($post_json, TRUE);
            $post_data = $this->input->post();

            // Validate post data
            $this->load->library("form_validation");

            $this->form_validation->set_rules("video_id","VideoId","required");
            $this->form_validation->set_rules("category_id","Category","required");
            $this->form_validation->set_rules("channel_id","Channel","required");
            $this->form_validation->set_rules("video_title","Title","required");
            $this->form_validation->set_rules("comments_count","Comment_coung","required");
            $this->form_validation->set_rules("dislikes_count","Dislike_count","required");
            $this->form_validation->set_rules("likes_count","Like_count","required");
            $this->form_validation->set_rules("views_count","View_count","required");
            $this->form_validation->set_rules("youtuber_id","Youtuber_id","required");

            if($this->form_validation->run())
            {
                $youtube_data = array(
                            "video_id"          => $post_data['video_id'],
                            "video_title"       => $post_data['video_title'],
                            "video_description" => $post_data['video_description'],
                            "channel_id"        => $post_data['channel_id'],
                            "channel_title"     => $post_data['channel_title'],
                            "category_id"       => $post_data['category_id'],
                            "default_thumbnail" => $post_data['default_thumbnail'],
                            "views_count"       => $post_data['views_count'],
                            "likes_count"       => $post_data['likes_count'],
                            "dislikes_count"    => $post_data['dislikes_count'],
                            "comments_count"    => $post_data['comments_count'],
                            "youtuber_id"       => $post_data['youtuber_id']
                        );

                $this->response_data['data'] = $this->mdyoutube_data->add_youtube_video($youtube_data);
                $this->response_data['error']   = 0;
                $this->response_data['message'] = 'Youtuber Video has been successfully added!';
            } else{
                $this->response_data['message'] = validation_errors();
            }

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        return;
    }

    public function youtuber_channel_existence_get($youtube_channel)
    {
        if($this->input->is_ajax_request())
        {
            $existed = $this->mdyoutube_data->get_youtube_channel_existence($youtube_channel);

            if($existed) {
                $this->response_data['data'] = 1;
            } else {
                $this->response_data['data'] = 0;
            }

            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Existence';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
    
        } else {
            $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
            return;
        }    
    }

    public function add_youtube_data_post() {
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $_POST = (array)json_decode($post_json, TRUE);
            $post_data = $this->input->post();

            // Validate post data
            $this->load->library("form_validation");

            $this->form_validation->set_rules("video_id","Video ID","required");

            if($this->form_validation->run())
            {
                $this->load->library("bcrypt");
                //insert data_user_businessman
                $youtube_data = array(
                            "video_id"      => $post_data['video_id'],
                            "likes"         => $post_data['likeCount'],
                            "dislikes"      => $post_data['dislikeCount'],
                            "views"         => $post_data['viewCount'],
                            "date_added"    => date("Y-m-d")
                        );

                $this->mdyoutube_data->youtube_data($youtube_data);
                $this->response_data['error']   = 0;
                $this->response_data['message'] = 'Youtube Data has been successfully synced!'; 
            } else{
                $this->response_data['message'] = validation_errors();
            }

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function update_youtuber_video_put($video_id)
    {
        $auth_response = Authorization::validateToken();
        if($auth_response['error'] == 0)
        {
            $token = (array) $auth_response['token'];
            $put_data = $this->put();

            $data_youtube_video = array(
                "views_count"       => $put_data['views_count'],
                "likes_count"       => $put_data['likes_count'],
                "dislikes_count"    => $put_data['dislikes_count'],
                "comments_count"    => $put_data['comments_count']
            );

            $this->mdyoutube_data->update_youtuber_video($video_id, $data_youtube_video);
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Youtube video sucessfully updated';
            $this->response($this->response_data, REST_Controller::HTTP_OK);
        }
    }

    public function remove_all_video_youtube_data_delete() {
        if($this->input->is_ajax_request())
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {
                $data_from_post = (array)$auth_response['token'];

                $data_youtube_data = $this->mdyoutube_data->remove_youtube_data();

                $this->response_data['error'] = 0;
                $this->response_data['message'] = 'Youtube Data Successfully Removed';

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                return;
            }else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function youtube_data_video_existed_get($video_id) {
        $video = $this->mdyoutube_data->get_video_existed($video_id);

        if(!empty($video)) {
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Video existed already.';
        } else {
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Video can be uploaded, video did not exist yet.';
        }

        $this->response($this->response_data, REST_Controller::HTTP_OK);
        return;
    }
}
