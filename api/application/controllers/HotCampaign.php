<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';

class HotCampaign extends REST_Controller {

    var $response_data = array();

    public function __construct()
    {
        parent::__construct();

        // init response data
        $this->response_data = array(
            'error' => 1,
            'data'  => array(),
            'message' => 'Failed on processing.'
        );

        $this->load->model('mdhotcampaign');
        $this->load->model('mdinterest');
    }

    public function display_hot_campaigns_get($youtuber_id)
    {
        $data_campaign = $this->mdhotcampaign->get_hot_campaigns();
        $data_final = array();

        if(!empty($data_campaign)) {
            foreach($data_campaign as $inside_key => $inside_value) {
                $accepted_interest_in_campaign = $this->mdhotcampaign->accepted_interest_count((int)$inside_value['campaign_id']);
                // Calculates current_advertiser is greater than 80% of advertiser_needed to a campaign
                if((int)$accepted_interest_in_campaign['count_interested'] >= ((int)$inside_value['advertiser_needed'] * 0.8)) {
                    array_push($data_final, $inside_value);
                }
            }
        }

        foreach($data_final as $in_key => $in_value) {
            $data_final[$in_key]['photo'] = profile_picture($in_value['photo']);
            $accepted = $this->mdinterest->interested_get($in_value['campaign_id'], $youtuber_id);
            if(!empty($accepted)) {
                $data_final[$in_key]['interested'] = 1;
            } else {
                $data_final[$in_key]['interested'] = 0;
            }
            $accepted_interest = $this->mdinterest->accepted_interest_get($in_value['campaign_id'], $youtuber_id);
            if(!empty($accepted_interest)){ // Interest has been accepted by a businessman
                $data_final[$in_key]['accepted'] = 'yes';
            } else {
                $data_final[$in_key]['accepted'] = 'no';
            }
        }

        $this->response_data['data'] = $data_final;
        $this->response_data['error'] = 0;
        $this->response_data['message'] = 'List of Hot Campaigns';
        
        $this->response($this->response_data);
        return;
    }

}
