<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';

class Campaign extends REST_Controller {

    var $response_data = array();

    public function __construct()
    {
        parent::__construct();

        // init response data
        $this->response_data = array(
            'error' => 1,
            'data'  => array(),
            'message' => 'Failed on processing.'
        );

        $this->load->model('mdcampaign');
        $this->load->model('mdbusinessman');
        $this->load->model('mdyoutuber');
        $this->load->model('mdyoutube_data');
        $this->load->model('mdcampaign_status');
        $this->load->model('mdmediakit');
        $this->load->model('mdinterest');
    }

    public function add_campaign_post() // Adding of campaign
    {   
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {
                $_POST = (array)json_decode($post_json, TRUE);
                $post_data = $this->input->post();

                // Validate post data
                $this->load->library("form_validation");

                $this->form_validation->set_rules("project_name","Project_name","required");
                $this->form_validation->set_rules("campaign_deadline","Campaign_Deadline","required");
                $this->form_validation->set_rules("production_deadline","Production_Deadline","required");

                if($this->form_validation->run())
                {
                        $data_campaign = array(
                                    "businessman_id"        => $post_data['businessman_id'],
                                    "project_name"          => $post_data['project_name'],
                                    "starting_budget"       => $post_data['starting_budget'],
                                    "ending_budget"         => $post_data['ending_budget'],
                                    "project_description"   => $post_data['project_description'],
                                    "campaign_deadline"     => $post_data['campaign_deadline'],
                                    "production_deadline"   => $post_data['production_deadline'],
                                    "video_duration"        => $post_data['video_duration'],
                                    "advertiser_needed"     => $post_data['advertiser_needed'],
                                    "category_id"           => $post_data['category_id'],
                                    "campaign_status_id"    => $post_data['campaign_status_id'],
                                    "date_added"            => date("Y-m-d H:i:s")
                                );

                        $this->mdcampaign->post_campaign($data_campaign);
                        $this->response_data['error']   = 0;
                        $this->response_data['data'] = $this->mdcampaign->get_specific_campaign_project_name($data_campaign['project_name']);
                        $this->response_data['message'] = 'Product Information added!';
                    
                } else {
                    $this->response_data['message'] = validation_errors();
                }

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                return;
            } else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function add_product_photo_campaign_post() // Adds product photos to a campaign
    {   
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {
                $_POST = (array)json_decode($post_json, TRUE);
                $post_data = $this->input->post();

                // Validate post data
                $this->load->library("form_validation");

                $this->form_validation->set_rules("campaign_id","Campaign_id","required");
                $this->form_validation->set_rules("photo","Photo","required");

                if($this->form_validation->run())
                {
                    $campaign_photo = array(
                        "campaign_id"   => $post_data['campaign_id']
                    );

                    // check if there is photo
                    if (isset($post_data['photo']) && !empty($post_data['photo'])) {
                        $profile_data = base64ToImage("data:".$post_data['photo']);
                        if($profile_data) {
                            $profile_path = './uploads/profile/';
                            if (file_put_contents($profile_path.$profile_data['file_name'], $profile_data['data'])) {
                                $campaign_photo['photo'] = $profile_data['file_name'];
                            }
                        }
                    }

                    $this->mdcampaign->post_product_photo_campaign($campaign_photo);
                    $this->response_data['error']   = 0;
                    $this->response_data['message'] = 'Photo added!';
                    
                } else {
                    $this->response_data['message'] = validation_errors();
                }

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                return;
            } else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function add_advertising_detail_post() // Add advertising details to a campaign
    {   
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {
                $_POST = (array)json_decode($post_json, TRUE);
                $post_data = $this->input->post();

                // Validate post data
                $this->load->library("form_validation");

                $this->form_validation->set_rules("detail","Detail","required");
                $this->form_validation->set_rules("campaign_id","Campaign_id","required");

                if($this->form_validation->run())
                {
                        $advertising_detail = array(
                                    "detail" => $post_data['detail'],
                                    "campaign_id" => $post_data['campaign_id']
                                );

                        $this->mdcampaign->post_advertising_detail($advertising_detail);
                        $this->response_data['error'] = 0;
                        $this->response_data['message'] = 'Product Advertising Detail added!';
                    
                } else {
                    $this->response_data['message'] = validation_errors();
                }

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                return;
            } else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function remove_campaign_delete($campaign_id)
    {
        $auth_response = Authorization::validateToken();
        if($auth_response['error']==0)
        {
            $token = (array)$auth_response['token'];
    
            $data_campaign = $this->mdcampaign->remove_campaign($campaign_id);

            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Campaign Successfully Removed';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        } else {
            $this->response_data['message'] = $auth_response['message'];
        }
        return;
    }

    public function remove_photo_delete($campaign_photo_id) {
        $auth_response = Authorization::validateToken();
        if($auth_response['error']==0)
        {
            $token = (array)$auth_response['token'];
    
            $data_campaign = $this->mdcampaign->remove_campaign_photo($campaign_photo_id);

            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Campaign Photo Successfully Removed';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        } else {
            $this->response_data['message'] = $auth_response['message'];
        }
        return;
    }

    public function remove_advertising_delete($advertising_detail_id) {
        $auth_response = Authorization::validateToken();
        if($auth_response['error']==0)
        {
            $token = (array)$auth_response['token'];
    
            $data_campaign = $this->mdcampaign->remove_advertising_detail($advertising_detail_id);

            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Campaign Detail Successfully Removed';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        } else {
            $this->response_data['message'] = $auth_response['message'];
        }
        return;
    }

    public function add_to_on_going_production_put($campaign_id) {
        $auth_response = Authorization::validateToken();
        if($auth_response['error'] == 0)
        {
            $token = (array) $auth_response['token'];
            $put_data = $this->put();

            $data_campaign = array(
                "campaign_status_id"    => 2
            );

            $this->mdcampaign->add_to_on_going_production($campaign_id, $data_campaign);
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Campaign to on going production sucessfully set.';
            $this->response($this->response_data, REST_Controller::HTTP_OK);
        }
    }

    public function update_campaign_put($campaign_id)
    {
        $auth_response = Authorization::validateToken();
        if($auth_response['error'] == 0)
        {
            $token = (array) $auth_response['token'];
            $put_data = $this->put();

            $data_campaign = array(
                "businessman_id"        => $put_data['businessman_id'],
                "project_name"          => $put_data['project_name'],
                "starting_budget"       => $put_data['starting_budget'],
                "ending_budget"         => $put_data['ending_budget'],
                "project_description"   => $put_data['project_description'],
                "campaign_deadline"     => $put_data['campaign_deadline'],
                "production_deadline"   => $put_data['production_deadline'],
                "video_duration"        => $put_data['video_duration'],
                "advertiser_needed"     => $put_data['advertiser_needed'],
                "category_id"           => $put_data['category_id'],
                "campaign_status_id"    => $put_data['campaign_status_id']
            );

            $this->mdcampaign->update_campaign($campaign_id, $data_campaign);
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Campaign sucessfully updated';
            $this->response($this->response_data, REST_Controller::HTTP_OK);
        }
    }

    public function all_campaigns_businessman_get($businessman_id) // All businessman campaigns of a specific businessman with photo and advertising details also has category
    {
        if($this->input->is_ajax_request())
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {
                $token = (array)$auth_response['token'];
    
                $data_campaign = $this->mdcampaign->get_all_campaigns_businessman($businessman_id);

                if(!empty($data_campaign)) {
                    foreach($data_campaign as $inside_key => $inside_value) {
                        $photos_campaign = $this->mdcampaign->get_campaign_photos($inside_value['campaign_id']);
                        $data_campaign[$inside_key]['photo'] = profile_picture($photos_campaign['photo']);
                        $data_campaign[$inside_key]['advertising_details'] = $this->mdcampaign->get_advertising_details($inside_value['campaign_id']);
                    }
                }

                $this->response_data['data'] = $data_campaign;
                $this->response_data['error'] = 0;
                $this->response_data['message'] = 'List of Campaigns of Businessman';

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                 return;
            } else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
         return;
    }

    public function all_on_going_campaigns_businessman_get($businessman_id) // All businessman campaigns of a specific businessman with photo and advertising details also has category
    {
        if($this->input->is_ajax_request())
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {
                $token = (array)$auth_response['token'];
    
                $data_campaign = $this->mdcampaign_status->get_all_on_going_campaign_businessman_status(1,$businessman_id); // On Going Campaign

                if(!empty($data_campaign)) {
                    foreach ($data_campaign as $inside_key => $inside_value) {
                        $photo_campaign = $this->mdcampaign->get_single_photo_display_feed((int)$inside_value['campaign_id']);
                        $data_campaign[$inside_key]['photo'] = profile_picture($photo_campaign['photo']);
                        $count = $this->mdcampaign->get_current_no_advertisers((int)$inside_value['campaign_id']);
                        $data_campaign[$inside_key]['count_youtuber'] = $count['count_youtuber'];
                    }
                }

                $this->response_data['data'] = $data_campaign;
                $this->response_data['error'] = 0;
                $this->response_data['message'] = 'List of All On Going Campaigns of a Businessman';

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                 return;
            } else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
         return;
    }

    public function all_on_going_production_businessman_get($businessman_id) // All businessman production of a specific businessman with photo and advertising details also has category
    {
        if($this->input->is_ajax_request())
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {
                $token = (array)$auth_response['token'];
    
                $data_campaign = $this->mdcampaign_status->get_all_on_going_production_businessman_status(2,$businessman_id); // On Going Campaign

                if(!empty($data_campaign)) {
                    foreach ($data_campaign as $inside_key => $inside_value) {
                        $photo_campaign = $this->mdcampaign->get_single_photo_display_feed((int)$inside_value['campaign_id']);
                        $data_campaign[$inside_key]['photo'] = profile_picture($photo_campaign['photo']);
                        $data_campaign[$inside_key]['advertisers'] = $this->mdyoutuber->get_all_youtuber_on_going_production((int)$inside_value['campaign_id']);
                        $count = $this->mdcampaign->get_current_accepted_advertisers((int)$inside_value['campaign_id']);
                        $data_campaign[$inside_key]['count_youtuber'] = $count['count_youtuber'];
                    }
                }

                $this->response_data['data'] = $data_campaign;
                $this->response_data['error'] = 0;
                $this->response_data['message'] = 'List of All On Going Production of a Businessman';

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                 return;
            } else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
         return;
    }

    public function specific_on_going_campaign_businessman_with_advertisers_get($campaign_id) // All businessman campaigns of a specific businessman with photo and advertising details also has category
    {
        if($this->input->is_ajax_request())
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {
                $token = (array)$auth_response['token'];
    
                $data_campaign = $this->mdcampaign_status->get_specific_on_going_campaign_businessman_with_advertisers(1,$campaign_id); // On Going Campaign with Advertisers

                if(!empty($data_campaign)) {
                    foreach($data_campaign as $inside_key => $inside_value) {
                        // $photo_campaign = $this->mdcampaign->get_single_photo_display_feed((int)$inside_value['campaign_id']);
                        // $data_campaign[$inside_key]['photo'] = profile_picture($photo_campaign['photo']);
                        $data_campaign[$inside_key]['photos'] = $this->mdcampaign->get_photo((int)$inside_value['campaign_id']);
                        if(!empty($data_campaign[$inside_key]['photos'])) {
                            foreach($data_campaign[$inside_key]['photos'] as $in_key => $in_value) {
                                $data_campaign[$inside_key]['photos'][$in_key]['photo'] = profile_picture($in_value['photo']);
                            }
                        }
                        $data_campaign[$inside_key]['advertising_details'] = $this->mdcampaign->get_advertising_details((int)$inside_value['campaign_id']);
                        $accepted_advertiser = $this->mdcampaign_status->get_accepted_advertisers((int)$inside_value['campaign_id']);
                        $data_campaign[$inside_key]['accepted_advertiser_count'] = (int)$accepted_advertiser['accepted_advertiser_count'];
                        $data_campaign[$inside_key]['advertisers'] = $this->mdinterest->get_interested_youtuber((int)$inside_value['campaign_id']);
                        if(!empty($data_campaign[$inside_key]['advertisers'])) {
                            foreach($data_campaign[$inside_key]['advertisers'] as $in_key => $in_value) {
                                $accepted = $this->mdinterest->accepted_interest_get($campaign_id, $in_value['youtuber_id']);
                                if(!empty($accepted)) {
                                    $data_campaign[$inside_key]['advertisers'][$in_key]['interested'] = 1;
                                } else {
                                    $data_campaign[$inside_key]['advertisers'][$in_key]['interested'] = 0;
                                }
                            }
                        }
                        $count = $this->mdcampaign->get_current_no_advertisers((int)$inside_value['campaign_id']);
                        $data_campaign[$inside_key]['count_youtuber'] = $count['count_youtuber'];
                    }
                }

                $this->response_data['data'] = $data_campaign;
                $this->response_data['error'] = 0;
                $this->response_data['message'] = 'Specific On Going Campaign of a Businessman with Advertisers';

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                 return;
            } else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
         return;
    }

    public function specific_on_going_production_businessman_with_advertisers_get($campaign_id) // All businessman campaigns of a specific businessman with photo and advertising details also has category
    {
        if($this->input->is_ajax_request())
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {
                $token = (array)$auth_response['token'];
    
                $data_campaign = $this->mdcampaign_status->get_specific_on_going_production_businessman_with_advertisers(2,$campaign_id); // On Going Production with Advertisers

                if(!empty($data_campaign)) {
                    foreach($data_campaign as $inside_key => $inside_value) {
                        $data_campaign[$inside_key]['photos'] = $this->mdcampaign->get_photo((int)$inside_value['campaign_id']);
                        if(!empty($data_campaign[$inside_key]['photos'])) {
                            foreach($data_campaign[$inside_key]['photos'] as $in_key => $in_value) {
                                $data_campaign[$inside_key]['photos'][$in_key]['photo'] = profile_picture($in_value['photo']);
                            }
                        }
                        $accepted_advertiser = $this->mdcampaign_status->get_accepted_advertisers((int)$inside_value['campaign_id']);
                        $data_campaign[$inside_key]['accepted_advertiser_count'] = (int)$accepted_advertiser['accepted_advertiser_count'];
                        $data_campaign[$inside_key]['accepted_advertisers'] = $this->mdinterest->get_current_advertisers_production((int)$inside_value['campaign_id']);
                        if(!empty($data_campaign[$inside_key]['accepted_advertisers'])) {
                            foreach($data_campaign[$inside_key]['accepted_advertisers'] as $in_key => $in_value) {
                                $data_campaign[$inside_key]['accepted_advertisers'][$in_key]['israted'] = (int)$data_campaign[$inside_key]['accepted_advertisers'][$in_key]['israted'];
                                $data_campaign[$inside_key]['accepted_advertisers'][$in_key]['withvideo'] = (int)$data_campaign[$inside_key]['accepted_advertisers'][$in_key]['withvideo'];
                            }
                        }
                    }
                    
                }

                $this->response_data['data'] = $data_campaign;
                $this->response_data['error'] = 0;
                $this->response_data['message'] = 'Specific On Going Production of a Businessman with Advertisers';

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                 return;
            } else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
         return;
    }

    public function all_on_going_campaigns_get($youtuber_id) { // Displays all the On Going Campaign with INTERESTED Campaign of a YouTuber.
        if($this->input->is_ajax_request())
        {
            $campaigns = $this->mdcampaign_status->get_all_on_going_campaign_status(1,$youtuber_id);

            if(!empty($campaigns)) {
                foreach($campaigns as $inside_key => $inside_value) {
                    $photo_campaign = $this->mdcampaign->get_single_photo_display_feed((int)$inside_value['campaign_id']);
                    $campaigns[$inside_key]['photo'] = profile_picture($photo_campaign['photo']);
                    $interested = $this->mdinterest->interested_get((int)$inside_value['campaign_id'], $youtuber_id);
                    if(!empty($interested)){
                        $campaigns[$inside_key]['interested'] = 1;
                    } else{
                        $campaigns[$inside_key]['interested'] = 0;
                    }
                    $accepted_interest = $this->mdinterest->accepted_interest_get((int)$inside_value['campaign_id'], $youtuber_id);
                    if(!empty($accepted_interest)){ // Interest has been accepted by a businessman
                        $campaigns[$inside_key]['accepted'] = 'yes';
                    } else {
                        $campaigns[$inside_key]['accepted'] = 'no';
                    }
                    $count = $this->mdcampaign->get_current_no_advertisers((int)$inside_value['campaign_id']);
                    $campaigns[$inside_key]['count_youtuber'] = $count['count_youtuber'];
                }
            }

            $this->response_data['data'] = $campaigns;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'List of On Going Campaigns of ALL Businessman';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function all_on_going_production_get($businessman_id) { // Displays all the On Going Production of Businessman
        if($this->input->is_ajax_request())
        {
            $campaigns = $this->mdcampaign_status->get_all_on_going_production_status(2,$businessman_id);

            if(!empty($campaigns)) {
                foreach($campaigns as $inside_key => $inside_value) {
                    $photo_campaign = $this->mdcampaign->get_single_photo_display_feed((int)$inside_value['campaign_id']);
                    $campaigns[$inside_key]['photo'] = profile_picture($photo_campaign['photo']);
                    $campaigns[$inside_key]['advertisers'] = $this->mdyoutuber->get_all_youtuber_on_going_production((int)$inside_value['campaign_id']);
                    $count = $this->mdcampaign->get_current_no_advertisers((int)$inside_value['campaign_id']);
                    $campaigns[$inside_key]['count_youtuber'] = $count['count_youtuber'];
                }
            }

            $this->response_data['data'] = $campaigns;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'List of On Going Production of a Businessman';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function campaign_details_get($campaign_id) { // Campaign Details in the Youtuber feed when opening View Details.
        if($this->input->is_ajax_request())
        {
            $campaigns = $this->mdcampaign->get_view_detail_campaign_feed($campaign_id);

            if(!empty($campaigns)) {
                foreach($campaigns as $inside_key => $inside_value) {
                    $campaigns[$inside_key]['category'] = $this->mdcampaign->get_category((int)$inside_value['category_id']);
                    $campaigns[$inside_key]['advertising_details'] = $this->mdcampaign->get_advertising_details((int)$inside_value['campaign_id']);
                    $campaigns[$inside_key]['businessman'] = $this->mdcampaign->get_businessman_specific_campaign((int)$inside_value['businessman_id']);
                    $campaigns[$inside_key]['businessman'][0]['profile_picture'] = profile_picture($campaigns[$inside_key]['businessman'][0]['profile_picture']);
                    $count = $this->mdcampaign->get_current_no_advertisers((int)$inside_value['campaign_id']);
                    $campaigns[$inside_key]['count_youtuber'] = $count['count_youtuber'];
                }
            }

            $this->response_data['data'] = $campaigns;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Campaign Details of a Businessman';

            $this->response($this->response_data);
            return;
        }
        
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function campaign_photo_youtuber_feed_get($campaign_id) { // Campaign Photos of a specific campaign in the youtube feed.
        if($this->input->is_ajax_request())
        {
            $photo_campaign = $this->mdcampaign->get_campaign_photos($campaign_id);

            if(!empty($photo_campaign)) {
                foreach($photo_campaign as $inside_key => $inside_value) {
                    $converted_photo = profile_picture($inside_value['photo']);
                    $photo_campaign[$inside_key]['photo'] = $converted_photo;
                }
            }

            $this->response_data['data'] = $photo_campaign;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Campaign Photos';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    /* =============================== INTEREST =============================== */
    
    public function interested_post() // When Youtuber has an interest to a campaign.
    {   
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE){

            $_POST = (array)json_decode($post_json, TRUE);
            $post_data = $this->input->post();
                
            // Validate post data
            $this->load->library("form_validation");

            $this->form_validation->set_rules("campaign_id","CampaignId","required");

            if($this->form_validation->run())
            {
                //insert user_youtuber
                $data_interested_youtuber = array(
                            "youtuber_id"           => $post_data['youtuber_id'],
                            "campaign_id"           => $post_data['campaign_id'],
                            "message"               => $post_data['message'],
                            "date_interested"       => date("Y-m-d H:i:s")
                        );
  
                $this->mdinterest->interested_post($data_interested_youtuber);
                $this->response_data['error']   = 0;
                $this->response_data['message'] = 'Interested Campaign'; 

                
            }else{
                $this->response_data['message'] = 'error Interested';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
                }
        }else{

            $this->response_data['message'] = $auth_response['message'];
        }
    }

    public function accept_interest_post() // Accept interest
    {
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {
                $_POST = (array)json_decode($post_json, TRUE);
                $post_data = $this->input->post();

                // Validate post data
                $this->load->library("form_validation");

                $this->form_validation->set_rules("campaign_id","Campaign_id","required");
                $this->form_validation->set_rules("youtuber_id","Youtuber_id","required");
                $this->form_validation->set_rules("submission_status_id","Submission_status_id","required");

                if($this->form_validation->run())
                {
                        $data_accepted_interest = array(
                                    "campaign_id"           => $post_data['campaign_id'],
                                    "youtuber_id"           => $post_data['youtuber_id'],
                                    "message"               => $post_data['message'],
                                    "date_interested"       => $post_data['date_interested'],
                                    "submission_status_id"  => $post_data['submission_status_id']
                                );

                        $this->response_data['error']   = 0;
                        $this->response_data['data'] = $this->mdinterest->post_accepted_interest($data_accepted_interest);
                        
                        if($this->response_data['data'] == 1) {
                            $get_message = 'Interest accepted!';
                        } else {
                            $get_message = 'Interest successfully removed!';
                        }
                    
                        $this->response_data['message'] = $get_message;

                } else {
                    $this->response_data['message'] = validation_errors();
                }

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                return;
            } else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function remove_interested_post() // Accept interest
    {
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {
                $_POST = (array)json_decode($post_json, TRUE);
                $post_data = $this->input->post();

                // Validate post data
                $this->load->library("form_validation");

                $this->form_validation->set_rules("campaign_id","Campaign_id","required");
                $this->form_validation->set_rules("youtuber_id","Youtuber_id","required");

                if($this->form_validation->run())
                {
                        $data_interest = array(
                                    "campaign_id"        => $post_data['campaign_id'],
                                    "youtuber_id"        => $post_data['youtuber_id'],
                                    "message"            => $post_data['message'],
                                    "date_interested"    => $post_data['date_interested']
                                );

                        $this->response_data['error']   = 0;
                        $this->response_data['data'] = $this->mdinterest->delete_interest($data_interest);
                        $this->response_data['message'] = 'Successfully deleted.';

                } else {
                    $this->response_data['message'] = validation_errors();
                }

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                return;
            } else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function campaign_youtuber_profile_get($businessman_id) {
        if($this->input->is_ajax_request())
        {
            $campaigns = $this->mdcampaign->get_all_campaigns($businessman_id);

            if(!empty($campaigns)) {
                foreach($campaigns as $inside_key => $inside_value) {
                    $campaigns[$inside_key]['advertising_details'] = $this->mdcampaign->get_advertising_details($inside_value['campaign_id']);
                    $campaigns[$inside_key]['list_of_youtubers'] = $this->mdyoutuber->get_youtuber_campaign($inside_value['campaign_id']);
                }
            }

            $this->response_data['error'] = 0;
            $this->response_data['data'] = $campaigns;
            $this->response_data['message'] = 'Returns of all campaigns from a specific businessman';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    // Interested YouTubers to a Campaign.

    public function interested_youtuber_campaign_get($businessman_id) {
        if($this->input->is_ajax_request())
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {   
                $data_from_post = (array)$auth_response['token'];

                $campaigns = $this->mdinterest->get_specific_campaign($businessman_id);

                if(!empty($campaigns)) {
                    foreach($campaigns as $inside_key => $inside_value) {
                        $campaigns[$inside_key]['category'] = $this->mdcampaign->get_category((int)$inside_value['category_id']);
                        $campaigns[$inside_key]['advertisers'] = $this->mdinterest->get_accepted_youtuber($inside_value['campaign_id']);
                        $photos_campaign = $this->mdcampaign->get_single_photo_display_feed($inside_value['campaign_id']);
                        $campaigns[$inside_key]['photo'] = profile_picture($photos_campaign['photo']);
                        if(!empty($campaigns[$inside_key]['advertisers'])) {
                            foreach($campaigns[$inside_key]['advertisers'] as $inside_key_ads => $inside_value_ads) {
                                $campaigns[$inside_key]['advertisers'][$inside_key_ads]['video_statistics'] = $this->mdyoutube_data->get_video_statistics($inside_value_ads['accepted_interest_id']);          
                            }
                        }
                    }
                }

                $this->response_data['data'] = $campaigns;
                $this->response_data['error'] = 0;
                $this->response_data['message'] = 'Current Businessman Information';

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                return;
            }else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function youtuber_my_campaigns_get($youtuber_id) { // Displays all My Campaign of the youtuber
        if($this->input->is_ajax_request())
        {
            $campaigns = $this->mdinterest->youtuber_my_campaigns_get($youtuber_id);

            if(!empty($campaigns)) {
                foreach($campaigns as $inside_key => $inside_value) {
                    $campaigns[$inside_key]['campaign_details'] = $this->mdcampaign->get_specific_campaign((int)$inside_value['campaign_id']);
                    $photos_campaign = $this->mdcampaign->get_photo((int)$inside_value['campaign_id']);
                    if(!empty($photos_campaign)) {
                        foreach($photos_campaign as $in_key => $in_value) {
                            $campaigns[$inside_key]['campaign_details']['photo'] = profile_picture($in_value['photo']);
                        }
                    }
                    $campaign_photos = $this->mdcampaign->get_campaign_photos((int)$inside_value['campaign_id']);
                    if(!empty($campaign_photos)) {
                        foreach($campaign_photos as $ins_key => $ins_value) {
                            $converted_photo = profile_picture($ins_value['photo']);
                            $campaign_photos[$ins_key]['photo'] = $converted_photo;
                        }
                    }
                    $campaigns[$inside_key]['campaign_details']['photos'] = $campaign_photos;
                    $campaigns[$inside_key]['advertising_details'] = $this->mdcampaign->get_advertising_details((int)$inside_value['campaign_id']);
                    $campaigns[$inside_key]['businessman_details'] = $this->mdbusinessman->get_specific_businessman((int)$campaigns[$inside_key]['campaign_details']['businessman_id']);
                    $campaigns[$inside_key]['businessman_details']['profile_picture'] = profile_picture($campaigns[$inside_key]['businessman_details']['profile_picture']);
                    $submission_status = $this->mdcampaign->status_video_submitted((int)$inside_value['accepted_interest_id']);
                    if(!empty($submission_status)) {
                        $campaigns[$inside_key]['with_video'] = 1;
                    } else {
                        $campaigns[$inside_key]['with_video'] = 0;
                    }
                }
            }
           

            $this->response_data['data'] = $campaigns;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'List of On Going Production of a Businessman';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }
    public function youtuber_my_campaigns_sort_by_production_get($youtuber_id) { // Displays all My Campaign of the youtuber
        if($this->input->is_ajax_request())
        {
            $campaigns = $this->mdinterest->youtuber_my_campaigns_by_production_get($youtuber_id);

            if(!empty($campaigns)) {
                foreach($campaigns as $inside_key => $inside_value) {
                    $campaigns[$inside_key]['campaign_details'] = $this->mdcampaign->get_specific_campaign((int)$inside_value['campaign_id']);
                    $photos_campaign = $this->mdcampaign->get_photo((int)$inside_value['campaign_id']);
                    if(!empty($photos_campaign)) {
                        foreach($photos_campaign as $in_key => $in_value) {
                            $campaigns[$inside_key]['campaign_details']['photo'] = profile_picture($in_value['photo']);
                        }
                    }
                    $campaign_photos = $this->mdcampaign->get_campaign_photos((int)$inside_value['campaign_id']);
                    if(!empty($campaign_photos)) {
                        foreach($campaign_photos as $ins_key => $ins_value) {
                            $converted_photo = profile_picture($ins_value['photo']);
                            $campaign_photos[$ins_key]['photo'] = $converted_photo;
                        }
                    }
                    $campaigns[$inside_key]['campaign_details']['photos'] = $campaign_photos;
                    $campaigns[$inside_key]['advertising_details'] = $this->mdcampaign->get_advertising_details((int)$inside_value['campaign_id']);
                    $campaigns[$inside_key]['businessman_details'] = $this->mdbusinessman->get_specific_businessman((int)$campaigns[$inside_key]['campaign_details']['businessman_id']);
                    $campaigns[$inside_key]['businessman_details']['profile_picture'] = profile_picture($campaigns[$inside_key]['businessman_details']['profile_picture']);
                    $submission_status = $this->mdcampaign->status_video_submitted((int)$inside_value['accepted_interest_id']);
                    if(!empty($submission_status)) {
                        $campaigns[$inside_key]['with_video'] = 1;
                    } else {
                        $campaigns[$inside_key]['with_video'] = 0;
                    }
                }
            }
           

            $this->response_data['data'] = $campaigns;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'List of On Going Production of a Businessman';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }
    public function youtuber_my_campaigns_sort_by_budget_get($youtuber_id,$start,$end) { // Displays all My Campaign of the youtuber
        if($this->input->is_ajax_request())
        {
            $campaigns = $this->mdinterest->youtuber_my_campaigns_by_budget_get($youtuber_id,$start,$end);

            if(!empty($campaigns)) {
                foreach($campaigns as $inside_key => $inside_value) {
                    $campaigns[$inside_key]['campaign_details'] = $this->mdcampaign->get_specific_campaign((int)$inside_value['campaign_id']);
                    $photos_campaign = $this->mdcampaign->get_photo((int)$inside_value['campaign_id']);
                    if(!empty($photos_campaign)) {
                        foreach($photos_campaign as $in_key => $in_value) {
                            $campaigns[$inside_key]['campaign_details']['photo'] = profile_picture($in_value['photo']);
                        }
                    }
                    $campaign_photos = $this->mdcampaign->get_campaign_photos((int)$inside_value['campaign_id']);
                    if(!empty($campaign_photos)) {
                        foreach($campaign_photos as $ins_key => $ins_value) {
                            $converted_photo = profile_picture($ins_value['photo']);
                            $campaign_photos[$ins_key]['photo'] = $converted_photo;
                        }
                    }
                    $campaigns[$inside_key]['campaign_details']['photos'] = $campaign_photos;
                    $campaigns[$inside_key]['advertising_details'] = $this->mdcampaign->get_advertising_details((int)$inside_value['campaign_id']);
                    $campaigns[$inside_key]['businessman_details'] = $this->mdbusinessman->get_specific_businessman((int)$campaigns[$inside_key]['campaign_details']['businessman_id']);
                    $campaigns[$inside_key]['businessman_details']['profile_picture'] = profile_picture($campaigns[$inside_key]['businessman_details']['profile_picture']);
                    $submission_status = $this->mdcampaign->status_video_submitted((int)$inside_value['accepted_interest_id']);
                    if(!empty($submission_status)) {
                        $campaigns[$inside_key]['with_video'] = 1;
                    } else {
                        $campaigns[$inside_key]['with_video'] = 0;
                    }
                }
            }
           

            $this->response_data['data'] = $campaigns;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'List of On Going Production of a Businessman';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }
    public function youtube_video_post() // Add video 
    {
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
                $_POST = (array)json_decode($post_json, TRUE);
                $post_data = $this->input->post();

                // Validate post data
                $this->load->library("form_validation");

                $this->form_validation->set_rules("video_id","Video_id","required");

                if($this->form_validation->run())
                {
                        $data_video = array(
                                    "video_id"             => $post_data['video_id'],
                                    "video_title"          => $post_data['video_title'],
                                    "video_description"    => $post_data['video_description'],
                                    "views_count"          => $post_data['views_count'],
                                    "likes_count"          => $post_data['likes_count'],
                                    "dislikes_count"       => $post_data['dislikes_count'],
                                    "default_thumbnail"    => $post_data['default_thumbnail'],
                                    "comments_count"       => $post_data['comments_count'],
                                    "category_id"          => $post_data['category_id'],
                                    "date_accomplished"    => date("Y-m-d"),
                                    "channel_id"           => $post_data['channel_id'],
                                    "channel_title"        => $post_data['channel_title'],
                                    "youtuber_id"          => $post_data['youtuber_id'],
                                    "accepted_interest_id" => $post_data['accepted_interest_id']
                                );

                        $this->response_data['error']   = 0;
                        $this->response_data['data'] = $this->mdcampaign->post_video($data_video);
                        $this->response_data['message'] = 'Video is Legit and Successfully added.';

                } else {
                    $this->response_data['message'] = 'Please Check your Url it seems it has been already uploaded';
                }

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                return;
        }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function youtube_video_get($accepted_interest) { // get video with details
        if($this->input->is_ajax_request())
        {
            $campaigns = $this->mdcampaign->get_video($accepted_interest);
           
            $this->response_data['data'] = $campaigns;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Get Video';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function report_for_businessman_get($campaign_id, $youtuber_id){
        if($this->input->is_ajax_request())
        {
            $campaigns = $this->mdinterest->businessman_my_campaigns_get($campaign_id, $youtuber_id);
            $campaigns['video_details'] = $this->mdcampaign->get_video($campaigns['accepted_interest_id']);
            $campaigns['youtuber_details'] = $this->mdmediakit->get_youtuber_mediakit($youtuber_id);
            // if(!empty($campaigns)) {
            //     foreach($campaigns as $inside_key => $inside_value) {
            //         $campaigns[$inside_key]['video_details'] = $this->mdcampaign->get_video($inside_value['accepted_interest_id']); 
            //     }
            // }
            $this->response_data['data'] = $campaigns;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Get Video';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function recommend_youtuber_for_campaign_get($category_id) {
        if($this->input->is_ajax_request())
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {   
                $data_from_post = (array)$auth_response['token'];

                $recommended = $this->mdyoutuber->get_recommended_youtuber($category_id);

                $this->response_data['data'] = $recommended;
                $this->response_data['error'] = 0;
                $this->response_data['message'] = 'Recommended Youtuber';

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                return;
            }else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    //=====harold=====///
    public function all_on_going_campaigns_by_category_get($youtuber_id,$category_id) { // Displays all the On Going Campaign with INTERESTED Campaign of a YouTuber.
        if($this->input->is_ajax_request())
        {
            $campaigns = $this->mdcampaign->get_all_on_going_campaign_by_category(1,$youtuber_id,$category_id);

            if(!empty($campaigns)) {
                foreach($campaigns as $inside_key => $inside_value) {
                    $photo_campaign = $this->mdcampaign->get_single_photo_display_feed((int)$inside_value['campaign_id']);
                    $campaigns[$inside_key]['photo'] = profile_picture($photo_campaign['photo']);
                    $interested = $this->mdinterest->interested_get((int)$inside_value['campaign_id'], $youtuber_id);     
                    if(!empty($interested)){
                        $campaigns[$inside_key]['interested'] = 1;
                    } else{
                        $campaigns[$inside_key]['interested'] = 0;
                    }
                    $accepted_interest = $this->mdinterest->accepted_interest_get((int)$inside_value['campaign_id'], $youtuber_id);
                    if(!empty($accepted_interest)){ // Interest has been accepted by a businessman
                        $campaigns[$inside_key]['accepted'] = 'yes';
                    } else {
                        $campaigns[$inside_key]['accepted'] = 'no';
                    }
                }
            }

            $this->response_data['data'] = $campaigns;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'List of On Going Campaigns of ALL Businessman';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }
    public function all_campaigns_get($youtuber_id) { // Displays all the On Going Campaign with INTERESTED Campaign of a YouTuber.
        if($this->input->is_ajax_request())
        {
            $campaigns = $this->mdcampaign->get_all_campaign($youtuber_id);

            if(!empty($campaigns)) {
                foreach($campaigns as $inside_key => $inside_value) {
                    $photo_campaign = $this->mdcampaign->get_single_photo_display_feed((int)$inside_value['campaign_id']);
                    $campaigns[$inside_key]['photo'] = profile_picture($photo_campaign['photo']);
                    $interested = $this->mdinterest->interested_get((int)$inside_value['campaign_id'], $youtuber_id);     
                    if(!empty($interested)){
                        $campaigns[$inside_key]['interested'] = 1;
                    } else{
                        $campaigns[$inside_key]['interested'] = 0;
                    }
                    $accepted_interest = $this->mdinterest->accepted_interest_get((int)$inside_value['campaign_id'], $youtuber_id);
                    if(!empty($accepted_interest)){ // Interest has been accepted by a businessman
                        $campaigns[$inside_key]['accepted'] = 'yes';
                    } else {
                        $campaigns[$inside_key]['accepted'] = 'no';
                    }
                }
            }

            $this->response_data['data'] = $campaigns;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'List of On Going Campaigns of ALL Businessman';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }
    public function all_on_going_campaigns_by_budget_get($youtuber_id,$start_budget,$end_budget) { // Displays all the On Going Campaign with INTERESTED Campaign of a YouTuber.
        if($this->input->is_ajax_request())
        {
            $campaigns = $this->mdcampaign->get_all_on_going_campaign_by_budget(1,$youtuber_id,$start_budget,$end_budget);

            if(!empty($campaigns)) {
                foreach($campaigns as $inside_key => $inside_value) {
                    $photo_campaign = $this->mdcampaign->get_single_photo_display_feed((int)$inside_value['campaign_id']);
                    $campaigns[$inside_key]['photo'] = profile_picture($photo_campaign['photo']);
                    $interested = $this->mdinterest->interested_get((int)$inside_value['campaign_id'], $youtuber_id);     
                    if(!empty($interested)){
                        $campaigns[$inside_key]['interested'] = 1;
                    } else{
                        $campaigns[$inside_key]['interested'] = 0;
                    }
                    $accepted_interest = $this->mdinterest->accepted_interest_get((int)$inside_value['campaign_id'], $youtuber_id);
                    if(!empty($accepted_interest)){ // Interest has been accepted by a businessman
                        $campaigns[$inside_key]['accepted'] = 'yes';
                    } else {
                        $campaigns[$inside_key]['accepted'] = 'no';
                    }
                }
            }

            $this->response_data['data'] = $campaigns;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'List of On Going Campaigns of ALL Businessman';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function all_on_going_campaigns_by_category_and_budget_get($youtuber_id,$category_id,$start_budget,$end_budget) { // Displays all the On Going Campaign with INTERESTED Campaign of a YouTuber.
        if($this->input->is_ajax_request())
        {
            $campaigns = $this->mdcampaign->get_all_on_going_campaign_by_category_and_budget(1,$youtuber_id,$category_id,$start_budget,$end_budget);

            if(!empty($campaigns)) {
                foreach($campaigns as $inside_key => $inside_value) {
                    $photo_campaign = $this->mdcampaign->get_single_photo_display_feed((int)$inside_value['campaign_id']);
                    $campaigns[$inside_key]['photo'] = profile_picture($photo_campaign['photo']);
                    $interested = $this->mdinterest->interested_get((int)$inside_value['campaign_id'], $youtuber_id);     
                    if(!empty($interested)){
                        $campaigns[$inside_key]['interested'] = 1;
                    } else{
                        $campaigns[$inside_key]['interested'] = 0;
                    }
                    $accepted_interest = $this->mdinterest->accepted_interest_get((int)$inside_value['campaign_id'], $youtuber_id);
                    if(!empty($accepted_interest)){ // Interest has been accepted by a businessman
                        $campaigns[$inside_key]['accepted'] = 'yes';
                    } else {
                        $campaigns[$inside_key]['accepted'] = 'no';
                    }
                }
            }

            $this->response_data['data'] = $campaigns;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'List of On Going Campaigns of ALL Businessman';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }
    public function all_camapign_category_get()
    {
        if($this->input->is_ajax_request())
        {
                $this->response_data['data'] = $this->mdcampaign->get_all_campaign_category();

                $this->response_data['error'] = 0;
                $this->response_data['message'] = 'New Youtuber';

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                return;

        } else {
            $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
            return;
        }
        
    }
    
    //=====harold=====///

    public function video_id_get() { // Campaign Details in the Youtuber feed when opening View Details.
        if($this->input->is_ajax_request())
        {
            $video = $this->mdcampaign->get_video_id();


            $this->response_data['data'] = $video;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Video Id of all videos submitted';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

}
