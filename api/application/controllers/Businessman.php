<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';

class Businessman extends REST_Controller {

    var $response_data = array();

    public function __construct()
    {
        parent::__construct();

        // init response data
        $this->response_data = array(
            'error' => 1,
            'data'  => array(),
            'message' => 'Failed on processing.'
        );

        $this->load->model('mdbusinessman');
    }

    public function add_activity_log_businessman_post()
    {   
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $_POST = (array)json_decode($post_json, TRUE);
            $post_data = $this->input->post();

            // Validate post data
            $this->load->library("form_validation");

            $this->form_validation->set_rules("activity","Activity","required");
            $this->form_validation->set_rules("businessman_id","Businessman_id","required");

            if($this->form_validation->run())
            {
                //insert activity_log
                $activity_log = array(
                            "activity"          => $post_data['activity'],
                            "businessman_id"    => $post_data['businessman_id'],
                            "date_added"        => date("Y-m-d H:i:s")
                        );

                $this->mdbusinessman->add_activity_log($activity_log);
                $this->response_data['error']   = 0;
                $this->response_data['message'] = 'Activity log inserted!'; 
            } else{
                $this->response_data['message'] = validation_errors();
            }

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }

        
        return;
    }
    
    public function add_businessman_post()
    {   
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $_POST = (array)json_decode($post_json, TRUE);
            $post_data = $this->input->post();

            // Validate post data
            $this->load->library("form_validation");

            $this->form_validation->set_rules("username","Username","required");
            $this->form_validation->set_rules("password","Password","required");

            if($this->form_validation->run())
            {
                $this->load->library("bcrypt");
                //insert data_user_businessman
                $data_user_businessman = array(
                            "username"          => $post_data['username'],
                            "password"          => $this->bcrypt->hash($post_data['password']),
                            "business_name"     => $post_data['business_name'],
                            "first_name"        => $post_data['first_name'],
                            "last_name"         => $post_data['last_name'],
                            "email_address"     => $post_data['email_address'],
                            "company_address"   => $post_data['company_address'],
                            "contact_number"    => $post_data['contact_number'],
                            "abouts"            => $post_data['abouts'],
                            "date_added"        => date("Y-m-d H:i:s")
                        );
                
                        if(isset($post_data['profile_picture']) && !empty($post_data['profile_picture'])) {
                            $profile_data = base64ToImage("data:".$post_data['profile_picture']);
                            if($profile_data) {
                                $profile_path = './uploads/profile/';
                                if (file_put_contents($profile_path.$profile_data['file_name'], $profile_data['data'])) {
                                    $data_user_businessman['profile_picture'] = $profile_data['file_name'];
                                }
                            }
                        }

                $this->mdbusinessman->add_businessman($data_user_businessman);
                $this->response_data['error']   = 0;
                $this->response_data['message'] = 'Businessman added sucessfully and saved!'; 
            } else{
                $this->response_data['message'] = validation_errors();
            }

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }

        
        return;
    }

    public function add_businessman_rating_post()
    {   
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $_POST = (array)json_decode($post_json, TRUE);
            $post_data = $this->input->post();

            // Validate post data
            $this->load->library("form_validation");

            $this->form_validation->set_rules("engaging","Engaging","required");
            $this->form_validation->set_rules("credibility","Credibility","required");
            $this->form_validation->set_rules("impression","Impression","required");
            $this->form_validation->set_rules("action_oriented","Action_Oriented","required");
            $this->form_validation->set_rules("significance","Significance","required");
            $this->form_validation->set_rules("integrated","Integrated","required");
            $this->form_validation->set_rules("brand_quality","Brand_Quality","required");
            $this->form_validation->set_rules("brand_innovation","Brand_Innovation","required");
            $this->form_validation->set_rules("brand_quality","Brand_Quality","required");
            $this->form_validation->set_rules("accepted_interest_id","Accepted_interest_id","required");

            if($this->form_validation->run())
            {
                $this->load->library("bcrypt");
                //insert data_user_businessman
                $businessman_rating = array(
                            "engaging"                   => $post_data['engaging'],
                            "credibility"                => $post_data['credibility'],
                            "impression"                 => $post_data['impression'],
                            "action_oriented"            => $post_data['action_oriented'],
                            "significance"               => $post_data['significance'],
                            "integrated"                 => $post_data['integrated'],
                            "brand_quality"              => $post_data['brand_quality'],
                            "brand_innovation"           => $post_data['brand_innovation'],
                            "brand_quality"              => $post_data['brand_quality'],
                            "accepted_interest_id"       => $post_data['accepted_interest_id'],
                        );

                $this->mdbusinessman->add_businessman_rating($businessman_rating);
                $this->response_data['error']   = 0;
                $this->response_data['message'] = 'Businessman rating added sucessfully and saved!'; 
            } else{
                $this->response_data['message'] = validation_errors();
            }

            $this->response($this->response_data);
            return;
        }

        
        return;
    }

    public function current_businessman_get()
    {
        if($this->input->is_ajax_request())
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {
                $data_from_post = (array)$auth_response['token'];
                $businessman_id = $data_from_post['businessman_id'];

                //print_r($nurse_details);exit;

                $businessman = $this->mdbusinessman->get_specific_businessman($businessman_id);
                $businessman['profile_picture'] = profile_picture($businessman['profile_picture']);

                $this->response_data['data'] = $businessman;
                $this->response_data['error'] = 0;
                $this->response_data['message'] = 'Current Businessman Information';

                $this->response($this->response_data);
                return;
            }else
            {
                $this->response_data['message'] = $auth_response['message'];
                $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
                return;
            }
        }
        return;
    }

    public function all_businessman_get()
    {
        if($this->input->is_ajax_request())
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {
                $data_from_post = (array)$auth_response['token'];

                $all_businessman = $this->mdbusinessman->get_all_businessman();

                if(!empty($all_businessman)) {
                    foreach($all_businessman as $inside_key => $inside_value) {
                        $all_businessman[$inside_key]['photo'] = profile_picture($inside_value['profile_picture']);
                    }
                }

                $this->response_data['data'] = $all_businessman;
                $this->response_data['error'] = 0;
                $this->response_data['message'] = 'Get All Businessman';

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                return;
            }else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }
        return;
    }

    public function remove_businessman_delete($businessman_id) {
        if($this->input->is_ajax_request())
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {
                $data_from_post = (array)$auth_response['token'];

                $data_businessman = $this->mdbusinessman->remove_businessman($businessman_id);

                $this->response_data['error'] = 0;
                $this->response_data['message'] = 'Businessman Successfully Removed';

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                return;
            }else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }
        return;
    }
}
