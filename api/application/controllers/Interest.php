<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';

class Interest extends REST_Controller {

    var $response_data = array();

    public function __construct()
    {
        parent::__construct();

        // init response data
        $this->response_data = array(
            'error' => 1,
            'data'  => array(),
            'message' => 'Failed on processing.'
        );

        $this->load->model('mdcampaign_status');
        $this->load->model('mdinterest');
    }

    public function updated_rating_status_put($accepted_interest_id) {
        $auth_response = Authorization::validateToken();
        if($auth_response['error'] == 0)
        {
            $token = (array) $auth_response['token'];
            $put_data = $this->put();

            $data = array(
                "israted" => $put_data['israted']
            );

            $this->mdcampaign_status->rating_status($accepted_interest_id, $data);
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Update rating status sucessfully set.';
            $this->response($this->response_data, REST_Controller::HTTP_OK);
        }
    }

    public function updated_accepted_interest_put($youtuber_id, $campaign_id) { // YouTuber
        if($this->input->is_ajax_request())
        {
            $put_data = $this->put();

            $data = array(
                "submission_status_id"  => $put_data['submission_status_id'],
                "withvideo"             => $put_data['withvideo']
            );

            $this->mdinterest->set_submission_status($youtuber_id, $campaign_id, $data);
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Video successfully submitted.';
            $this->response($this->response_data, REST_Controller::HTTP_OK);
        }
    }
    public function updated_accepted_interest_video_put($youtuber_id, $campaign_id) { // YouTuber
        if($this->input->is_ajax_request())
        {
            $put_data = $this->put();

            $data = array(
                "submission_status_id"  => $put_data['submission_status_id'],
                "withvideo"             => $put_data['withvideo'],
                "date_submitted"        => $put_data['date_submitted']

            );

            $this->mdinterest->set_submission_status($youtuber_id, $campaign_id, $data);
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Video successfully submitted.';
            $this->response($this->response_data, REST_Controller::HTTP_OK);
        }
    }

    public function youtuber_submitted_video_get($youtuber_id, $campaign_id) {
        if($this->input->is_ajax_request())
        {
            $this->response_data['data'] = $this->mdinterest->get_youtuber_submitted_video($youtuber_id, $campaign_id);
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Retrieved youtuber submitted video.';
            $this->response($this->response_data, REST_Controller::HTTP_OK);
        }
    }

}
