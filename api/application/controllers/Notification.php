<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';

class Notification extends REST_Controller {

    var $response_data = array();

    public function __construct()
    {
        parent::__construct();

        // init response data
        $this->response_data = array(
            'error' => 1,
            'data'  => array(),
            'message' => 'Failed on processing.'
        );

        $this->load->model('mdnotification');
    }

    public function add_notification_post()
    {   
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $_POST = (array)json_decode($post_json, TRUE);
            $post_data = $this->input->post();

            //insert user_youtuber
            $data_notification = array(
                    "user_img"        => $post_data['user_img'],
                    "name"            => $post_data['name'],
                    "notif_subject"   => $post_data['notif_subject'],
                    "notif_msg"       => $post_data['notif_msg'],
                    "notif_to"        => $post_data['notif_to'],
                    "notif_from"      => $post_data['notif_from'],
                    "seen"            => 0,
                    "date_added"      => date('Y-m-d H:i:s'),
                    
            );

            $this->mdnotification->send_notification($data_notification);
            $this->response_data['error']   = 0;
            $this->response_data['message'] = 'Notification sent to the user'; 

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;

        } else{
            $this->response_data['message'] = $auth_response['message'];
        }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }
    public function notification_get($enc_id)
    {
        $id = $enc_id;
        if($this->input->is_ajax_request())
        {
    
            $this->response_data['data'] = $this->mdnotification->get_notification($id);

            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Notification';

            $this->response($this->response_data);
            return;
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function update_campaign_put($notif_id)
    {
        // $auth_response = Authorization::validateToken();
        // if($auth_response['error'] == 0)
        // {
            // $token = (array) $auth_response['token'];
            // $put_data = $this->put();

            $data_notif = array(
                "seen"        => 1
            );

            $this->mdnotification->update_campaign($notif_id, $data_notif);
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Notification sucessfully updated';
            $this->response($this->response_data);
        // }
    }

   

}
