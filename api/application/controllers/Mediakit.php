<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';

class Mediakit extends REST_Controller {

    var $response_data = array();

    public function __construct()
    {
        parent::__construct();

        // init response data
        $this->response_data = array(
            'error' => 1,
            'data'  => array(),
            'message' => 'Failed on processing.'
        );

        $this->load->model('mdmediakit');
        $this->load->model('mdyoutuber');

    }
    
    public function add_mediakit_post()
    
    {   
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $_POST = (array)json_decode($post_json, TRUE);
            $post_data = $this->input->post();

            // Validate post data
            $this->load->library("form_validation");

            $this->form_validation->set_rules("youtuber_id","YoutuberId","required");
            $this->form_validation->set_rules("firstName","FirstName","required");
            $this->form_validation->set_rules("lastName","LastName","required");
            $this->form_validation->set_rules("photoUrl","PhotoUrl","required");

            if($this->form_validation->run())
            {
                //insert user_youtuber
                $data_user_youtuber = array(
                        "youtuber_id"           => $post_data['youtuber_id'],
                        "firstName"             => $post_data['firstName'],
                        "lastName"              => $post_data['lastName'],
                        "address"               => $post_data['address'],
                        "contact_no"            => $post_data['contact_no'],
                        "youtubeCh"             => $post_data['youtubeCh'],
                        "ig_ac"                 => $post_data['ig_ac'],
                        "fb_ac"                 => $post_data['fb_ac'],
                        "twitter_ac"            => $post_data['twitter_ac'],
                        "gender"                => $post_data['gender'],
                        "photoUrl"              => $post_data['photoUrl'],
                        "abouts"                => $post_data['abouts'],
                        "date_added"             => date("Y-m-d")
                );

                $this->mdmediakit->add_mediakit($data_user_youtuber);
                $this->response_data['error']   = 0;
                $this->response_data['data']    = $this->mdmediakit->get_youtuber_mediakit($post_data['youtuber_id']);
                $this->response_data['message'] = 'Youtuber added sucessfully and saved!'; 
            }

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;

        } else{

            $this->response_data['message'] = $auth_response['message'];
        }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function add_mediakit_interest_post()
    {   
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $_POST = (array)json_decode($post_json, TRUE);
            $post_data = $this->input->post();

            //insert user_youtuber
            $data_user_youtuber = array(
                    "interest"       => $post_data['interest'],
                    "id"             => $post_data['id']
            );

            $this->mdmediakit->add_mediakit_interest($data_user_youtuber);
            $this->response_data['error']   = 0;
            $this->response_data['message'] = 'Youtuber added sucessfully and saved!'; 

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;

        } else{

            $this->response_data['message'] = $auth_response['message'];
        }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }
    
    public function mediakit_get($enc_id)
    {
        $id = $enc_id;
        if($this->input->is_ajax_request())
        {
    
            $this->response_data['data'] = $this->mdmediakit->get_youtuber_info($id);

            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Youtuber';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function all_mediakit_get($enc_id)
    {
        $id = $enc_id;
        if($this->input->is_ajax_request())
        {
    
            $this->response_data['data'] = $this->mdmediakit->get_all_youtuber($id);

            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Youtuber';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function search_mediakit_get($search)
    {   
        if($this->input->is_ajax_request())
        {

            $this->response_data['data'] = $this->mdmediakit->search_mediakit($search);
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Youtuber Found!'; 

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;

        }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }
    public function all_mediakit_refer_get($youtuber_id , $campaign_id)
    {
        if($this->input->is_ajax_request())
        {
            if(isset($youtuber_id)) {
                if(isset($campaign_id)) {
                    $youtuber_refer = $this->mdmediakit->get_all_youtuber($youtuber_id);
                    foreach($youtuber_refer as $inside_key => $inside_value) {
                        $interested = $this->mdyoutuber->get_youtuber_refer($youtuber_id,$inside_value['youtuber_id'],$campaign_id);
                        if(!empty($interested)){
                            $youtuber_refer[$inside_key]['referred_status'] = 1;
                        } else{
                            $youtuber_refer[$inside_key]['referred_status'] = 0;
                        }
                    }
    
                    $this->response_data['data'] = $youtuber_refer;
                    $this->response_data['error'] = 0;
                    $this->response_data['message'] = 'Youtuber Referred';
    
                    $this->response($this->response_data, REST_Controller::HTTP_OK);
                    return;
                } else {
                    $this->response_data['message'] = 'Campaign_id is required.';
                    $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
                    return;
                }
            }
        } else {
            $this->response_data['message'] = 'Youtuber_id and campaign_id is required.';
            $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
            return;
        }
    }



}