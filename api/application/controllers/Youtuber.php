<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';

class Youtuber extends REST_Controller {

    var $response_data = array();

    public function __construct()
    {
        parent::__construct();

        // init response data
        $this->response_data = array(
            'error' => 1,
            'data'  => array(),
            'message' => 'Failed on processing.'
        );

        $this->load->model('mdyoutuber');
        $this->load->model('mdyoutube_data');
        $this->load->model('mdbusinessman');
        $this->load->model('mdcampaign');
        $this->load->model('mdauth');
        $this->load->model('mdhotcampaign');
        $this->load->model('mdinterest');
    }

    public function add_youtuber_post()
    {   
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE){

            $_POST = (array)json_decode($post_json, TRUE);
            $post_data = $this->input->post();
                
            // Validate post data
            $this->load->library("form_validation");

            $this->form_validation->set_rules("id","Id","required");
            $this->form_validation->set_rules("idToken","IdToken","required");
            $this->form_validation->set_rules("name","Name","required");
            $this->form_validation->set_rules("email","Email","required");
            $this->form_validation->set_rules("authToken","AuthToken","required");
            $this->form_validation->set_rules("photoUrl","PhotoUrl","required");

            if($this->form_validation->run())
            {
                //insert user_youtuber
                $data_user_youtuber = array(
                            "youtuber_id"           => $post_data['id'],
                            "idToken"               => $post_data['idToken'],
                            "name"                  => $post_data['name'],
                            "email"                 => $post_data['email'],
                            "authToken"             => $post_data['authToken'],
                            "photoUrl"              => $post_data['photoUrl'],
                            "date_added"            => date("Y-m-d H:i:s"),
                        );
  
                $this->mdyoutuber->add_youtuber($data_user_youtuber);
                $this->response_data['error']   = 0;
                $this->response_data['message'] = 'Youtuber added sucessfully and saved!'; 
                $this->response($this->response_data, REST_Controller::HTTP_OK);
                return;
            } else {
                $this->response_data['message'] = 'Youtuber not added';
            }
            return;
        } else {
            $this->response_data['message'] = $auth_response['message'];
        }
    }

    public function all_youtuber_get()
    {
        if($this->input->is_ajax_request())
        {
            $this->response_data['data'] = $this->mdyoutuber->get_all_youtuber();
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Get All YouTuber';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }
     
    public function youtuber_get($enc_id)
    {
        $id = $enc_id;
        if($this->input->is_ajax_request())
        {
            $this->response_data['data'] = $this->mdyoutuber->get_youtuber($id);

            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'List of Youtuber';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function youtuber_channel_get($enc_id)
    {
        $id = $enc_id;
        if($this->input->is_ajax_request())
        {
                $this->response_data['data'] = $this->mdyoutuber->get_youtuber_channel($id);

                $this->response_data['error'] = 0;
                $this->response_data['message'] = 'Youtuber';

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                return;
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function all_youtubers_info_get()
    {
        if($this->input->is_ajax_request())
        {
            $this->response_data['data'] = $this->mdyoutuber->get_all_new_youtuber();

            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'New Youtuber';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
    
        } else {
            $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
            return;
        }    
    }

    public function businessman_rating($accepted_interest_id) {
        $total_businessman_rating = 0;
        // $rating = $this->mdyoutube_data->get_businessman_rating_v2($accepted_interest_id);
        $rating = $this->mdyoutube_data->get_businessman_rating($accepted_interest_id);
        if(!empty($rating)) {
            foreach($rating as $inside_key => $inside_value) {
                $weight = $this->mdauth->get_all_criteria();
                $engaging_rate = $inside_value['engaging'] * (double)$weight[3]['criteria_percentage']; // 0.45
                $credibility_rate = $inside_value['credibility'] * (double)$weight[4]['criteria_percentage']; // 0.8
                $impression_rate = $inside_value['impression'] * (double)$weight[5]['criteria_percentage']; // 1.35
                $action_oriented_rate = $inside_value['action_oriented'] * (double)$weight[6]['criteria_percentage']; // 0.9
                $significance_rate = $inside_value['significance'] * (double)$weight[7]['criteria_percentage']; // 1.2
                $integrated_rate = $inside_value['integrated'] * (double)$weight[8]['criteria_percentage']; //  0.4
                $brand_service_rate = $inside_value['brand_service'] * (double)$weight[9]['criteria_percentage']; // 0
                $brand_innovation_rate = $inside_value['brand_innovation'] * (double)$weight[10]['criteria_percentage']; // 0.7
                $brand_quality_rate = $inside_value['brand_quality'] * (double)$weight[11]['criteria_percentage']; // 1.8

                $total_businessman_rating = $engaging_rate + $credibility_rate + $impression_rate +
                                $action_oriented_rate + $significance_rate + $integrated_rate +
                                $brand_service_rate + $brand_innovation_rate + $brand_quality_rate;
            }
        } else { // Video submitted but there is no Businessman Rating
            $total_businessman_rating = 0;
        }

        return $total_businessman_rating;
    }

    public function reputation_computation($videos) {
        
        // $range['minmax_values']['comedy'] = $this->mdyoutube_data->get_minmax_values_v2(1);
        // $range['minmax_values']['drama'] = $this->mdyoutube_data->get_minmax_values_v2(2);
        // $range['minmax_values']['education'] = $this->mdyoutube_data->get_minmax_values_v2(3);
        // $range['minmax_values']['entertainment'] = $this->mdyoutube_data->get_minmax_values_v2(4);
        // $range['minmax_values']['family'] = $this->mdyoutube_data->get_minmax_values_v2(5);
        // $range['minmax_values']['gaming'] = $this->mdyoutube_data->get_minmax_values_v2(6);
        // $range['minmax_values']['howto_and_styles'] = $this->mdyoutube_data->get_minmax_values_v2(7);
        // $range['minmax_values']['music'] = $this->mdyoutube_data->get_minmax_values_v2(8);
        // $range['minmax_values']['sports'] = $this->mdyoutube_data->get_minmax_values_v2(9);
        // $range['minmax_values']['travel_and_events'] = $this->mdyoutube_data->get_minmax_values_v2(10);
        // $range['minmax_values']['people_and_blogs'] = $this->mdyoutube_data->get_minmax_values_v2(11);

        $range['minmax_values']['comedy'] = $this->mdyoutube_data->get_minmax_values(1);
        $range['minmax_values']['drama'] = $this->mdyoutube_data->get_minmax_values(2);
        $range['minmax_values']['education'] = $this->mdyoutube_data->get_minmax_values(3);
        $range['minmax_values']['entertainment'] = $this->mdyoutube_data->get_minmax_values(4);
        $range['minmax_values']['family'] = $this->mdyoutube_data->get_minmax_values(5);
        $range['minmax_values']['gaming'] = $this->mdyoutube_data->get_minmax_values(6);
        $range['minmax_values']['howto_and_styles'] = $this->mdyoutube_data->get_minmax_values(7);
        $range['minmax_values']['music'] = $this->mdyoutube_data->get_minmax_values(8);
        $range['minmax_values']['sports'] = $this->mdyoutube_data->get_minmax_values(9);
        $range['minmax_values']['travel_and_events'] = $this->mdyoutube_data->get_minmax_values(10);
        $range['minmax_values']['people_and_blogs'] = $this->mdyoutube_data->get_minmax_values(11);

        $weight = $this->mdauth->get_all_criteria();

        $temp_comedy = 0;
        $temp_drama = 0;
        $temp_education = 0;
        $temp_entertainment = 0;
        $temp_family = 0;
        $temp_gaming = 0;
        $temp_howto_and_styles = 0;
        $temp_music = 0;
        $temp_sports = 0;
        $temp_travel_and_events = 0;
        $temp_people_and_blogs = 0;

        $temp_businessman_rating = 0;
        $total_businessman_rating = 0;
        $count_ratings = 1; // Initialize, videos gathered with businessman rating will be counted as ONE.

        $comedy_count_ratings = 0; // Initialize, videos gathered with businessman rating will be counted as ONE.
        $drama_count_ratings = 0; // Initialize, videos gathered with businessman rating will be counted as ONE.
        $education_count_ratings = 0; // Initialize, videos gathered with businessman rating will be counted as ONE.
        $entertainment_count_ratings = 0; // Initialize, videos gathered with businessman rating will be counted as ONE.
        $family_count_ratings = 0; // Initialize, videos gathered with businessman rating will be counted as ONE.
        $gaming_count_ratings = 0; // Initialize, videos gathered with businessman rating will be counted as ONE.
        $howto_and_styles_count_ratings = 0; // Initialize, videos gathered with businessman rating will be counted as ONE.
        $music_count_ratings = 0; // Initialize, videos gathered with businessman rating will be counted as ONE.
        $sports_count_ratings = 0; // Initialize, videos gathered with businessman rating will be counted as ONE.
        $travel_and_events_count_ratings = 0; // Initialize, videos gathered with businessman rating will be counted as ONE.
        $people_and_blogs_count_ratings = 0; // Initialize, videos gathered with businessman rating will be counted as ONE.

        $numItems = count($videos);
        $index_count = 1;

        foreach($videos as $inside_key => $inside_value) {

            $point_range = 10 - 0;
            $views_percentage = (double)$weight[0]['criteria_percentage'];
            $reactions_percentage = (double)$weight[1]['criteria_percentage'];
            $comments_percentage = (double)$weight[2]['criteria_percentage'];
            
            switch((int)$inside_value['category_id']) {
                case 11: { // People & Blogs
                    $minmaxViews = (int)$range['minmax_values']['people_and_blogs'][0]['maxViewsCount'] - (int)$range['minmax_values']['people_and_blogs'][0]['minViewsCount'];
                    $view_count = (int)$inside_value['views_count'] - (int)$range['minmax_values']['people_and_blogs'][0]['minViewsCount'];
                    $video_views_score = ( ($view_count) * ( ( ($point_range) / ($minmaxViews) )  + 0 ) ) * $views_percentage;
                    
                    // Likes - Dislikes
                    $min_reaction = (int)$range['minmax_values']['people_and_blogs'][0]['minLikesCount'] - (int)$range['minmax_values']['people_and_blogs'][0]['minDislikesCount'];
                    $minmaxReactions = ($range['minmax_values']['people_and_blogs'][0]['maxLikesCount'] - (int)$range['minmax_values']['people_and_blogs'][0]['maxDislikesCount']) - $min_reaction;
                    $current_reaction = (int)$inside_value['likes_count'] - (int)$inside_value['dislikes_count'];
                    $reaction_count = $current_reaction - $min_reaction;
                    $video_reactions_score = ( ($reaction_count) * ( ( ($point_range) / ($minmaxReactions) ) + 0 ) ) * $reactions_percentage;

                    $minmaxComments = $range['minmax_values']['people_and_blogs'][0]['maxCommentsCount'] - (int)$range['minmax_values']['people_and_blogs'][0]['minCommentsCount'];
                    $comment_count = (int)$inside_value['comments_count'] - (int)$range['minmax_values']['people_and_blogs'][0]['minCommentsCount'];
                    $video_comments_score = ( ($comment_count) * ( ( ($point_range) / ($minmaxComments) ) + 0 ) ) * $comments_percentage;

                    $total_video_performance = $video_views_score + $video_reactions_score + $video_comments_score;
                    $total_youtube_status = $total_video_performance * 0.8;

                    if($inside_value['accepted_interest_id'] > 0) { // If there is accepted interest id of video submitted
                        if($this->businessman_rating($inside_value['accepted_interest_id']) != 0) { // Video submitted with Businessman Rating. If no Businessman Rating, default is 0.
                            $temp_video_with_rating_11 = $total_youtube_status + ($this->businessman_rating($inside_value['accepted_interest_id']) * 0.2);
                            $temp_people_and_blogs = $temp_people_and_blogs + $temp_video_with_rating_11;
                            ++$people_and_blogs_count_ratings; // Count videos with businessman ratings
                        } else {
                            $temp_people_and_blogs = $temp_people_and_blogs + $total_youtube_status;
                            ++$people_and_blogs_count_ratings; // Count videos with businessman ratings
                        }
                    } else {
                        $temp_people_and_blogs = $temp_people_and_blogs + $total_youtube_status;
                        ++$people_and_blogs_count_ratings; // Count videos with businessman ratings
                    }
                }
                break;
                case 10: { // Travel and Events
                    $minmaxViews = (int)$range['minmax_values']['travel_and_events'][0]['maxViewsCount'] - (int)$range['minmax_values']['travel_and_events'][0]['minViewsCount'];
                    $view_count = (int)$inside_value['views_count'] - (int)$range['minmax_values']['travel_and_events'][0]['minViewsCount'];
                    $video_views_score = ( ($view_count) * ( ( ($point_range) / ($minmaxViews) )  + 0 ) ) * $views_percentage;
                    
                    // Likes - Dislikes
                    $min_reaction = (int)$range['minmax_values']['travel_and_events'][0]['minLikesCount'] - (int)$range['minmax_values']['travel_and_events'][0]['minDislikesCount'];
                    $minmaxReactions = ($range['minmax_values']['travel_and_events'][0]['maxLikesCount'] - (int)$range['minmax_values']['travel_and_events'][0]['maxDislikesCount']) - $min_reaction;
                    $current_reaction = (int)$inside_value['likes_count'] - (int)$inside_value['dislikes_count'];
                    $reaction_count = $current_reaction - $min_reaction;
                    $video_reactions_score = ( ($reaction_count) * ( ( ($point_range) / ($minmaxReactions) ) + 0 ) ) * $reactions_percentage;

                    $minmaxComments = $range['minmax_values']['travel_and_events'][0]['maxCommentsCount'] - (int)$range['minmax_values']['travel_and_events'][0]['minCommentsCount'];
                    $comment_count = (int)$inside_value['comments_count'] - (int)$range['minmax_values']['travel_and_events'][0]['minCommentsCount'];
                    $video_comments_score = ( ($comment_count) * ( ( ($point_range) / ($minmaxComments) ) + 0 ) ) * $comments_percentage;

                    $total_video_performance = $video_views_score + $video_reactions_score + $video_comments_score;
                    $total_youtube_status = $total_video_performance * 0.8;
                    // NOTE: Video submitted will be later recorded unless businessman rating is submitted.
                    if($inside_value['accepted_interest_id'] > 0) { // If there is accepted interest id
                        if($this->businessman_rating($inside_value['accepted_interest_id']) != 0) { // Video submitted with Businessman Rating. If no Businessman Rating, default is 0.
                            $temp_video_with_rating_10 = $total_youtube_status + ($this->businessman_rating($inside_value['accepted_interest_id']) * 0.2);
                            $temp_travel_and_events = $temp_travel_and_events + $temp_video_with_rating_10;
                            ++$travel_and_events_count_ratings; // Count videos with businessman ratings
                        } else {
                            $temp_travel_and_events = $temp_travel_and_events + $total_youtube_status;
                            ++$travel_and_events_count_ratings; // Count videos with businessman ratings
                        }
                    } else {
                        $temp_travel_and_events = $temp_travel_and_events + $total_youtube_status;
                        ++$travel_and_events_count_ratings; // Count videos with businessman ratings
                    }
                }
                break;
                case 9: { // Sports
                    $minmaxViews = (int)$range['minmax_values']['sports'][0]['maxViewsCount'] - (int)$range['minmax_values']['sports'][0]['minViewsCount'];
                    $view_count = (int)$inside_value['views_count'] - (int)$range['minmax_values']['sports'][0]['minViewsCount'];
                    $video_views_score = ( ($view_count) * ( ( ($point_range) / ($minmaxViews) )  + 0 ) ) * $views_percentage;
                    
                    // Likes - Dislikes
                    $min_reaction = (int)$range['minmax_values']['sports'][0]['minLikesCount'] - (int)$range['minmax_values']['sports'][0]['minDislikesCount'];
                    $minmaxReactions = ($range['minmax_values']['sports'][0]['maxLikesCount'] - (int)$range['minmax_values']['sports'][0]['maxDislikesCount']) - $min_reaction;
                    $current_reaction = (int)$inside_value['likes_count'] - (int)$inside_value['dislikes_count'];
                    $reaction_count = $current_reaction - $min_reaction;
                    $video_reactions_score = ( ($reaction_count) * ( ( ($point_range) / ($minmaxReactions) ) + 0 ) ) * $reactions_percentage;

                    $minmaxComments = $range['minmax_values']['sports'][0]['maxCommentsCount'] - (int)$range['minmax_values']['sports'][0]['minCommentsCount'];
                    $comment_count = (int)$inside_value['comments_count'] - (int)$range['minmax_values']['sports'][0]['minCommentsCount'];
                    $video_comments_score = ( ($comment_count) * ( ( ($point_range) / ($minmaxComments) ) + 0 ) ) * $comments_percentage;

                    $total_video_performance = $video_views_score + $video_reactions_score + $video_comments_score;
                    $total_youtube_status = $total_video_performance * 0.8;
                    // NOTE: Video submitted will be later recorded unless businessman rating is submitted.
                    if($inside_value['accepted_interest_id'] > 0) { // If there is accepted interest id
                        if($this->businessman_rating($inside_value['accepted_interest_id']) != 0) { // Video submitted with Businessman Rating. If no Businessman Rating, default is 0.
                            $temp_video_with_rating_9 = $total_youtube_status + ($this->businessman_rating($inside_value['accepted_interest_id']) * 0.2);
                            $temp_sports = $temp_sports + $temp_video_with_rating_9;
                            ++$sports_count_ratings; // Count videos with businessman ratings
                        } else {
                            $temp_sports = $temp_sports + $total_youtube_status;
                            ++$sports_count_ratings; // Count videos with businessman ratings
                        }
                    } else {
                        $temp_sports = $temp_sports + $total_youtube_status;
                        ++$sports_count_ratings; // Count videos with businessman ratings
                    }
                }
                break;
                case 8: { // Music
                    $minmaxViews = (int)$range['minmax_values']['music'][0]['maxViewsCount'] - (int)$range['minmax_values']['music'][0]['minViewsCount'];
                    $view_count = (int)$inside_value['views_count'] - (int)$range['minmax_values']['music'][0]['minViewsCount'];
                    $video_views_score = ( ($view_count) * ( ( ($point_range) / ($minmaxViews) )  + 0 ) ) * $views_percentage;
                    
                    // Likes - Dislikes
                    $min_reaction = (int)$range['minmax_values']['music'][0]['minLikesCount'] - (int)$range['minmax_values']['music'][0]['minDislikesCount'];
                    $minmaxReactions = ($range['minmax_values']['music'][0]['maxLikesCount'] - (int)$range['minmax_values']['music'][0]['maxDislikesCount']) - $min_reaction;
                    $current_reaction = (int)$inside_value['likes_count'] - (int)$inside_value['dislikes_count'];
                    $reaction_count = $current_reaction - $min_reaction;
                    $video_reactions_score = ( ($reaction_count) * ( ( ($point_range) / ($minmaxReactions) ) + 0 ) ) * $reactions_percentage;

                    $minmaxComments = $range['minmax_values']['music'][0]['maxCommentsCount'] - (int)$range['minmax_values']['music'][0]['minCommentsCount'];
                    $comment_count = (int)$inside_value['comments_count'] - (int)$range['minmax_values']['music'][0]['minCommentsCount'];
                    $video_comments_score = ( ($comment_count) * ( ( ($point_range) / ($minmaxComments) ) + 0 ) ) * $comments_percentage;

                    $total_video_performance = $video_views_score + $video_reactions_score + $video_comments_score;
                    $total_youtube_status = $total_video_performance * 0.8;
                    // NOTE: Video submitted will be later recorded unless businessman rating is submitted.
                    if($inside_value['accepted_interest_id'] > 0) { // If there is accepted interest id
                        if($this->businessman_rating($inside_value['accepted_interest_id']) != 0) { // Video submitted with Businessman Rating. If no Businessman Rating, default is 0.
                            $temp_video_with_rating_8 = $total_youtube_status + ($this->businessman_rating($inside_value['accepted_interest_id']) * 0.2);
                            $temp_music = $temp_music + $temp_video_with_rating_8;
                            ++$music_count_ratings; // Count videos with businessman ratings
                        } else {
                            $temp_music = $temp_music + $total_youtube_status;
                            ++$music_count_ratings; // Count videos with businessman ratings
                        }
                    } else {
                        $temp_music = $temp_music + $total_youtube_status;
                        ++$music_count_ratings; // Count videos with businessman ratings
                    }
                }
                break;
                case 7: { // Howto and Styles
                    $minmaxViews = (int)$range['minmax_values']['howto_and_styles'][0]['maxViewsCount'] - (int)$range['minmax_values']['howto_and_styles'][0]['minViewsCount'];
                    $view_count = (int)$inside_value['views_count'] - (int)$range['minmax_values']['howto_and_styles'][0]['minViewsCount'];
                    $video_views_score = ( ($view_count) * ( ( ($point_range) / ($minmaxViews) )  + 0 ) ) * $views_percentage;

                    // Likes - Dislikes
                    $min_reaction = (int)$range['minmax_values']['howto_and_styles'][0]['minLikesCount'] - (int)$range['minmax_values']['howto_and_styles'][0]['minDislikesCount'];
                    $minmaxReactions = ($range['minmax_values']['howto_and_styles'][0]['maxLikesCount'] - (int)$range['minmax_values']['howto_and_styles'][0]['maxDislikesCount']) - $min_reaction;
                    $current_reaction = (int)$inside_value['likes_count'] - (int)$inside_value['dislikes_count'];
                    $reaction_count = $current_reaction - $min_reaction;
                    $video_reactions_score = ( ($reaction_count) * ( ( ($point_range) / ($minmaxReactions) ) + 0 ) ) * $reactions_percentage;

                    $minmaxComments = $range['minmax_values']['howto_and_styles'][0]['maxCommentsCount'] - (int)$range['minmax_values']['howto_and_styles'][0]['minCommentsCount'];
                    $comment_count = (int)$inside_value['comments_count'] - (int)$range['minmax_values']['howto_and_styles'][0]['minCommentsCount'];
                    $video_comments_score = ( ($comment_count) * ( ( ($point_range) / ($minmaxComments) ) + 0 ) ) * $comments_percentage;

                    $total_video_performance = $video_views_score + $video_reactions_score + $video_comments_score;
                    $total_youtube_status = $total_video_performance * 0.8;

                    // NOTE: Video submitted will be later recorded unless businessman rating is submitted.
                    if($inside_value['accepted_interest_id'] > 0) { // If there is accepted interest id
                        if($this->businessman_rating($inside_value['accepted_interest_id']) != 0) { // Video submitted with Businessman Rating. If no Businessman Rating, default is 0.
                            $temp_video_with_rating_7 = $total_youtube_status + ($this->businessman_rating($inside_value['accepted_interest_id']) * 0.2);
                            $temp_howto_and_styles = $temp_howto_and_styles + $temp_video_with_rating_7;
                            ++$howto_and_styles_count_ratings; // Count videos with businessman ratings
                        } else {
                            $temp_howto_and_styles = $temp_howto_and_styles + $total_youtube_status;
                            ++$howto_and_styles_count_ratings; // Count videos with businessman ratings
                        }
                    } else {
                        $temp_howto_and_styles = $temp_howto_and_styles + $total_youtube_status;
                        ++$howto_and_styles_count_ratings; // Count videos with businessman ratings
                    }
                }
                break;
                case 6: { // Gaming
                    $minmaxViews = (int)$range['minmax_values']['gaming'][0]['maxViewsCount'] - (int)$range['minmax_values']['gaming'][0]['minViewsCount'];
                    $view_count = (int)$inside_value['views_count'] - (int)$range['minmax_values']['gaming'][0]['minViewsCount'];
                    $video_views_score = ( ($view_count) * ( ( ($point_range) / ($minmaxViews) )  + 0 ) ) * $views_percentage;
                    
                    // Likes - Dislikes
                    $min_reaction = (int)$range['minmax_values']['gaming'][0]['minLikesCount'] - (int)$range['minmax_values']['gaming'][0]['minDislikesCount'];
                    $minmaxReactions = ($range['minmax_values']['gaming'][0]['maxLikesCount'] - (int)$range['minmax_values']['gaming'][0]['maxDislikesCount']) - $min_reaction;
                    $current_reaction = (int)$inside_value['likes_count'] - (int)$inside_value['dislikes_count'];
                    $reaction_count = $current_reaction - $min_reaction;
                    $video_reactions_score = ( ($reaction_count) * ( ( ($point_range) / ($minmaxReactions) ) + 0 ) ) * $reactions_percentage;

                    $minmaxComments = $range['minmax_values']['gaming'][0]['maxCommentsCount'] - (int)$range['minmax_values']['gaming'][0]['minCommentsCount'];
                    $comment_count = (int)$inside_value['comments_count'] - (int)$range['minmax_values']['gaming'][0]['minCommentsCount'];
                    $video_comments_score = ( ($comment_count) * ( ( ($point_range) / ($minmaxComments) ) + 0 ) ) * $comments_percentage;

                    $total_video_performance = $video_views_score + $video_reactions_score + $video_comments_score;
                    $total_youtube_status = $total_video_performance * 0.8;
                    // NOTE: Video submitted will be later recorded unless businessman rating is submitted.
                    if($inside_value['accepted_interest_id'] > 0) { // If there is accepted interest id
                        if($this->businessman_rating($inside_value['accepted_interest_id']) != 0) { // Video submitted with Businessman Rating. If no Businessman Rating, default is 0.
                            $temp_video_with_rating_6 = $total_youtube_status + ($this->businessman_rating($inside_value['accepted_interest_id']) * 0.2);
                            $temp_gaming = $temp_gaming + $temp_video_with_rating_6;
                            ++$gaming_count_ratings; // Count videos with businessman ratings
                        } else {
                            $temp_gaming = $temp_gaming + $total_youtube_status;
                            ++$gaming_count_ratings; // Count videos with businessman ratings
                        }
                    } else {
                        $temp_gaming = $temp_gaming + $total_youtube_status;
                        ++$gaming_count_ratings; // Count videos with businessman ratings
                    }
                }
                break;
                case 5: { // Family
                    $minmaxViews = (int)$range['minmax_values']['family'][0]['maxViewsCount'] - (int)$range['minmax_values']['family'][0]['minViewsCount'];
                    $view_count = (int)$inside_value['views_count'] - (int)$range['minmax_values']['family'][0]['minViewsCount'];
                    $video_views_score = ( ($view_count) * ( ( ($point_range) / ($minmaxViews) )  + 0 ) ) * $views_percentage;

                    // Likes - Dislikes
                    $min_reaction = (int)$range['minmax_values']['family'][0]['minLikesCount'] - (int)$range['minmax_values']['family'][0]['minDislikesCount'];
                    $minmaxReactions = ($range['minmax_values']['family'][0]['maxLikesCount'] - (int)$range['minmax_values']['family'][0]['maxDislikesCount']) - $min_reaction;
                    $current_reaction = (int)$inside_value['likes_count'] - (int)$inside_value['dislikes_count'];
                    $reaction_count = $current_reaction - $min_reaction;
                    $video_reactions_score = ( ($reaction_count) * ( ( ($point_range) / ($minmaxReactions) ) + 0 ) ) * $reactions_percentage;

                    $minmaxComments = $range['minmax_values']['family'][0]['maxCommentsCount'] - (int)$range['minmax_values']['family'][0]['minCommentsCount'];
                    $comment_count = (int)$inside_value['comments_count'] - (int)$range['minmax_values']['family'][0]['minCommentsCount'];
                    $video_comments_score = ( ($comment_count) * ( ( ($point_range) / ($minmaxComments) ) + 0 ) ) * $comments_percentage;

                    $total_video_performance = $video_views_score + $video_reactions_score + $video_comments_score;
                    $total_youtube_status = $total_video_performance * 0.8;

                    // NOTE: Video submitted will be later recorded unless businessman rating is submitted.
                    if($inside_value['accepted_interest_id'] > 0) { // If there is accepted interest id
                        if($this->businessman_rating($inside_value['accepted_interest_id']) != 0) { // Video submitted with Businessman Rating. If no Businessman Rating, default is 0.
                            $temp_video_with_rating_5 = $total_youtube_status + ($this->businessman_rating($inside_value['accepted_interest_id']) * 0.2);
                            $temp_family = $temp_family + $temp_video_with_rating_5;
                            ++$family_count_ratings; // Count videos with businessman ratings
                        } else {
                            $temp_family = $temp_family + $total_youtube_status;
                            ++$family_count_ratings; // Count videos with businessman ratings
                        }
                    } else {
                        $temp_family = $temp_family + $total_youtube_status;
                        ++$family_count_ratings; // Count videos with businessman ratings
                    }
                }
                break;
                case 4: { // Entertainment
                    $minmaxViews = (int)$range['minmax_values']['entertainment'][0]['maxViewsCount'] - (int)$range['minmax_values']['entertainment'][0]['minViewsCount'];
                    $view_count = (int)$inside_value['views_count'] - (int)$range['minmax_values']['entertainment'][0]['minViewsCount'];
                    $video_views_score = ( ($view_count) * ( ( ($point_range) / ($minmaxViews) )  + 0 ) ) * $views_percentage;
                    
                    // Likes - Dislikes
                    $min_reaction = (int)$range['minmax_values']['entertainment'][0]['minLikesCount'] - (int)$range['minmax_values']['entertainment'][0]['minDislikesCount'];
                    $minmaxReactions = ($range['minmax_values']['entertainment'][0]['maxLikesCount'] - (int)$range['minmax_values']['entertainment'][0]['maxDislikesCount']) - $min_reaction;
                    $current_reaction = (int)$inside_value['likes_count'] - (int)$inside_value['dislikes_count'];
                    $reaction_count = $current_reaction - $min_reaction;
                    $video_reactions_score = ( ($reaction_count) * ( ( ($point_range) / ($minmaxReactions) ) + 0 ) ) * $reactions_percentage;

                    $minmaxComments = $range['minmax_values']['entertainment'][0]['maxCommentsCount'] - (int)$range['minmax_values']['entertainment'][0]['minCommentsCount'];
                    $comment_count = (int)$inside_value['comments_count'] - (int)$range['minmax_values']['entertainment'][0]['minCommentsCount'];
                    $video_comments_score = ( ($comment_count) * ( ( ($point_range) / ($minmaxComments) ) + 0 ) ) * $comments_percentage;

                    $total_video_performance = $video_views_score + $video_reactions_score + $video_comments_score;
                    $total_youtube_status = $total_video_performance * 0.8;
                    // NOTE: Video submitted will be later recorded unless businessman rating is submitted.
                    if($inside_value['accepted_interest_id'] > 0) { // If there is accepted interest id
                        if($this->businessman_rating($inside_value['accepted_interest_id']) != 0) { // Video submitted with Businessman Rating. If no Businessman Rating, default is 0.
                            $temp_video_with_rating_4 = $total_youtube_status + ($this->businessman_rating($inside_value['accepted_interest_id']) * 0.2);
                            $temp_entertainment = $temp_entertainment + $temp_video_with_rating_4;
                            ++$entertainment_count_ratings; // Count videos with businessman ratings
                        } else {
                            $temp_entertainment = $temp_entertainment + $total_youtube_status;
                            ++$entertainment_count_ratings; // Count videos with businessman ratings
                        }
                    } else {
                        $temp_entertainment = $temp_entertainment + $total_youtube_status;
                        ++$entertainment_count_ratings; // Count videos with businessman ratings
                    }
                }
                break;
                case 3: { // Education
                    $minmaxViews = (int)$range['minmax_values']['education'][0]['maxViewsCount'] - (int)$range['minmax_values']['education'][0]['minViewsCount'];
                    $view_count = (int)$inside_value['views_count'] - (int)$range['minmax_values']['education'][0]['minViewsCount'];
                    $video_views_score = ( ($view_count) * ( ( ($point_range) / ($minmaxViews) )  + 0 ) ) * $views_percentage;
                    
                    // Likes - Dislikes
                    $min_reaction = (int)$range['minmax_values']['education'][0]['minLikesCount'] - (int)$range['minmax_values']['education'][0]['minDislikesCount'];
                    $minmaxReactions = ($range['minmax_values']['education'][0]['maxLikesCount'] - (int)$range['minmax_values']['education'][0]['maxDislikesCount']) - $min_reaction;
                    $current_reaction = (int)$inside_value['likes_count'] - (int)$inside_value['dislikes_count'];
                    $reaction_count = $current_reaction - $min_reaction;
                    $video_reactions_score = ( ($reaction_count) * ( ( ($point_range) / ($minmaxReactions) ) + 0 ) ) * $reactions_percentage;

                    $minmaxComments = $range['minmax_values']['education'][0]['maxCommentsCount'] - (int)$range['minmax_values']['education'][0]['minCommentsCount'];
                    $comment_count = (int)$inside_value['comments_count'] - (int)$range['minmax_values']['education'][0]['minCommentsCount'];
                    $video_comments_score = ( ($comment_count) * ( ( ($point_range) / ($minmaxComments) ) + 0 ) ) * $comments_percentage;

                    $total_video_performance = $video_views_score + $video_reactions_score + $video_comments_score;
                    $total_youtube_status = $total_video_performance * 0.8;
                    // NOTE: Video submitted will be later recorded unless businessman rating is submitted.
                    if($inside_value['accepted_interest_id'] > 0) { // If there is accepted interest id
                        if($this->businessman_rating($inside_value['accepted_interest_id']) != 0) { // Video submitted with Businessman Rating. If no Businessman Rating, default is 0.
                            $temp_video_with_rating_3 = $total_youtube_status + ($this->businessman_rating($inside_value['accepted_interest_id']) * 0.2);
                            $temp_education = $temp_education + $temp_video_with_rating_3;
                            ++$education_count_ratings; // Count videos with businessman ratings
                        } else {
                            $temp_education = $temp_education + $total_youtube_status;
                            ++$education_count_ratings; // Count videos with businessman ratings
                        }
                    } else {
                        $temp_education = $temp_education + $total_youtube_status;
                        ++$education_count_ratings; // Count videos with businessman ratings
                    }
                }
                break;
                case 2: { // Drama
                    $minmaxViews = (int)$range['minmax_values']['drama'][0]['maxViewsCount'] - (int)$range['minmax_values']['drama'][0]['minViewsCount'];
                    $view_count = (int)$inside_value['views_count'] - (int)$range['minmax_values']['drama'][0]['minViewsCount'];
                    $video_views_score = ( ($view_count) * ( ( ($point_range) / ($minmaxViews) )  + 0 ) ) * $views_percentage;
                    
                    // Likes - Dislikes
                    $min_reaction = (int)$range['minmax_values']['drama'][0]['minLikesCount'] - (int)$range['minmax_values']['drama'][0]['minDislikesCount'];
                    $minmaxReactions = ($range['minmax_values']['drama'][0]['maxLikesCount'] - (int)$range['minmax_values']['drama'][0]['maxDislikesCount']) - $min_reaction;
                    $current_reaction = (int)$inside_value['likes_count'] - (int)$inside_value['dislikes_count'];
                    $reaction_count = $current_reaction - $min_reaction;
                    $video_reactions_score = ( ($reaction_count) * ( ( ($point_range) / ($minmaxReactions) ) + 0 ) ) * $reactions_percentage;

                    $minmaxComments = $range['minmax_values']['drama'][0]['maxCommentsCount'] - (int)$range['minmax_values']['drama'][0]['minCommentsCount'];
                    $comment_count = (int)$inside_value['comments_count'] - (int)$range['minmax_values']['drama'][0]['minCommentsCount'];
                    $video_comments_score = ( ($comment_count) * ( ( ($point_range) / ($minmaxComments) ) + 0 ) ) * $comments_percentage;

                    $total_video_performance = $video_views_score + $video_reactions_score + $video_comments_score;
                    $total_youtube_status = $total_video_performance * 0.8;
                    // NOTE: Video submitted will be later recorded unless businessman rating is submitted.
                    if($inside_value['accepted_interest_id'] > 0) { // If there is accepted interest id
                        if($this->businessman_rating($inside_value['accepted_interest_id']) != 0) { // Video submitted with Businessman Rating. If no Businessman Rating, default is 0.
                            $temp_video_with_rating_2 = $total_youtube_status + ($this->businessman_rating($inside_value['accepted_interest_id']) * 0.2);
                            $temp_drama = $temp_drama + $temp_video_with_rating_2;
                            ++$drama_count_ratings; // Count videos with businessman ratings
                        } else {
                            $temp_drama = $temp_drama + $total_youtube_status;
                            ++$drama_count_ratings; // Count videos with businessman ratings
                        }
                    } else {
                        $temp_drama = $temp_drama + $total_youtube_status;
                        ++$drama_count_ratings; // Count videos with businessman ratings
                    }
                }
                break;
                case 1: { // Comedy
                    $minmaxViews = (int)$range['minmax_values']['comedy'][0]['maxViewsCount'] - (int)$range['minmax_values']['comedy'][0]['minViewsCount'];
                    $view_count = (int)$inside_value['views_count'] - (int)$range['minmax_values']['comedy'][0]['minViewsCount'];
                    $video_views_score = ( ($view_count) * ( ( ($point_range) / ($minmaxViews) )  + 0 ) ) * $views_percentage;
                    
                    // Likes - Dislikes
                    $min_reaction = (int)$range['minmax_values']['comedy'][0]['minLikesCount'] - (int)$range['minmax_values']['comedy'][0]['minDislikesCount'];
                    $minmaxReactions = ($range['minmax_values']['comedy'][0]['maxLikesCount'] - (int)$range['minmax_values']['comedy'][0]['maxDislikesCount']) - $min_reaction;
                    $current_reaction = (int)$inside_value['likes_count'] - (int)$inside_value['dislikes_count'];
                    $reaction_count = $current_reaction - $min_reaction;
                    $video_reactions_score = ( ($reaction_count) * ( ( ($point_range) / ($minmaxReactions) ) + 0 ) ) * $reactions_percentage;

                    $minmaxComments = $range['minmax_values']['comedy'][0]['maxCommentsCount'] - (int)$range['minmax_values']['comedy'][0]['minCommentsCount'];
                    $comment_count = (int)$inside_value['comments_count'] - (int)$range['minmax_values']['comedy'][0]['minCommentsCount'];
                    $video_comments_score = ( ($comment_count) * ( ( ($point_range) / ($minmaxComments) ) + 0 ) ) * $comments_percentage;

                    $total_video_performance = $video_views_score + $video_reactions_score + $video_comments_score;
                    $total_youtube_status = $total_video_performance * 0.8;

                    // NOTE: Video submitted will be later recorded unless businessman rating is submitted.
                    if($inside_value['accepted_interest_id'] > 0) { // If there is accepted interest id
                        if($this->businessman_rating($inside_value['accepted_interest_id']) != 0) { // Video submitted with Businessman Rating. If no Businessman Rating, default is 0.
                            $temp_video_with_rating_1 = $total_youtube_status + ($this->businessman_rating($inside_value['accepted_interest_id']) * 0.2);
                            $temp_comedy = $temp_comedy + $temp_video_with_rating_1;
                            ++$comedy_count_ratings; // Count videos with businessman ratings
                        } else {
                            $temp_comedy = $temp_comedy + $total_youtube_status;
                            ++$comedy_count_ratings; // Count videos with businessman ratings
                        }
                    } else {
                        $temp_comedy = $temp_comedy + $total_youtube_status;
                        ++$comedy_count_ratings; // Count videos with businessman ratings
                    }

                }
                break;
            }

            if(++$index_count == $numItems) {
                if($comedy_count_ratings == 0) {
                    $temp_comedy = 0;
                } else {
                    $temp_comedy = $temp_comedy; // divide comedy_count_ratings
                }
                
                if($drama_count_ratings == 0) {
                    $temp_drama = 0;
                } else {
                    $temp_drama = $temp_drama;
                }

                if($education_count_ratings == 0) {
                    $temp_education = 0;
                } else {
                    $temp_education = $temp_education;
                }

                if($entertainment_count_ratings == 0) {
                    $temp_entertainment = 0;
                } else {
                    $temp_entertainment = $temp_entertainment;
                }

                if($family_count_ratings == 0) {
                    $temp_family = 0;
                } else {
                    $temp_family = $temp_family;
                }

                if($gaming_count_ratings == 0) {
                    $temp_gaming = 0;
                } else {
                    $temp_gaming = $temp_gaming;
                }

                if($howto_and_styles_count_ratings == 0) {
                    $temp_howto_and_styles = 0;
                } else {
                    $temp_howto_and_styles = $temp_howto_and_styles;
                }

                if($music_count_ratings == 0) {
                    $temp_music = 0;
                } else {
                    $temp_music = $temp_music;
                }
                
                if($sports_count_ratings == 0) {
                    $temp_sports = 0;
                } else {
                    $temp_sports = $temp_sports;
                }

                if($travel_and_events_count_ratings == 0) {
                    $temp_travel_and_events = 0;
                } else {
                    $temp_travel_and_events = $temp_travel_and_events;
                }

                if($people_and_blogs_count_ratings == 0) {
                    $temp_people_and_blogs = 0;
                } else {
                    $temp_people_and_blogs = $temp_people_and_blogs;
                }
            }
        }

        $data_returned = array(
            "comedy" => $temp_comedy,
            "drama" => $temp_drama,
            "education" => $temp_education,
            "entertainment" => $temp_entertainment,
            "family" => $temp_family,
            "gaming" => $temp_gaming,
            "howto_and_styles" => $temp_howto_and_styles,
            "music" => $temp_music,
            "sports" => $temp_sports,
            "travel_and_events" => $temp_travel_and_events,
            "people_and_blogs" => $temp_people_and_blogs
        );

        return $data_returned;
    }

    public function youtuber_profile_get($youtuber_id) {
        // if($this->input->is_ajax_request())
        // {
            // $range['minmax_values']['comedy'] = $this->mdyoutube_data->get_minmax_values_v2(1);
            // $range['minmax_values']['drama'] = $this->mdyoutube_data->get_minmax_values_v2(2);
            // $range['minmax_values']['education'] = $this->mdyoutube_data->get_minmax_values_v2(3);
            // $range['minmax_values']['entertainment'] = $this->mdyoutube_data->get_minmax_values_v2(4);
            // $range['minmax_values']['family'] = $this->mdyoutube_data->get_minmax_values_v2(5);
            // $range['minmax_values']['gaming'] = $this->mdyoutube_data->get_minmax_values_v2(6);
            // $range['minmax_values']['howto_and_styles'] = $this->mdyoutube_data->get_minmax_values_v2(7);
            // $range['minmax_values']['music'] = $this->mdyoutube_data->get_minmax_values_v2(8);
            // $range['minmax_values']['sports'] = $this->mdyoutube_data->get_minmax_values_v2(9);
            // $range['minmax_values']['travel_and_events'] = $this->mdyoutube_data->get_minmax_values_v2(10);
            // $range['minmax_values']['people_and_blogs'] = $this->mdyoutube_data->get_minmax_values_v2(11);

            $range['minmax_values']['comedy'] = $this->mdyoutube_data->get_minmax_values(1);
            $range['minmax_values']['drama'] = $this->mdyoutube_data->get_minmax_values(2);
            $range['minmax_values']['education'] = $this->mdyoutube_data->get_minmax_values(3);
            $range['minmax_values']['entertainment'] = $this->mdyoutube_data->get_minmax_values(4);
            $range['minmax_values']['family'] = $this->mdyoutube_data->get_minmax_values(5);
            $range['minmax_values']['gaming'] = $this->mdyoutube_data->get_minmax_values(6);
            $range['minmax_values']['howto_and_styles'] = $this->mdyoutube_data->get_minmax_values(7);
            $range['minmax_values']['music'] = $this->mdyoutube_data->get_minmax_values(8);
            $range['minmax_values']['sports'] = $this->mdyoutube_data->get_minmax_values(9);
            $range['minmax_values']['travel_and_events'] = $this->mdyoutube_data->get_minmax_values(10);
            $range['minmax_values']['people_and_blogs'] = $this->mdyoutube_data->get_minmax_values(11);

            $youtuber_profile['info'] = $this->mdyoutuber->get_youtuber_channel($youtuber_id);
            $youtuber_profile['info']['interests'] = $this->mdyoutuber->get_mediakit_interests($youtuber_profile['info']['id']);
            
            // $videos = $this->mdyoutuber->get_videos_v2($youtuber_id);
            $videos = $this->mdyoutuber->get_videos($youtuber_id);
            if(!empty($videos)) {

                $data_returned = $this->reputation_computation($videos);

                $youtuber_profile['reputation_score'][0]['name'] = 'Comedy';
                $youtuber_profile['reputation_score'][0]['points'] = $data_returned['comedy'];
                $youtuber_profile['reputation_score'][0]['photo'] = './../../../../assets/img/category_icons/png/011-theater.png';

                $youtuber_profile['reputation_score'][1]['name'] = 'Drama';
                $youtuber_profile['reputation_score'][1]['points'] = $data_returned['drama'];
                $youtuber_profile['reputation_score'][1]['photo'] = './../../../../assets/img/category_icons/png/008-cinema.png';

                $youtuber_profile['reputation_score'][2]['name'] = 'Education';
                $youtuber_profile['reputation_score'][2]['points'] = $data_returned['education'];
                $youtuber_profile['reputation_score'][2]['photo'] = './../../../../assets/img/category_icons/png/009-book.png';

                $youtuber_profile['reputation_score'][3]['name'] = 'Entertainement';
                $youtuber_profile['reputation_score'][3]['points'] = $data_returned['entertainment'];
                $youtuber_profile['reputation_score'][3]['photo'] = './../../../../assets/img/category_icons/png/001-computer.png';

                $youtuber_profile['reputation_score'][4]['name'] = 'Family';
                $youtuber_profile['reputation_score'][4]['points'] = $data_returned['family'];
                $youtuber_profile['reputation_score'][4]['photo'] = './../../../../assets/img/category_icons/png/007-mother.png';

                $youtuber_profile['reputation_score'][5]['name'] = 'Gaming';
                $youtuber_profile['reputation_score'][5]['points'] = $data_returned['gaming'];
                $youtuber_profile['reputation_score'][5]['photo'] = './../../../../assets/img/category_icons/png/005-gamepad.png';

                $youtuber_profile['reputation_score'][6]['name'] = 'Howto And Styles';
                $youtuber_profile['reputation_score'][6]['points'] = $data_returned['howto_and_styles'];
                $youtuber_profile['reputation_score'][6]['photo'] = './../../../../assets/img/category_icons/png/010-tie.png';

                $youtuber_profile['reputation_score'][7]['name'] = 'Music';
                $youtuber_profile['reputation_score'][7]['points'] = $data_returned['music'];
                $youtuber_profile['reputation_score'][7]['photo'] = './../../../../assets/img/category_icons/png/004-audio.png';

                $youtuber_profile['reputation_score'][8]['name'] = 'Sports';
                $youtuber_profile['reputation_score'][8]['points'] = $data_returned['sports'];
                $youtuber_profile['reputation_score'][8]['photo'] = './../../../../assets/img/category_icons/png/003-soccer.png';

                $youtuber_profile['reputation_score'][9]['name'] = 'Travel And Events';
                $youtuber_profile['reputation_score'][9]['points'] = $data_returned['travel_and_events'];
                $youtuber_profile['reputation_score'][9]['photo'] = './../../../../assets/img/category_icons/png/002-plane.png';

                $youtuber_profile['reputation_score'][10]['name'] = 'People And Blogs';
                $youtuber_profile['reputation_score'][10]['points'] = $data_returned['people_and_blogs'];
                $youtuber_profile['reputation_score'][10]['photo'] = './../../../../assets/img/category_icons/png/006-wrench.png';
            
                $youtuber_registered = $this->mdyoutuber->get_youtuber($youtuber_id);
                if(!empty($youtuber_registered)) {
                    $youtuber_exist = $this->mdyoutuber->get_youtuber_exist_on_recommend($youtuber_id);
                    if(!empty($youtuber_exist)) { // Update Youtuber Reputation Score
                        for($i = 0; $i < 11; $i++) {
                            $data_update_points = array(
                                "points"            => $youtuber_profile['reputation_score'][$i]['points']
                            );
                            
                            $category_id = $i + 1;
                            // Youtuber ID and Category ID to validate then update
                            $this->mdyoutuber->update_youtuber_reputation_score($youtuber_id, $category_id, $data_update_points);
                        }
                    } else { // Add Youtuber Reputation Score
                        for($i = 0; $i < 11; $i++) {
                            $data_user_youtuber = array(
                                "youtuber_id"       => $youtuber_id,
                                "category_id"       => $i + 1,
                                "points"            => $youtuber_profile['reputation_score'][$i]['points']
                            );
                            $this->mdyoutuber->add_youtuber_reputation_score($data_user_youtuber);
                        }
                    }
                }
            }
            
            $this->response_data['data'] = $youtuber_profile;
            $this->response_data['data']['videos'] = $videos;
            $this->response_data['data']['range'] = $range; // Reserve for Presentation
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'YouTuber Profile';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        // }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function remove_youtuber_delete($youtuber_id) {
        if($this->input->is_ajax_request())
        {
            $data_youtuber = $this->mdyoutuber->remove_youtuber($youtuber_id);

            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Youtuber Successfully Removed';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function youtuber_report_list_get($youtuber_id) {
        if($this->input->is_ajax_request())
        {
            $data_reports = $this->mdyoutuber->get_youtuber_reports($youtuber_id);

            if(!empty($data_reports)) {
                foreach($data_reports as $inside_key => $inside_value) {
                    $photo_campaign = $this->mdcampaign->get_single_photo_display_feed((int)$inside_value['campaign_id']);
                    $data_reports[$inside_key]['photo'] = profile_picture($photo_campaign['photo']);
                }
            }

            $this->response_data['data'] = $data_reports;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Youtuber reports retrived';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function youtuber_report_details_get($accepted_interest_id) {
        if($this->input->is_ajax_request())
        {
            $id = $this->mdyoutuber->get_youtuber_report_details((int)$accepted_interest_id);

            $data_report_details = array(
                "accepted_interest_id"      => $id['accepted_interest_id'],
                "submission_status"         => $id['submission_status'],
                "video"                     => $this->mdyoutuber->get_video_details($id['video_id']),
                "businessman"               => $this->mdbusinessman->get_specific_businessman($id['businessman_id']),
                "businessman_rating"        => $this->mdbusinessman->get_businessman_rating($id['businessman_rating_id'])
            );

            $data_report_details['businessman_rating']['engaging'] = (int)$data_report_details['businessman_rating']['engaging'];
            $data_report_details['businessman_rating']['credibility'] = (int)$data_report_details['businessman_rating']['credibility'];
            $data_report_details['businessman_rating']['impression'] = (int)$data_report_details['businessman_rating']['impression'];
            $data_report_details['businessman_rating']['action_oriented'] = (int)$data_report_details['businessman_rating']['action_oriented'];
            $data_report_details['businessman_rating']['significance'] = (int)$data_report_details['businessman_rating']['significance'];
            $data_report_details['businessman_rating']['integrated'] = (int)$data_report_details['businessman_rating']['integrated'];
            $data_report_details['businessman_rating']['brand_service'] = (int)$data_report_details['businessman_rating']['brand_service'];
            $data_report_details['businessman_rating']['brand_innovation'] = (int)$data_report_details['businessman_rating']['brand_innovation'];
            $data_report_details['businessman_rating']['brand_quality'] = (int)$data_report_details['businessman_rating']['brand_quality'];

            $businessman_photo = profile_picture($data_report_details['businessman']['profile_picture']);
            $data_report_details['businessman']['profile_picture'] = $businessman_photo;

            $data_campaign = $this->mdcampaign->get_campaign_details($id['campaign_id']);

            if(!empty($data_campaign)) {
                $data_campaign['photos'] = $this->mdcampaign->get_photo($id['campaign_id']);
                if(!empty($data_campaign['photos'])) {
                    foreach($data_campaign['photos'] as $in_key => $in_value) {
                        $data_campaign['photos'][$in_key]['photo'] = profile_picture($in_value['photo']);
                    }
                }
                $data_campaign['advertising_details'] = $this->mdcampaign->get_advertising_details($id['campaign_id']);
            }

            $data_report_details['campaign'] = $data_campaign;

            $this->response_data['data'] = $data_report_details;
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Youtuber report details retrived';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }   
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function youtuber_video_existed_get($video_id) {
        $video = $this->mdyoutuber->get_video_existed($video_id);

        if(!empty($video)) {
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Video existed already.';
            $this->response($this->response_data['message'], REST_Controller::HTTP_OK);
        } else {
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Video can be uploaded, video did not exist yet.';
        }
        return;
    }

    public function display_hot_campaigns_get($youtuber_id)
    {
        $data_campaign = $this->mdhotcampaign->get_hot_campaigns();
        $data_final = array();

        if(!empty($data_campaign)) {
            foreach($data_campaign as $inside_key => $inside_value) {
                $accepted_interest_in_campaign = $this->mdhotcampaign->accepted_interest_count((int)$inside_value['campaign_id']);
                // Calculates current_advertiser is greater than 80% of advertiser_needed to a campaign
                if((int)$accepted_interest_in_campaign['count_interested'] >= ((int)$inside_value['advertiser_needed'] * 0.8)) {
                    array_push($data_final, $inside_value);
                }
            }
        }

        foreach($data_final as $in_key => $in_value) {
            $data_final[$in_key]['photo'] = profile_picture($in_value['photo']);
            $accepted = $this->mdinterest->interested_get($in_value['campaign_id'], $youtuber_id);
            if(!empty($accepted)) {
                $data_final[$in_key]['interested'] = 1;
            } else {
                $data_final[$in_key]['interested'] = 0;
            }
            $accepted_interest = $this->mdinterest->accepted_interest_get($in_value['campaign_id'], $youtuber_id);
            if(!empty($accepted_interest)){ // Interest has been accepted by a businessman
                $data_final[$in_key]['accepted'] = 'yes';
            } else {
                $data_final[$in_key]['accepted'] = 'no';
            }
        }

        $this->response_data['data'] = $data_final;
        $this->response_data['error'] = 0;
        $this->response_data['message'] = 'List of Hot Campaigns';
        
        $this->response($this->response_data);
        return;
    }

    //TOP INFLUENCER

    public function top_influencer_get() {
        // if($this->input->is_ajax_request())
        // {    
            $category_id = $this->mdyoutuber->get_category_top_influencer();

            foreach($category_id as $inside_key => $inside_value) {
                $category_id[$inside_key]['youtuber'] = $this->mdyoutuber->get_top_influencer((int)$inside_value['category_id']);
                $category_id[$inside_key]['youtuber']['points'] = (double)$category_id[$inside_key]['youtuber']['points'];
            }


            $this->response_data['data'] = $category_id;

            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'YouTuber Profile';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        // }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }
    public function add_youtuber_refer_post()
    {   
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $_POST = (array)json_decode($post_json, TRUE);
            $post_data = $this->input->post();

            //insert user_youtuber
            $data_refer = array(
                    "youtuber_id"        => $post_data['youtuber_id'],
                    "youtuber_referred"  => $post_data['youtuber_referred'],
                    "referred_status"    => $post_data['referred_status'],
                    "campaign_id"        => $post_data['campaign_id'],
                    "date_added"         => date('Y-m-d H:i:s')
                    
            );

            $this->mdyoutuber->add_youtuber_refer($data_refer);
            $this->response_data['error']   = 0;
            $this->response_data['message'] = 'Referred Youtuber'; 

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;

        } else{

            $this->response_data['message'] = $auth_response['message'];
        }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }
    

   
   
       
    public function add_sync_youtube_data_post()
    {
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $_POST = (array)json_decode($post_json, TRUE);
            $post_data = $this->input->post();

            // Validate post data
            $this->load->library("form_validation");

            $this->form_validation->set_rules("videoId","VideoId","required");
            $this->form_validation->set_rules("category_id","CategoryId","required");

            if($this->form_validation->run())
            {
                $this->load->library("bcrypt");
                //insert data_user_businessman
                $youtube_data = array(
                            "videoId"           => $post_data['videoId'],
                            "category_id"       => $post_data['category_id'],
                            "likeCount"         => $post_data['likeCount'],
                            "dislikeCount"      => $post_data['dislikeCount'],
                            "commentCount"      => $post_data['commentCount'],
                            "viewCount"         => $post_data['viewCount']
                        );

                $this->mdyoutube_data->sync_youtube_data($youtube_data);
                $this->response_data['error']   = 0;
                $this->response_data['message'] = 'Youtube Data has been successfully synced!'; 
            } else{
                $this->response_data['message'] = validation_errors();
            }

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function add_youtuber_video_post() // Video entry
    {
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $_POST = (array)json_decode($post_json, TRUE);
            $post_data = $this->input->post();

            // Validate post data
            $this->load->library("form_validation");

            $this->form_validation->set_rules("video_id","VideoId","required");
            $this->form_validation->set_rules("category_id","Category","required");
            $this->form_validation->set_rules("channel_id","Channel","required");
            $this->form_validation->set_rules("video_title","Title","required");
            $this->form_validation->set_rules("comments_count","Comment_coung","required");
            $this->form_validation->set_rules("dislikes_count","Dislike_count","required");
            $this->form_validation->set_rules("likes_count","Like_count","required");
            $this->form_validation->set_rules("views_count","View_count","required");
            $this->form_validation->set_rules("youtuber_id","Youtuber_id","required");

            if($this->form_validation->run())
            {
                $youtube_data = array(
                            "video_id"          => $post_data['video_id'],
                            "video_title"       => $post_data['video_title'],
                            "video_description" => $post_data['video_description'],
                            "channel_id"        => $post_data['channel_id'],
                            "channel_title"     => $post_data['channel_title'],
                            "category_id"       => $post_data['category_id'],
                            "default_thumbnail" => $post_data['default_thumbnail'],
                            "views_count"       => $post_data['views_count'],
                            "likes_count"       => $post_data['likes_count'],
                            "dislikes_count"    => $post_data['dislikes_count'],
                            "comments_count"    => $post_data['comments_count'],
                            "youtuber_id"       => $post_data['youtuber_id']
                        );

                $this->response_data['data'] = $this->mdyoutube_data->add_youtube_video($youtube_data);
                $this->response_data['error']   = 0;
                $this->response_data['message'] = 'Youtuber Video has been successfully added!';
            } else{
                $this->response_data['message'] = validation_errors();
            }

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }
        return;
    }

    public function youtuber_channel_existence_get($youtube_channel)
    {
        if($this->input->is_ajax_request())
        {
            $existed = $this->mdyoutube_data->get_youtube_channel_existence($youtube_channel);

            if($existed) {
                $this->response_data['data'] = 1;
            } else {
                $this->response_data['data'] = 0;
            }

            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Existence';

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
    
        } else {
            $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
            return;
        }    
    }

    public function add_youtube_data_post() {
        $post_json = $this->security->xss_clean($this->input->raw_input_stream);
        if($this->input->is_ajax_request() && $post_json !== FALSE)
        {
            $_POST = (array)json_decode($post_json, TRUE);
            $post_data = $this->input->post();

            // Validate post data
            $this->load->library("form_validation");

            $this->form_validation->set_rules("video_id","Video ID","required");

            if($this->form_validation->run())
            {
                $this->load->library("bcrypt");
                //insert data_user_businessman
                $youtube_data = array(
                            "video_id"      => $post_data['video_id'],
                            "likes"         => $post_data['likeCount'],
                            "dislikes"      => $post_data['dislikeCount'],
                            "views"         => $post_data['viewCount'],
                            "date_added"    => date("Y-m-d")
                        );

                $this->mdyoutube_data->youtube_data($youtube_data);
                $this->response_data['error']   = 0;
                $this->response_data['message'] = 'Youtube Data has been successfully synced!'; 
            } else{
                $this->response_data['message'] = validation_errors();
            }

            $this->response($this->response_data, REST_Controller::HTTP_OK);
            return;
        }

        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function update_youtuber_video_put($video_id)
    {
        $auth_response = Authorization::validateToken();
        if($auth_response['error'] == 0)
        {
            $token = (array) $auth_response['token'];
            $put_data = $this->put();

            $data_youtube_video = array(
                "views_count"       => $put_data['views_count'],
                "likes_count"       => $put_data['likes_count'],
                "dislikes_count"    => $put_data['dislikes_count'],
                "comments_count"    => $put_data['comments_count']
            );

            $this->mdyoutube_data->update_youtuber_video($video_id, $data_youtube_video);
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Youtube video sucessfully updated';
            $this->response($this->response_data, REST_Controller::HTTP_OK);
        }
    }

    public function remove_all_video_youtube_data_delete() {
        if($this->input->is_ajax_request())
        {
            $auth_response = Authorization::validateToken();
            if($auth_response['error']==0)
            {
                $data_from_post = (array)$auth_response['token'];

                $data_youtube_data = $this->mdyoutube_data->remove_youtube_data();

                $this->response_data['error'] = 0;
                $this->response_data['message'] = 'Youtube Data Successfully Removed';

                $this->response($this->response_data, REST_Controller::HTTP_OK);
                return;
            }else
            {
                $this->response_data['message'] = $auth_response['message'];
            }
        }
        $this->response($this->response_data, REST_Controller::HTTP_FORBIDDEN);
        return;
    }

    public function youtube_data_video_existed_get($video_id) {
        $video = $this->mdyoutube_data->get_video_existed($video_id);

        if(!empty($video)) {
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Video existed already.';
        } else {
            $this->response_data['error'] = 0;
            $this->response_data['message'] = 'Video can be uploaded, video did not exist yet.';
        }

        $this->response($this->response_data, REST_Controller::HTTP_OK);
        return;
    }
        


}