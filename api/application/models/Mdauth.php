<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * MDAUTH
 * 
 * @extends CI_Model
*/
class Mdauth extends CI_Model {

	var $response = false;

	public function __construct() 
	{
		parent::__construct();
	}

	public function check_user_login($identity,$password)
    {
        $businessman = BUSINESSMAN;

		$this->db->select("u.businessman_id,u.username,u.password");
        $this->db->from("{$businessman} u");
		$this->db->where('u.username', $identity);

        $result = $this->db->get()->first_row('array');

        if(!empty($result))
		{
			$this->load->library("bcrypt");
			if($this->bcrypt->verify($password, $result['password']))
			{
				$this->response = $result;
			}
		}
		return $this->response;
	}
	
	public function check_admin_login($identity,$password)
    {
        $admin = ADMIN;

		$this->db->select("a.admin_id,a.username,a.password");
        $this->db->from("{$admin} a");
		$this->db->where('a.username', $identity);
		$this->db->where('a.password', $password);

        $result = $this->db->get()->first_row('array');

		return $result;
	}
	
	public function edit_weight($criteria_id, $data = array()) {
        if(!empty($data)) {
            $this->db->where("criteria_id", $criteria_id);
            $this->response = $this->db->update(CRITERIA, $data);
        }
	}
	
	public function get_all_criteria()
    {
		$this->db->select("*");
        $this->db->from(CRITERIA);

        $this->response = $this->db->get()->result_array();
        return $this->response;
	}

}