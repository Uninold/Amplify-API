<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mdinterest extends CI_Model {

	public function interested_post($data=array()){
        $youtuber_interest = YOUTUBER_INTEREST;
        if(!empty($data))
        {
            $this->db->select('*');
            $this->db->from($youtuber_interest);
            $this->db->where("campaign_id", $data['campaign_id']);
            $this->db->where("youtuber_id", $data['youtuber_id']);

            $this->response = $this->db->get()->result_array();

            if(empty($this->response)){
                $this->db->insert(YOUTUBER_INTEREST,$data);
                $this->response = $this->db->insert_id();
            }
            else{
                $this->db->where("campaign_id", $data['campaign_id']);
                $this->db->where("youtuber_id", $data['youtuber_id']);
                $this->db->delete(YOUTUBER_INTEREST);
            }
        }
        return $this->response;
    }

    public function post_accepted_interest($data=array()) {
    	$accepted_interest = ACCEPTED_INTEREST;
    	$youtuber_interest = YOUTUBER_INTEREST;

    	if(!empty($data))
        {
        	$this->db->select("*");
            $this->db->from("{$accepted_interest} ai");
            $this->db->where("ai.campaign_id", $data['campaign_id']);
            $this->db->where("ai.youtuber_id", $data['youtuber_id']);

            $list_accepted_interest = $this->db->get()->result_array();

            if(empty($list_accepted_interest)) {
            	// Accepting/Inserting youtuber in table accepted_interest
            	$this->db->insert(ACCEPTED_INTEREST,$data);
            	$this->response = 1;
            } else {
            	$this->db->where("campaign_id", $data['campaign_id']);
                $this->db->where("youtuber_id", $data['youtuber_id']);
                $this->db->delete(ACCEPTED_INTEREST);
                $this->response = 0;
            }
        }
        return $this->response;
    }

    public function delete_interest($data=array()) {
    	$youtuber_interest = YOUTUBER_INTEREST;

    	if(!empty($data))
        {
        	$this->db->select("*");
            $this->db->from("{$youtuber_interest} yi");
            $this->db->where("yi.campaign_id", $data['campaign_id']);
            $this->db->where("yi.youtuber_id", $data['youtuber_id']);

            $list_accepted_interest = $this->db->get()->result_array();

            if(!empty($list_accepted_interest)) {
            	$this->db->where("campaign_id", $data['campaign_id']);
                $this->db->where("youtuber_id", $data['youtuber_id']);
                $this->db->delete(YOUTUBER_INTEREST);
                $this->response = 0;
            }
        }
        return $this->response;
    }

    public function get_specific_campaign($businessman_id) { // Specific Campaign
        $campaign = CAMPAIGN;

        $this->db->select('*');
        $this->db->from("{$campaign} c");
        $this->db->where("c.businessman_id", $businessman_id);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }
    
    public function get_accepted_youtuber($campaign_id){
        $accepted_interest = ACCEPTED_INTEREST;
        $youtuber = YOUTUBER;
        $mediakit = MEDIAKIT;
        
        $this->db->select("
            ai.accepted_interest_id,
            m.youtuber_id,
            ai.campaign_id,
            m.firstName,
            m.lastName,
            ai.message,
            ai.date_interested,
            y.photoUrl,
            ai.israted
            ");
        $this->db->from("{$accepted_interest} ai");
        $this->db->join("{$youtuber} y", "ai.youtuber_id = y.youtuber_id");
        $this->db->join("{$mediakit} m", "m.youtuber_id = y.youtuber_id");
        $this->db->where("ai.campaign_id", $campaign_id);

        $this->response = $this->db->get()->result_array();

        return $this->response;
    }

    public function get_interested_youtuber($campaign_id){
        $youtuber_interest = YOUTUBER_INTEREST;
        $youtuber = YOUTUBER;
        $mediakit = MEDIAKIT;
        
        $this->db->select("
            yi.youtuber_interest_id,
            m.youtuber_id,
            yi.campaign_id,
            m.firstName,
            m.lastName,
            yi.message,
            yi.date_interested,
            y.photoUrl,
            ");
        $this->db->from("{$youtuber_interest} yi");
        $this->db->join("{$youtuber} y", "yi.youtuber_id = y.youtuber_id");
        $this->db->join("{$mediakit} m", "m.youtuber_id = y.youtuber_id");
        $this->db->where("yi.campaign_id", $campaign_id);

        $this->response = $this->db->get()->result_array();

        return $this->response;
    }

    public function get_current_advertisers_production($campaign_id) {
        $accepted_interest = ACCEPTED_INTEREST;
        $submission_status = SUBMISSION_STATUS;
        $youtuber = YOUTUBER;
        $mediakit = MEDIAKIT;

        $this->db->select("
            ai.accepted_interest_id,
            m.youtuber_id,
            ai.campaign_id,
            m.firstName,
            m.lastName,
            y.photoUrl,
            ss.submission_status,
            ai.israted,
            ai.withvideo
            ");
        $this->db->from("{$accepted_interest} ai");
        $this->db->join("{$submission_status} ss", "ss.submission_status_id = ai.submission_status_id");
        $this->db->join("{$youtuber} y", "ai.youtuber_id = y.youtuber_id");
        $this->db->join("{$mediakit} m", "m.youtuber_id = y.youtuber_id");
        $this->db->where("ai.campaign_id", $campaign_id);

        $this->response = $this->db->get()->result_array();

        return $this->response;
    }

    public function interested_get($campaign_id, $youtuber_id){ // If youtuber interest to campaign exist in table youtuber_interest.
        $youtuber_interest = YOUTUBER_INTEREST;

        $this->db->select('*');
        $this->db->from($youtuber_interest);
        $this->db->where("campaign_id", $campaign_id);
        $this->db->where("youtuber_id", $youtuber_id);

        $this->response = $this->db->get()->first_row('array');

        return $this->response;
    }

    public function accepted_interest_get($campaign_id, $youtuber_id){ // If businessman accepted youtuber exist in table accepted_interest.
    	$accepted_interest = ACCEPTED_INTEREST;

    	$this->db->select('*');
    	$this->db->from($accepted_interest);
    	$this->db->where("campaign_id", $campaign_id);
    	$this->db->where("youtuber_id", $youtuber_id);

    	$this->response = $this->db->get()->first_row('array');

    	return $this->response;
    }

    public function youtuber_my_campaigns_get($youtuber_id){ //Get campaigns working on by the youtuber.
        $accepted_interest = ACCEPTED_INTEREST;
        $submission_status = SUBMISSION_STATUS;
        $campaign = CAMPAIGN;
        // $campaign_status = CAMPAIGN_STATUS;

        $this->db->select("
            ai.accepted_interest_id,
            ai.campaign_id,
            ss.submission_status,
            c.campaign_status_id");
        $this->db->from("{$accepted_interest} ai");
        $this->db->join("{$submission_status} ss", "ss.submission_status_id = ai.submission_status_id");
        // $this->db->join("{$campaign_status} cs", "cs.campaign_status_id = ss.campaign_status_id");
        $this->db->join("{$campaign} c", "c.campaign_id = ai.campaign_id");
        $this->db->where("youtuber_id", $youtuber_id);
        $this->response = $this->db->get()->result_array();


        return $this->response;
    }
    
    public function youtuber_my_campaigns_by_production_get($youtuber_id){ //Get campaigns working on by the youtuber.
        $accepted_interest = ACCEPTED_INTEREST;
        $submission_status = SUBMISSION_STATUS;
        $campaign = CAMPAIGN;
        // $campaign_status = CAMPAIGN_STATUS;
        $video = VIDEO;

        $this->db->select("
            ai.accepted_interest_id,
            ai.campaign_id,
            ss.submission_status,
            c.campaign_status_id");
        $this->db->from("{$accepted_interest} ai");
        $this->db->join("{$submission_status} ss", "ss.submission_status_id = ai.submission_status_id");
        // $this->db->join("{$campaign_status} cs", "cs.campaign_status_id = ss.campaign_status_id");
        $this->db->join("{$campaign} c", "c.campaign_id = ai.campaign_id");
        // $this->db->join("{$video} v", "v.accepted_interest_id = ai.accepted_interest_id");
        $this->db->where("youtuber_id", $youtuber_id);
        // $this->db->order_by("v.date_accomplished","desc");
        $this->response = $this->db->get()->result_array();


        return $this->response;
    }
    public function youtuber_my_campaigns_by_budget_get($youtuber_id,$starting_budget,$ending_budget ){ //Get campaigns working on by the youtuber.
        $accepted_interest = ACCEPTED_INTEREST;
        $submission_status = SUBMISSION_STATUS;
        $campaign = CAMPAIGN;
        // $campaign_status = CAMPAIGN_STATUS;

        $this->db->select("
            ai.accepted_interest_id,
            ai.campaign_id,
            ss.submission_status,
            c.campaign_status_id");
        $this->db->from("{$accepted_interest} ai");
        $this->db->join("{$submission_status} ss", "ss.submission_status_id = ai.submission_status_id");
        // $this->db->join("{$campaign_status} cs", "cs.campaign_status_id = ss.campaign_status_id");
        $this->db->join("{$campaign} c", "c.campaign_id = ai.campaign_id");
        $this->db->where("youtuber_id", $youtuber_id);
        $this->db->where("c.starting_budget >= $starting_budget");
        $this->db->where("c.ending_budget <= $ending_budget ");
                // $this->db->order_by("c.project_name", "desc"); 
        $this->response = $this->db->get()->result_array();


        return $this->response;
    }
    public function businessman_my_campaigns_get($campaign_id, $youtuber_id){ //Get campaigns working on by the youtuber.
        $accepted_interest = ACCEPTED_INTEREST;

        $this->db->select('*');
        $this->db->from($accepted_interest);
        $this->db->where("youtuber_id", $youtuber_id);
        $this->db->where("campaign_id", $campaign_id);
        $this->response = $this->db->get()->first_row('array');

        return $this->response;
    }

    public function get_youtuber_submitted_video($youtuber_id, $campaign_id) { // Get details of submitted videp
        $video = VIDEO;
        $accepted_interest = ACCEPTED_INTEREST;

        $this->db->select();
        $this->db->from("{$accepted_interest} ai");
        $this->db->join("{$video} vid", "vid.accepted_interest_id = ai.accepted_interest_id");
        $this->db->where("vid.youtuber_id", $youtuber_id);
        $this->db->where("ai.campaign_id", $campaign_id);
        $this->response = $this->db->get()->first_row('array');

        return $this->response;
    }

    public function update_campaign($campaign_id, $data = array()) {
        if(!empty($data)) {
            $this->db->where("campaign_id", $campaign_id);
            $this->response = $this->db->update(CAMPAIGN, $data);
        }
    }

    public function set_submission_status($youtuber_id, $campaign_id, $data = array()) {
        if(!empty($data)) {
            $this->db->where("campaign_id", $campaign_id);
            $this->db->where("youtuber_id", $youtuber_id);
            $this->response = $this->db->update(ACCEPTED_INTEREST, $data);
        }
    }
}