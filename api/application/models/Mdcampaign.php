<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * MDAUTH
 * 
 * @extends CI_Model
*/
class Mdcampaign extends CI_Model {

	public function post_campaign($data=array()){
        if(!empty($data))
        {
            $this->db->insert(CAMPAIGN,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }

    public function post_product_photo_campaign($data=array()){
        if(!empty($data))
        {
            $this->db->insert(CAMPAIGN_PHOTO,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }

    public function post_advertising_detail($data=array()){
        if(!empty($data))
        {
            $this->db->insert(ADVERTISING_DETAILS,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }

    public function remove_campaign($campaign_id){
        $this->db->where("campaign_id", $campaign_id);
        $this->db->delete(CAMPAIGN);
    }

    public function remove_campaign_photo($campaign_photo_id) {
        $this->db->where("campaign_photo_id", $campaign_photo_id);
        $this->db->delete(CAMPAIGN_PHOTO);
    }

    public function remove_advertising_detail($advertising_detail_id) {
        $this->db->where("advertising_details_id", $advertising_detail_id);
        $this->db->delete(ADVERTISING_DETAILS);
    }

    public function add_to_on_going_production($campaign_id, $data = array()) {
        if(!empty($data)) {
            $this->db->where("campaign_id", $campaign_id);
            $this->response = $this->db->update(CAMPAIGN, $data);
        }
    }

    public function update_campaign($campaign_id, $data = array()) {
        if(!empty($data)) {
            $this->db->where("campaign_id", $campaign_id);
            $this->response = $this->db->update(CAMPAIGN, $data);
        }
    }

    public function get_current_no_advertisers($campaign_id) {
        $youtuber_interest = YOUTUBER_INTEREST;

        $this->db->select("count(yi.campaign_id) as count_youtuber");
        $this->db->from("{$youtuber_interest} yi");
        $this->db->where("yi.campaign_id", $campaign_id);

        $this->response = $this->db->get()->row_array();
        return $this->response;
    }

    public function get_current_accepted_advertisers($campaign_id) {
        $accepted_interest = ACCEPTED_INTEREST;

        $this->db->select("count(ai.campaign_id) as count_youtuber");
        $this->db->from("{$accepted_interest} ai");
        $this->db->where("ai.campaign_id", $campaign_id);

        $this->response = $this->db->get()->row_array();
        return $this->response;
    }

    public function get_businessman_campaigns($businessman_id){ // All Campaigns of a businessman
        $campaign = CAMPAIGN;

        $this->db->select("
            c.campaign_id,
            c.project_name,
            c.project_description,
            c.production_deadline,
            c.campaign_deadline,
            c.advertiser_needed,
            c.product_photo,
            c.date_added
            ");
        $this->db->from("{$campaign} c");
        $this->db->where("c.businessman_id", $businessman_id);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }

    public function get_specific_campaign_project_name($project_name) {
        // After campaign details added, campaign_id is returned and used for product_photos and advertising_details 

    	$campaign = CAMPAIGN;
    	
    	$this->db->select("c.campaign_id,c.project_name");
    	$this->db->from("{$campaign} c");
    	$this->db->where("c.project_name", $project_name);

    	$this->response = $this->db->get()->row_array();
        return $this->response;
    }

    public function get_all_campaigns_businessman($businessman_id){//get mycampaigns of a businessman connected with category and businessman
        $campaign = CAMPAIGN;
        $category = CATEGORY;
        $businessman = BUSINESSMAN;
        $this->db->select("c.campaign_id, c.project_name, c.project_description, c.starting_budget, c.ending_budget, c.production_deadline, c.campaign_deadline, c.video_duration, c.advertiser_needed, b.business_name, b.company_address, b.email_address, ct.category_name, c.date_added");
        $this->db->from("{$campaign} c");
        $this->db->join("{$category} ct","c.category_id = ct.category_id");
        $this->db->join("{$businessman} b","c.businessman_id = b.businessman_id");
        
        $this->db->where("c.businessman_id", $businessman_id);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }
    
    public function get_view_detail_campaign_feed($campaign_id) { // Display all campaigns in feed including businessman, campaign photo and advertising details. 
        $campaign = CAMPAIGN;

        $this->db->select("
            c.campaign_id,
            c.businessman_id,
            c.project_name,
            c.starting_budget,
            c.ending_budget,
            c.project_description,
            c.campaign_deadline,
            c.production_deadline,
            c.video_duration,
            c.advertiser_needed,
            c.date_added,
            c.category_id,
            c.campaign_status_id
            ");
        $this->db->from("{$campaign} c");
        $this->db->where("c.campaign_id", $campaign_id);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }

    public function get_single_photo_display_feed($campaign_id) { // Get Single photo to display in the youtube feed.
        $campaign_photo = CAMPAIGN_PHOTO;

        $this->db->select("cp.photo");
        $this->db->from("{$campaign_photo} cp");
        $this->db->where("cp.campaign_id", $campaign_id);

        $this->response = $this->db->get()->first_row('array');
        return $this->response;
    }

    public function get_category($category_id) { // Category of each campaign
        $category = CATEGORY;

        $this->db->select("c.category_name");
        $this->db->from("{$category} c");
        $this->db->where("c.category_id", $category_id);

        $this->response = $this->db->get()->row_array();
        return $this->response;
    }

    public function get_advertising_details($campaign_id) { // Advertising details of Each Campaign. Can be Reusable.
        $advertiser_details = ADVERTISING_DETAILS;

        $this->db->select("ad.advertising_details_id, ad.detail");
        $this->db->from("{$advertiser_details} ad");
        $this->db->where("ad.campaign_id", $campaign_id);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }

    public function get_campaign_photos($campaign_id) { // Photos of Each Campaign in Youtuber Feed. Can be reusable.
        $campaign_photo = CAMPAIGN_PHOTO;

        $this->db->select("cp.campaign_photo_id, cp.photo");
        $this->db->from("{$campaign_photo} cp");
        $this->db->where("cp.campaign_id", $campaign_id);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }

    public function get_businessman_specific_campaign($businessman_id) { // Specific Businessman of a Campaign to display in Youtuber Feed.
        $businessman = BUSINESSMAN;

        $this->db->select("
            b.businessman_id,
            b.business_name,
            b.first_name,
            b.last_name,
            b.email_address,
            b.company_address,
            b.abouts,
            b.profile_picture,
            b.contact_number,
            b.date_added
            ");
        $this->db->from("{$businessman} b");
        $this->db->where("b.businessman_id", $businessman_id);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }

    public function get_specific_campaign($campaign_id) { // Get specific campaign for mycampaigns youtuber
        $campaign = CAMPAIGN;
        $category = CATEGORY;

        $this->db->select("
            c.businessman_id,
            c.campaign_id,
            c.project_name,
            cat.category_id,
            cat.category_name,
            c.project_description,
            c.campaign_deadline,
            c.production_deadline,
            c.starting_budget,
            c.ending_budget,
            c.advertiser_needed,
            c.video_duration,
            c.date_added
            ");
        $this->db->from("{$campaign} c");
        $this->db->join("{$category} cat","c.category_id = cat.category_id");
        $this->db->where("c.campaign_id", $campaign_id);

           $this->response = $this->db->get()->first_row('array');
        return $this->response;
    }
    
    public function get_photo($campaign_id) { // Get  photo to display in the youtuber My campaigns.
        $campaign_photo = CAMPAIGN_PHOTO;

        $this->db->select("cp.photo");
        $this->db->from("{$campaign_photo} cp");
        $this->db->where("cp.campaign_id", $campaign_id);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }
    
    public function post_video($data=array()){
        if(!empty($data))
        {
            $this->db->insert(VIDEO,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }
     public function get_video($accepted_interest) { // Get  photo to display in the youtuber My campaigns.

        $this->db->select('*');
        $this->db->from(VIDEO);
        $this->db->where("accepted_interest_id", $accepted_interest);

        $this->response = $this->db->get()->first_row('array');

        return $this->response;
    }
    public function get_video_id() { // Get  video id of all video submitted

        $this->db->select('video_id');
        $this->db->from(VIDEO);
        $this->db->where('accepted_interest_id is NOT NULL');

        $this->response = $this->db->get()->result_array();

        return $this->response;
    }

    public function get_campaign_details($campaign_id) { // All On Going Campaigns of BUSINESSMAN
        $campaign = CAMPAIGN;
        $category = CATEGORY;

        $this->db->select("
            c.campaign_id,
            c.project_name,
            cat.category_name,
            c.project_description,
            c.campaign_deadline,
            c.production_deadline,
            c.starting_budget,
            c.ending_budget,
            c.advertiser_needed,
            c.video_duration,
            c.date_added
            ");
        $this->db->from("{$campaign} c");
        $this->db->join("{$category} cat","c.category_id = cat.category_id");
        $this->db->where("c.campaign_id", $campaign_id);

        $this->response = $this->db->get()->first_row('array');
        return $this->response;
    }

    public function status_video_submitted($accepted_interest_id) {
        $video = VIDEO;

        $this->db->select('*');
        $this->db->from("{$video} v");
        $this->db->where("v.accepted_interest_id", $accepted_interest_id);

        $this->response = $this->db->get()->first_row('array');

        return $this->response;
    }
    
    /* =============================== INTEREST =============================== */

    //=====harold=====//

    public function get_all_campaign($youtuber_id) {
        $campaign = CAMPAIGN;
        $category = CATEGORY;
        $campaign_status = CAMPAIGN_STATUS;
        $accepted_interest = ACCEPTED_INTEREST;

        $this->db->select("accepted.campaign_id");
        $this->db->from("{$accepted_interest} accepted");
        $this->db->where("accepted.youtuber_id", $youtuber_id);
        $accepted = $this->db->get_compiled_select();

        $this->db->select("
            c.campaign_id,
            c.project_name,
            cs.status,
            c.campaign_deadline,
            c.production_deadline,
            c.starting_budget,
            c.ending_budget,
            c.advertiser_needed,
            cat.category_name,
            c.date_added
            ");
        $this->db->from("{$campaign} c");
        $this->db->join("{$category} cat","c.category_id = cat.category_id");
        $this->db->join("{$campaign_status} cs", "cs.campaign_status_id = c.campaign_status_id");
        $this->db->where_not_in("c.campaign_id", $accepted);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }

    public function get_all_on_going_campaign_by_category($campaign_status_id,$youtuber_id,$campaign_category_id) {
        $campaign = CAMPAIGN;
        $category = CATEGORY;
        $campaign_status = CAMPAIGN_STATUS;
        $accepted_interest = ACCEPTED_INTEREST;

        $this->db->select("accepted.campaign_id");
        $this->db->from("{$accepted_interest} accepted");
        $this->db->where("accepted.youtuber_id", $youtuber_id);
        $accepted = $this->db->get_compiled_select();

        $this->db->select("
            c.campaign_id,
            c.project_name,
            cs.status,
            c.campaign_deadline,
            c.production_deadline,
            c.starting_budget,
            c.ending_budget,
            c.advertiser_needed,
            cat.category_name,
            c.date_added
            ");
        $this->db->from("{$campaign} c");
        $this->db->join("{$category} cat","c.category_id = cat.category_id");
        $this->db->join("{$campaign_status} cs", "cs.campaign_status_id = c.campaign_status_id");
        $this->db->where("cat.category_id", $campaign_category_id);
        $this->db->where("c.campaign_status_id", $campaign_status_id);
        $this->db->where_not_in("c.campaign_id", $accepted);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }
    public function get_all_on_going_campaign_by_budget($campaign_status_id,$youtuber_id,$starting_budget,$ending_budget) {
        $campaign = CAMPAIGN;
        $category = CATEGORY;
        $campaign_status = CAMPAIGN_STATUS;
        $accepted_interest = ACCEPTED_INTEREST;

        $this->db->select("accepted.campaign_id");
        $this->db->from("{$accepted_interest} accepted");
        $this->db->where("accepted.youtuber_id", $youtuber_id);
        $accepted = $this->db->get_compiled_select();

        $this->db->select("
            c.campaign_id,
            c.project_name,
            cs.status,
            c.campaign_deadline,
            c.production_deadline,
            c.starting_budget,
            c.ending_budget,
            c.advertiser_needed,
            cat.category_name,
            c.date_added
            ");
        $this->db->from("{$campaign} c");
        $this->db->join("{$category} cat","c.category_id = cat.category_id");
        $this->db->join("{$campaign_status} cs", "cs.campaign_status_id = c.campaign_status_id");
        $this->db->where("c.starting_budget >= $starting_budget");
        $this->db->where("c.ending_budget <= $ending_budget ");
        $this->db->where("c.campaign_status_id", $campaign_status_id);
        $this->db->where_not_in("c.campaign_id", $accepted);
        $this->db->order_by("c.ending_budget","desc");


        $this->response = $this->db->get()->result_array();
        return $this->response;
    }
    public function get_all_on_going_campaign_by_category_and_budget($campaign_status_id,$youtuber_id,$category_id,$starting_budget,$ending_budget) {
        $campaign = CAMPAIGN;
        $category = CATEGORY;
        $campaign_status = CAMPAIGN_STATUS;
        $accepted_interest = ACCEPTED_INTEREST;

        $this->db->select("accepted.campaign_id");
        $this->db->from("{$accepted_interest} accepted");
        $this->db->where("accepted.youtuber_id", $youtuber_id);
        $accepted = $this->db->get_compiled_select();

        $this->db->select("
            c.campaign_id,
            c.project_name,
            cs.status,
            c.campaign_deadline,
            c.production_deadline,
            c.starting_budget,
            c.ending_budget,
            c.advertiser_needed,
            cat.category_name,
            c.date_added
            ");
        $this->db->from("{$campaign} c");
        $this->db->join("{$category} cat","c.category_id = cat.category_id");
        $this->db->join("{$campaign_status} cs", "cs.campaign_status_id = c.campaign_status_id");
        $this->db->where("c.starting_budget >= $starting_budget");
        $this->db->where("c.ending_budget <= $ending_budget ");
        $this->db->where("c.campaign_status_id", $campaign_status_id);
        $this->db->where("cat.category_id", $category_id);
        $this->db->where_not_in("c.campaign_id", $accepted);
        $this->db->order_by("c.ending_budget","desc");


        $this->response = $this->db->get()->result_array();
        return $this->response;
    }
    public function get_all_campaign_category() { // Get  video id of all video submitted

        $campaign = CAMPAIGN;
        $category = CATEGORY;
        $this->db->distinct();
        $this->db->select("cat.category_name");
        $this->db->from("{$campaign} c");
        $this->db->join("{$category} cat","c.category_id = cat.category_id");
        $this->db->where("c.campaign_status_id = 1");
      

        $this->response = $this->db->get()->result_array();

        return $this->response;
    }



    //=====harold=====//

}