<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Mdbusinessman
 * 
 * @extends CI_Model
*/
class Mdbusinessman extends CI_Model {

    public function add_businessman($data=array()){
        if(!empty($data))
        {
            $this->db->insert(BUSINESSMAN,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }

    public function add_businessman_rating($data=array()){
        if(!empty($data))
        {
            $this->db->insert(BUSINESSMAN_RATING,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }

    public function add_activity_log($data=array()){
        if(!empty($data))
        {
            $this->db->insert(ACTIVITY_LOG_BUSINESSMAN,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }

    public function get_campaign_id($project_name)
    {
        $businessman = BUSINESSMAN;

        $this->db->select('b.businessman_id');
        $this->db->from('{$businessman} b');
        $this->db->where('b.project_name', $project_name);

        $this->response = $this->db->get()->row_array();
        return $this->response;
    }

    public function get_specific_businessman($businessman_id)
    {

        $businessman = BUSINESSMAN;

        $this->db->select("
            u.businessman_id,
            u.business_name,
            u.first_name,
            u.last_name,
            u.email_address,
            u.company_address,
            u.abouts,
            u.contact_number,
            u.date_added,
            u.profile_picture
            ");
        $this->db->from("{$businessman} u");
        $this->db->where("u.businessman_id", $businessman_id);

        $this->response = $this->db->get()->row_array();
        return $this->response;
    }

    public function get_all_businessman()
    {
        $businessman = BUSINESSMAN;

        $this->db->select("
            b.businessman_id,
            b.business_name,
            b.first_name,
            b.last_name,
            b.email_address,
            b.company_address,
            b.abouts,
            b.contact_number,
            b.profile_picture,
            b.date_added
            ");
        $this->db->from("{$businessman} b");

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }

    public function get_businessman_rating($businessman_rating_id) {
        $businessman_rating = BUSINESSMAN_RATING;

        $this->db->select("*");
        $this->db->from("{$businessman_rating} br");
        $this->db->where("br.businessman_rating_id", $businessman_rating_id);

        $this->response = $this->db->get()->first_row('array');
        return $this->response;
    }

    public function remove_businessman($businessman_id) {
        $this->db->where("businessman_id", $businessman_id);
        $this->db->delete(BUSINESSMAN);
    }

}