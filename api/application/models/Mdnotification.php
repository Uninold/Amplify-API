<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Mdbusinessman
 * 
 * @extends CI_Model
*/
class Mdnotification extends CI_Model {

    public function send_notification($data=array()){
        if(!empty($data))
        {
            $this->db->insert(NOTIFICATION,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }
    public function get_notification($id)
    {
        $notification = NOTIFICATION;

        $this->db->select('*');
        $this->db->from(NOTIFICATION);
        $this->db->where('notif_to', $id);
        $this->db->order_by('date_added', 'desc');
        $this->response = $this->db->get()->result_array();
        return $this->response;
    }

    public function get_specific_businessman($businessman_id)
    {

        $businessman = BUSINESSMAN;

        $this->db->select("
            u.businessman_id,
            u.business_name,
            u.first_name,
            u.last_name,
            u.email_address,
            u.company_address,
            u.abouts,
            u.contact_number,
            u.date_added,
            u.profile_picture
            ");
        $this->db->from("{$businessman} u");
        $this->db->where("u.businessman_id", $businessman_id);

        $this->response = $this->db->get()->row_array();
        return $this->response;
    }

    public function get_all_businessman()
    {
        $businessman = BUSINESSMAN;

        $this->db->select("
            u.businessman_id,
            u.business_name,
            u.first_name,
            u.last_name,
            u.email_address,
            u.company_address,
            u.abouts,
            u.contact_number,
            u.date_added
            ");
        $this->db->from("{$businessman} u");

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }
    public function update_campaign($notif_id, $data = array()) {
        if(!empty($data)) {
            $this->db->where("notif_id", $notif_id);
            $this->response = $this->db->update(NOTIFICATION, $data);
        }
    }

}