<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mdyoutube_data extends CI_Model {

    public function sync_youtube_data($data=array()){
        if(!empty($data))
        {
            $this->db->insert(YOUTUBE_DATA,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }

    public function add_youtube_video($data=array()){
        if(!empty($data))
        {
            $this->db->insert(VIDEO,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }

    public function youtube_data($data=array()){
        if(!empty($data))
        {
            $this->db->select('*');
            $this->db->from(VIDEO_STATISTICS);
            $this->db->where("date_added",  date("Y-m-d"));                
            
            $this->response = $this->db->get()->result_array();

            if(empty($this->response)){
                $this->db->insert(VIDEO_STATISTICS,$data);
                $this->response = $this->db->insert_id();
            }
            else{
                $this->db->where("date_added",  date("Y-m-d"));                
                $this->db->delete(VIDEO_STATISTICS);
                
                $this->db->insert(VIDEO_STATISTICS,$data);
                $this->response = $this->db->insert_id();
            }
        }
        return $this->response;
    }

    public function get_minmax_values($category_id){
        $youtube_data = YOUTUBE_DATA;

        $this->db->select('
            yd.category_id,
            MAX(yd.likeCount) as maxLikesCount,
            MAX(yd.dislikeCount) as maxDislikesCount,
            MAX(yd.commentCount) as maxCommentsCount,
            MAX(yd.viewCount) as maxViewsCount,
            MIN(yd.likeCount) as minLikesCount,
            MIN(yd.dislikeCount) as minDislikesCount,
            MIN(yd.commentCount) as minCommentsCount,
            MIN(yd.viewCount) as minViewsCount
        ');
        $this->db->from("{$youtube_data} yd");
        $this->db->where("yd.category_id", $category_id);

        $this->response = $this->db->get()->result_array();
    
        return $this->response;
    }

    public function get_minmax_values_v2($category_id){
        $v2_youtube_data = V2_YOUTUBE_DATA;

        $this->db->select('
            yd.category_id,
            MAX(yd.likeCount) as maxLikesCount,
            MAX(yd.dislikeCount) as maxDislikesCount,
            MAX(yd.commentCount) as maxCommentsCount,
            MAX(yd.viewCount) as maxViewsCount,
            MIN(yd.likeCount) as minLikesCount,
            MIN(yd.dislikeCount) as minDislikesCount,
            MIN(yd.commentCount) as minCommentsCount,
            MIN(yd.viewCount) as minViewsCount
        ');
        $this->db->from("{$v2_youtube_data} yd");
        $this->db->where("yd.category_id", $category_id);

        $this->response = $this->db->get()->result_array();
    
        return $this->response;
    }

    public function get_businessman_rating($accepted_interest_id){
        $businessman_rating = BUSINESSMAN_RATING;
        $accepted_interest = ACCEPTED_INTEREST;
        $campaign = CAMPAIGN;

        $this->db->select('
            cam.campaign_id,
            ai.youtuber_id,
            br.engaging,
            br.credibility,
            br.impression,
            br.action_oriented,
            br.significance,
            br.integrated,
            br.brand_service,
            br.brand_innovation,
            br.brand_quality
        ');
        $this->db->from("{$businessman_rating} br");
        $this->db->join("{$accepted_interest} ai", "ai.accepted_interest_id = br.accepted_interest_id");
        $this->db->join("{$campaign} cam", "cam.campaign_id = ai.campaign_id");
        $this->db->where("br.accepted_interest_id", $accepted_interest_id);

        $this->response = $this->db->get()->result_array();
    
        return $this->response;
    }

    public function get_businessman_rating_v2($accepted_interest_id){
        $v2_businessman_rating = V2_BUSINESSMAN_RATING;
        $v2_accepted_interest = V2_ACCEPTED_INTEREST;

        $this->db->select('
            ai.youtuber_id,
            br.engaging,
            br.credibility,
            br.impression,
            br.action_oriented,
            br.significance,
            br.integrated,
            br.brand_service,
            br.brand_innovation,
            br.brand_quality
        ');
        $this->db->from("{$v2_businessman_rating} br");
        $this->db->join("{$v2_accepted_interest} ai", "ai.accepted_interest_id = br.accepted_interest_id");
        $this->db->where("br.accepted_interest_id", $accepted_interest_id);

        $this->response = $this->db->get()->result_array();
    
        return $this->response;
    }
    
    public function get_video_statistics($accepted_interest_id){
        $video_statistics = VIDEO_STATISTICS;
        $video = VIDEO;
        $this->db->select('vs.video_id, vs.views, vs.likes, vs.dislikes, vs.date_added');
        $this->db->from("{$video} v");
        $this->db->join("{$video_statistics} vs","v.video_id = vs.video_id");        
        $this->db->where('v.accepted_interest_id', $accepted_interest_id);
        $this->response = $this->db->get()->result_array();

     
        return $this->response;
    }

    public function get_video_existed($video_id) {
        $youtube_data = YOUTUBE_DATA;

        $this->db->select('*');
        $this->db->from("{$youtube_data} v");
        $this->db->where("v.videoId", $video_id);

        $this->response = $this->db->get()->first_row('array');

        return $this->response;
    }

    public function get_youtube_channel_existence($youtube_channel){
        $mediakit = MEDIAKIT;

        $this->db->select('m.youtubeCh');
        $this->db->from("{$mediakit} m");      
        $this->db->where('m.youtubeCh', $youtube_channel);
        $this->response = $this->db->get()->first_row('array');
    
        return $this->response;
    }

    public function remove_youtube_data() {
        $this->db->empty_table(YOUTUBE_DATA);
    }

    public function update_youtuber_video($video_id, $data = array()) {
        if(!empty($data)) {
            $this->db->where("video_id", $video_id);
            $this->response = $this->db->update(VIDEO, $data);
        }
    }
}