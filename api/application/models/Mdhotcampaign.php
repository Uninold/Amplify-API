<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * MDAUTH
 * 
 * @extends CI_Model
*/
class Mdhotcampaign extends CI_Model {

    // Ending budget is greater than 80% of MAXIMUM Budget among all campaign
    // Then starting budget is greater than 80% of the ending budget, above.
    // When campaign status is on going
    public function get_hot_campaigns(){
        $campaign = CAMPAIGN;
        $campaign_photo = CAMPAIGN_PHOTO;
        $category = CATEGORY;

        $this->db->select("MAX(cam.ending_budget) as max_budg");
        $this->db->from("{$campaign} cam");
        $max_ending_budget = $this->db->get()->first_row('array');
        $max_budg = (int)$max_ending_budget['max_budg'];

        $this->db->select("
            cam.campaign_id,
            cam.project_name,
            c.category_name,
            cam.starting_budget,
            cam.ending_budget,
            cp.photo,
            cam.advertiser_needed
            ");
        $this->db->from("{$campaign} cam");
        $this->db->join("{$campaign_photo} cp", "cam.campaign_id = cp.campaign_id");
        $this->db->join("{$category} c", "cam.category_id = c.category_id");
        $this->db->where("cam.ending_budget >= ($max_budg * 0.8)");
        $this->db->where("cam.starting_budget >= (cam.ending_budget * 0.8)");
        $this->db->where("cam.campaign_status_id", 1);

        $this->response = $this->db->get()->result_array();

        return $this->response;
    }

    public function accepted_interest_count($campaign_id) {
        $accepted_interest = ACCEPTED_INTEREST;

        $this->db->select("COUNT(*) as count_interested");
        $this->db->from("{$accepted_interest} ai");
        $this->db->where("ai.campaign_id", $campaign_id);

        $this->response = $this->db->get()->first_row('array');

        return $this->response;
    }

}