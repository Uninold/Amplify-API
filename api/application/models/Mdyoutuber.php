<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Mdyoutuber
 * 
 * @extends CI_Model
*/
class Mdyoutuber extends CI_Model {

    public function add_youtuber($data=array()){
        $youtuber = YOUTUBER;

        if(!empty($data))
        {
            $this->db->insert(YOUTUBER,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }

    public function add_youtuber_reputation_score($data=array()) {
        $recommendation = RECOMMENDATION;

        if(!empty($data))
        {
            $this->db->insert(RECOMMENDATION,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }

    public function update_youtuber_reputation_score($youtuber_id, $category_id, $data = array()) {
        if(!empty($data)) {
            $this->db->where("youtuber_id", $youtuber_id);
            $this->db->where("category_id", $category_id);
            $this->response = $this->db->update(RECOMMENDATION, $data);
        }
    }

    public function interested($data=array()){
        if(!empty($data))
        {
            $this->db->insert(TEMP_ADVERTISER,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }

    public function get_all_youtuber() {
        $youtuber = YOUTUBER;
        $mediakit = MEDIAKIT;
        
        $this->db->select('
            y.youtuber_id,
            y.photoUrl,
            mk.lastName,
            mk.firstName,
            mk.youtubeCh,
            mk.date_added
        ');
        $this->db->from("{$youtuber} y"); 
        $this->db->join("{$mediakit} mk", "y.youtuber_id = mk.youtuber_id");
        $this->response = $this->db->get()->result_array();
     
        return $this->response;
    }

    public function get_youtuber($id){
        $youtuber = YOUTUBER;
        $this->db->select('*');
        $this->db->from($youtuber); 
        $this->db->where('youtuber_id', $id);
        $this->response = $this->db->get()->first_row('array');
     
        return $this->response;
    }
    
    public function get_all_youtuber_on_going_production($campaign_id) { // Get all YouTuber in On Going Production
        $youtuber = YOUTUBER;
        $mediakit = MEDIAKIT;
        $accepted_interest = ACCEPTED_INTEREST;
        $submission_status = SUBMISSION_STATUS;

        $this->db->select("
            ai.accepted_interest_id,
            m.youtuber_id,
            m.firstName,
            m.lastName,
            y.photoUrl,
            ss.submission_status
        ");
        $this->db->from("{$accepted_interest} ai");
        $this->db->join("{$youtuber} y", "ai.youtuber_id = y.youtuber_id");
        $this->db->join("{$mediakit} m", "m.youtuber_id = y.youtuber_id");
        $this->db->join("{$submission_status} ss", "ss.submission_status_id = ai.submission_status_id");
        $this->db->where("ai.campaign_id", $campaign_id);

        $this->response = $this->db->get()->result_array();
     
        return $this->response;
    }
    
    public function get_youtuber_channel($youtuber_id){
        $mediakit = MEDIAKIT;
        $youtuber = YOUTUBER;
        $this->db->select('
            y.youtuber_id,
            m.id,
            m.firstName,
            m.lastName,
            m.address,
            m.contact_no,
            m.youtubeCh,
            m.ig_ac,
            m.fb_ac,
            m.twitter_ac,
            m.gender,
            m.photoUrl,
            m.abouts
        ');
        $this->db->from("{$youtuber} y"); 
        $this->db->join("{$mediakit} m","y.youtuber_id = m.youtuber_id");
        $this->db->where("y.youtuber_id",$youtuber_id);
  

        $this->response = $this->db->get()->first_row('array');
        return $this->response;
    }

    public function get_mediakit_interests($youtuber_id) {
        $mediakit_interest = MEDIAKIT_INTEREST;
        $this->db->select('
            mi.interest
        ');
        $this->db->from("{$mediakit_interest} mi"); 
        $this->db->where("mi.id",$youtuber_id);
  
        $this->response = $this->db->get()->result_array();
        return $this->response;
    }

    public function get_videos($youtuber_id) { 
        $video = VIDEO;
        $category = CATEGORY;

        $this->db->select("
            v.video_id,
            v.video_title,
            v.video_description,
            v.channel_id,
            v.channel_title,
            cat.category_name,
            cat.category_id,
            v.default_thumbnail,
            v.views_count,
            v.likes_count,
            v.dislikes_count,
            v.comments_count,
            v.date_accomplished,
            v.youtuber_id,
            v.accepted_interest_id
        ");
        $this->db->from("{$video} v");
        $this->db->join("{$category} cat", "cat.category_id = v.category_id");
        $this->db->where("v.youtuber_id", $youtuber_id);

        $this->response = $this->db->get()->result_array();
     
        return $this->response;
    }

    public function get_videos_v2($youtuber_id) { // For Presentation 
        $v2_video = V2_VIDEO;

        $this->db->select("
            v.video_id,
            v.category_id,
            v.views_count,
            v.likes_count,
            v.dislikes_count,
            v.comments_count,
            v.date_accomplished,
            v.youtuber_id,
            v.accepted_interest_id
        ");
        $this->db->from("{$v2_video} v");
        $this->db->where("v.youtuber_id", $youtuber_id);

        $this->response = $this->db->get()->result_array();
     
        return $this->response;
    }

    public function get_youtuber_reports($youtuber_id) {
        $accepted_interest = ACCEPTED_INTEREST;
        $campaign = CAMPAIGN;
        $businessman = BUSINESSMAN;
        $submission_status = SUBMISSION_STATUS;
        $video = VIDEO;
        
        $this->db->select('
            ai.accepted_interest_id,
            cam.campaign_id,
            cam.project_name,
            b.business_name,
            ss.submission_status
        ');
        $this->db->from("{$accepted_interest} ai");
        $this->db->join("{$campaign} cam", "cam.campaign_id = ai.campaign_id");
        $this->db->join("{$businessman} b", "b.businessman_id = cam.businessman_id");
        $this->db->join("{$submission_status} ss", "ss.submission_status_id = ai.submission_status_id");
        $this->db->join("{$video} v", "v.accepted_interest_id = ai.accepted_interest_id");
        $this->db->where("ai.israted", 1);
        $this->db->where("ai.withvideo", 1);
        $this->db->where("ai.youtuber_id", $youtuber_id);
        $this->db->where("ai.submission_status_id", 1);
        $this->db->or_where("ai.submission_status_id", 3);

        $this->response = $this->db->get()->result_array();
     
        return $this->response;
    }

    public function get_youtuber_report_details($accepted_interest_id) {
        $accepted_interest = ACCEPTED_INTEREST;
        $campaign = CAMPAIGN;
        $businessman = BUSINESSMAN;
        $submission_status = SUBMISSION_STATUS;
        $video = VIDEO;
        $businessman_rating = BUSINESSMAN_RATING;
        $campaign_photo = CAMPAIGN_PHOTO;

        
        $this->db->select('
            ai.accepted_interest_id,
            ss.submission_status,
            v.video_id,
            b.businessman_id,
            br.businessman_rating_id,
            cam.campaign_id,
            cp.campaign_photo_id
        ');
        $this->db->from("{$accepted_interest} ai");
        $this->db->join("{$campaign} cam", "cam.campaign_id = ai.campaign_id");
        $this->db->join("{$businessman} b", "b.businessman_id = cam.businessman_id");
        $this->db->join("{$submission_status} ss", "ss.submission_status_id = ai.submission_status_id");
        $this->db->join("{$video} v", "v.accepted_interest_id = ai.accepted_interest_id");
        $this->db->join("{$businessman_rating} br", "br.accepted_interest_id = v.accepted_interest_id");
        $this->db->join("{$campaign_photo} cp", "cp.campaign_id = cam.campaign_id");
        $this->db->where("ai.accepted_interest_id", $accepted_interest_id);

        $this->response = $this->db->get()->first_row('array');
     
        return $this->response;
    }

    public function get_recommended_youtuber($category_id) {
        $recommendation = RECOMMENDATION;
        $youtuber = YOUTUBER;
        $mediakit = MEDIAKIT;

        $this->db->select('
            r.youtuber_id,
            r.category_id,
            r.points,
            mk.firstName,
            mk.lastName,
            mk.contact_no,
            mk.photoUrl
        ');
        $this->db->from("{$recommendation} r");
        $this->db->join("{$youtuber} y", "y.youtuber_id = r.youtuber_id");
        $this->db->join("{$mediakit} mk", "mk.youtuber_id = y.youtuber_id");
        $this->db->where("r.category_id", $category_id);
        $this->db->where("r.points > 0");
        $this->db->order_by("r.points", "desc");
        $this->db->limit(3);

        $this->response = $this->db->get()->result_array();
     
        return $this->response;
    }

    public function get_youtuber_exist_on_recommend($youtuber_id) {
        $recommendation = RECOMMENDATION;

        $this->db->select('*');
        $this->db->from("{$recommendation} r");
        $this->db->where("r.youtuber_id", $youtuber_id);

        $this->response = $this->db->get()->first_row('array');
     
        return $this->response;
    }

    public function get_video_existed($video_id) {
        $video = VIDEO;

        $this->db->select('*');
        $this->db->from("{$video} v");
        $this->db->where("v.video_id", $video_id);

        $this->response = $this->db->get()->first_row('array');

        return $this->response;
    }

    public function get_video_details($video_id) {
        $video = VIDEO;

        $this->db->select('*');
        $this->db->from("{$video} v");
        $this->db->where("v.video_id", $video_id);

        $this->response = $this->db->get()->first_row('array');
     
        return $this->response;
    }

    public function remove_youtuber($youtuber_id) {
        $this->db->where("youtuber_id", $youtuber_id);
        $this->db->delete(YOUTUBER);
    }

    //== HAROLD ==//
    public function get_all_new_youtuber(){
        // $date_plus = date("Y-m-d", strtotime($Date. ' + 10 days'));
        $current_date = date('Y-m-d');  
        $this->db->select('*');
        $this->db->from(YOUTUBER); 
        $this->db->where('date_added >= DATE_ADD(NOW(), INTERVAL -11 DAY)', null, null, false, false, true);
        $this->db->order_by("date_added","desc");

        $this->response = $this->db->get()->result_array();

        return $this->response;
    }
    public function get_top_influencer($category_id){
        $recommendation = RECOMMENDATION;
        $youtuber = YOUTUBER;
        $category = CATEGORY;

        $this->db->select('
          r.youtuber_id,
          c.category_name,
          m.name,
          m.photoUrl,
          r.points
        ');
        $this->db->from("{$recommendation} r");
        $this->db->join("{$youtuber} m", "m.youtuber_id = r.youtuber_id");
        $this->db->join("{$category} c", "c.category_id = r.category_id");
        $this->db->where('r.category_id',$category_id);
        $this->db->order_by("r.points","desc");
        $this->db->order_by("m.date_added","desc");

        $this->response = $this->db->get()->first_row('array');
     
        return $this->response;
    }
    public function get_category_top_influencer(){
        $recommendation = RECOMMENDATION;

        $this->db->distinct();
        $this->db->select('r.category_id');
        $this->db->from("{$recommendation} r");
        $this->db->order_by("r.category_id","asc");

        $this->response = $this->db->get()->result_array();
     
        return $this->response;
    }

    public function get_referred_youtuber($youtuber_id){
        $notif = NOTIFICATION;
        $mediakit = MEDIAKIT;

        $this->db->select('
          n.notif_id,
          n.notif_to,
          n.referred,
          mk.photoUrl,
          mk.firstName,
          mk.lastName

        ');
        $this->db->from("{$mediakit} mk");
        $this->db->join("{$notif} n","n.notif_to = mk.youtuber_id");
        $this->db->where('n.notif_from',$youtuber_id);

        $this->response = $this->db->get()->result_array();
     
        return $this->response;
    }
    public function add_youtuber_refer($data=array()){

        if(!empty($data))
        {
            $this->db->insert(YOUTUBER_REFER,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }
    public function get_youtuber_refer($youtuber_id,$youtuber_refer,$campaign_id){

        $this->db->select('*');
        $this->db->from(YOUTUBER_REFER); 
        $this->db->where('youtuber_id', $youtuber_id);
        $this->db->where('youtuber_referred', $youtuber_refer);
        $this->db->where('campaign_id', $campaign_id);

        $this->response = $this->db->get()->result_array();
     
        return $this->response;
    }

    

    
 

    
    //== HAROLD ==//
}