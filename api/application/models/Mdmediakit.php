<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mdmediakit extends CI_Model {

    public function add_mediakit($data=array()){

        if(!empty($data))
        {
            $this->db->insert(MEDIAKIT,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }

    public function add_mediakit_interest($data=array()){

        if(!empty($data))
        {
            $this->db->insert(MEDIAKIT_INTEREST,$data);
            $this->response = $this->db->insert_id();
        }
        return $this->response;
    }

    public function get_youtuber_mediakit($id){
        $mediakit = MEDIAKIT;
        $this->db->select('*');
        $this->db->from($mediakit); 
        $this->db->where('youtuber_id', $id);
        $this->response = $this->db->get()->first_row('array');
     
        return $this->response;
    }

    public function get_youtuber_info($id){
        $mediakit = MEDIAKIT;
        $youtuber = YOUTUBER;
        $this->db->select('*');
        $this->db->from("{$youtuber} uy"); 
        $this->db->join("{$mediakit} yi","uy.youtuber_id=yi.youtuber_id");
        $this->db->where("yi.youtuber_id",$id);

        $this->response = $this->db->get()->first_row('array');
        return $this->response;
    }
    public function get_all_youtuber($id){
        $mediakit = MEDIAKIT;
        $this->db->select('*');
        $this->db->from($mediakit); 
        $this->db->where_not_in('youtuber_id', $id);
        $this->response = $this->db->get()->result_array();
     
        return $this->response;
    }
    public function search_mediakit($search){
        $mediakit = MEDIAKIT;
        $this->db->select('*');
        $this->db->from($mediakit); 
        $this->db->like('firstName', $search);
        $this->db->or_like('lastName', $search);
        $this->response = $this->db->get()->result_array();
     
        return $this->response;
    }
   

}