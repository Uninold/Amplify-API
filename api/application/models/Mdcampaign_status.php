<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mdcampaign_status extends CI_Model {

    public function get_all_on_going_campaign_businessman_status($campaign_status_id,$businessman_id) { // All On Going Campaigns of BUSINESSMAN
        $campaign = CAMPAIGN;
        $category = CATEGORY;
        $campaign_status = CAMPAIGN_STATUS;

        $this->db->select("
            c.campaign_id,
            c.project_name,
            cs.status,
            c.campaign_deadline,
            c.production_deadline,
            c.starting_budget,
            c.ending_budget,
            c.advertiser_needed,
            cat.category_name,
            c.date_added
            ");
        $this->db->from("{$campaign} c");
        $this->db->join("{$category} cat","c.category_id = cat.category_id");
        $this->db->join("{$campaign_status} cs", "cs.campaign_status_id = c.campaign_status_id");
        $this->db->where("c.campaign_status_id", $campaign_status_id);
        $this->db->where("c.businessman_id", $businessman_id);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }

    public function get_all_on_going_production_businessman_status($campaign_status_id,$businessman_id) { // All On Going Production of BUSINESSMAN
        $campaign = CAMPAIGN;
        $category = CATEGORY;
        $campaign_status = CAMPAIGN_STATUS;

        $this->db->select("
            c.campaign_id,
            c.project_name,
            cs.status,
            c.campaign_deadline,
            c.production_deadline,
            c.starting_budget,
            c.ending_budget,
            c.advertiser_needed,
            cat.category_name,
            c.date_added
            ");
        $this->db->from("{$campaign} c");
        $this->db->join("{$category} cat","c.category_id = cat.category_id");
        $this->db->join("{$campaign_status} cs", "cs.campaign_status_id = c.campaign_status_id");
        $this->db->where("c.campaign_status_id", $campaign_status_id);
        $this->db->where("c.businessman_id", $businessman_id);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }

    public function get_specific_on_going_campaign_businessman_with_advertisers($campaign_status_id,$campaign_id) { // All On Going Campaigns of BUSINESSMAN
        $campaign = CAMPAIGN;
        $category = CATEGORY;
        $campaign_status = CAMPAIGN_STATUS;

        $this->db->select("
            c.campaign_id,
            c.project_name,
            cs.status,
            cat.category_id,
            cat.category_name,
            c.project_description,
            c.campaign_deadline,
            c.production_deadline,
            c.starting_budget,
            c.ending_budget,
            c.advertiser_needed,
            c.video_duration,
            c.date_added
            ");
        $this->db->from("{$campaign} c");
        $this->db->join("{$category} cat","c.category_id = cat.category_id");
        $this->db->join("{$campaign_status} cs", "cs.campaign_status_id = c.campaign_status_id");
        $this->db->where("c.campaign_status_id", $campaign_status_id);
        $this->db->where("c.campaign_id", $campaign_id);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }

    public function get_specific_on_going_production_businessman_with_advertisers($campaign_status_id,$campaign_id) { // All On Going Campaigns of BUSINESSMAN
        $campaign = CAMPAIGN;
        $category = CATEGORY;
        $campaign_status = CAMPAIGN_STATUS;

        $this->db->select("
            c.campaign_id,
            c.project_name,
            cs.status,
            cat.category_name,
            c.project_description,
            c.campaign_deadline,
            c.production_deadline,
            c.starting_budget,
            c.ending_budget,
            c.advertiser_needed,
            c.video_duration,
            c.date_added
            ");
        $this->db->from("{$campaign} c");
        $this->db->join("{$category} cat","c.category_id = cat.category_id");
        $this->db->join("{$campaign_status} cs", "cs.campaign_status_id = c.campaign_status_id");
        $this->db->where("c.campaign_status_id", $campaign_status_id);
        $this->db->where("c.campaign_id", $campaign_id);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }

    public function get_accepted_advertisers($campaign_id) {
        $accepted_interest = ACCEPTED_INTEREST;
        
        $this->db->select("count(ai.campaign_id) as accepted_advertiser_count");
        $this->db->from("{$accepted_interest} ai");
        $this->db->where("ai.campaign_id", $campaign_id);

        $this->response = $this->db->get()->row_array();
        return $this->response;
    }

    public function get_all_on_going_production_status($campaign_status_id,$businessman_id){ // All On Going Production
        $campaign = CAMPAIGN;
        $category = CATEGORY;
        $campaign_status = CAMPAIGN_STATUS;

        $this->db->select("
            c.campaign_id,
            c.project_name,
            cs.status,
            c.production_deadline,
            c.starting_budget,
            c.ending_budget,
            c.advertiser_needed,
            cat.category_name,
            c.date_added
            ");
        $this->db->from("{$campaign} c");
        $this->db->join("{$category} cat","c.category_id = cat.category_id");
        $this->db->join("{$campaign_status} cs", "cs.campaign_status_id = c.campaign_status_id");
        $this->db->where("c.campaign_status_id", $campaign_status_id);
        $this->db->where("c.businessman_id", $businessman_id);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }

    // All On Going Campaigns ALL BUSINESSMAN.
    // Accepted Campaigns of Youtuber will NOT be displayed.
    public function get_all_on_going_campaign_status($campaign_status_id,$youtuber_id) {
        $campaign = CAMPAIGN;
        $category = CATEGORY;
        $campaign_status = CAMPAIGN_STATUS;
        $accepted_interest = ACCEPTED_INTEREST;

        $this->db->select("accepted.campaign_id");
        $this->db->from("{$accepted_interest} accepted");
        $this->db->where("accepted.youtuber_id", $youtuber_id);
        $accepted = $this->db->get_compiled_select();

        $this->db->select("
            c.campaign_id,
            c.project_name,
            cs.status,
            c.campaign_deadline,
            c.production_deadline,
            c.starting_budget,
            c.ending_budget,
            c.advertiser_needed,
            cat.category_name,
            c.date_added
            ");
        $this->db->from("{$campaign} c");
        $this->db->join("{$category} cat","c.category_id = cat.category_id");
        $this->db->join("{$campaign_status} cs", "cs.campaign_status_id = c.campaign_status_id");
        $this->db->where("c.campaign_status_id", $campaign_status_id);
        $this->db->where_not_in("c.campaign_id", $accepted);

        $this->response = $this->db->get()->result_array();
        return $this->response;
    }

    public function rating_status($accepted_interest_id, $data) {
        if(!empty($data)) {
            $this->db->where("accepted_interest_id", $accepted_interest_id);
            $this->response = $this->db->update(ACCEPTED_INTEREST, $data);
        }
    }
    
}