<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Tables extends CI_Migration {
	
    public function up()
    {
        /*          */
        /* PRODUCTS */ 
        /*          */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'description' => array(
                'type' => 'TEXT'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("`date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->create_table(PRODUCTS);

        /*           */
        /* BORROWERS */ 
        /*           */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'first_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'last_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'cell_phone' => array(
                'type' => 'VARCHAR',
                'constraint' => 20
            ),
            'work_phone' => array(
                'type' => 'VARCHAR',
                'constraint' => 20
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table(BORROWERS);

        /*           */
        /* COMPANY   */ 
        /*           */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'description' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'profile_picture' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'address' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'latitude' => array(
                'type' => 'DOUBLE'
            ),
            'longitude' => array(
                'type' => 'DOUBLE'
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'address' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'tel_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 20
            ),
            'fax_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 20
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'is_selected' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 0,
                'comment' => '0 = not selected, 1 = selected first in the company dropdown'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table(COMPANY);

        /*                  */
        /* NOTARYCLOUD INFO */ 
        /*                  */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'address' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'phone_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 20
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("`date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->create_table(NOTARYCLOUD_INFO);

        /*                  */
        /* CLOSING_STATUS   */ 
        /*                  */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'label' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("`date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->create_table(CLOSING_STATUS);

        /*                */
        /* STATUS DEFAULT */ 
        /*                */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'label' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'color' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'border_color' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'importance' => array(
                'type' => 'INT',
                'constraint' => 5
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("`date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->create_table(STATUS_DEFAULT);

        /*                 */
        /* FOLDERS DEFAULT */ 
        /*                 */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            )
        ));
        $this->dbforge->add_field("`date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table(FOLDERS_DEFAULT);

        /*              */
        /* USER GROUPS  */ 
        /*              */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'description' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("`date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->create_table(USER_GROUPS);

        /*                       */
        /* NOTARY DOCUMENT TYPES */ 
        /*                       */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'type' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("`date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->create_table(NOTARY_DOCUMENT_TYPES);

        /*                            */
        /* SCHEDULE DOCUMENTS DEFAULT */ 
        /*                            */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'file_icon' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'file_title' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'file_name' => array(
                'type' => 'TEXT'
            ),
            'size' => array(
                'type' => 'DOUBLE'
            ),
            'type' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("`date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->create_table(SIGNING_DOCUMENTS_DEFAULT);

        /*                */
        /* SHIPMENT TYPES */ 
        /*                */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'type' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            )
        ));
        $this->dbforge->add_field("`date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table(SHIPMENT_TYPES);

        /*                   */
        /* PAYMENT FEE TYPES */ 
        /*                   */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            )
        ));
        $this->dbforge->add_field("`date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table(PAYMENT_FEE_TYPES);

        /*                */
        /* SIGNING STATUS */ 
        /*                */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            )
        ));
        $this->dbforge->add_field("`date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table(SIGNING_STATUS);

        /*          */
        /* TIMEZONE */ 
        /*          */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'country_code' => array(
                'type' => 'VARCHAR',
                'constraint' => 2
            ),
            'country_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'time_start' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'gmt_offset' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'dst' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("`date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->create_table(TIMEZONE);

        /*        */
        /* USERS  */ 
        /*        */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => 50,
                'unique' => TRUE
            ),
            'password' => array(
                'type' => 'TEXT',
            ),
            'forgotten_password_code' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'forgotten_password_time' => array(
                'type' => 'DATETIME',
                'null' => TRUE
            ),
            'remember_code' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'first_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'middle_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => TRUE
            ),
            'last_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'description' => array(
                'type' => 'TEXT'
            ),
            'profile_picture' => array(
                'type' => 'TEXT'
            ),
            'status' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'comment' => '0 = inactive, 1 = active'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            ),
            'date_updated' => array(
                'type' => 'DATETIME',
                'default' => NULL
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table(USERS);

        /*          */
        /* NOTARIES */ 
        /*          */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'description' => array(
                'type' => 'TEXT'
            ),
            'profile_picture' => array(
                'type' => 'TEXT'
            ),
            'service' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => TRUE
            ),
            'track_time' => array(
                'type' => 'TIME',
                'null' => TRUE
            ),
            'auto' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 0,
                'comment' => '0 = manual, 1 = automated'
            ),
            'eo_insurance' => array(
                'type' => 'DOUBLE',
                'null' => TRUE
            ),
            'company' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => TRUE
            ),
            'status' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'comment' => '0 = inactive, 1 = active, 2 = certified'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            ),
            'date_updated' => array(
                'type' => 'DATETIME',
                'default' => NULL
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table(NOTARIES);

        /*                  */
        /* NOTARY DOCUMENTS */ 
        /*                  */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'notary_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'notary_document_types_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'file_title' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'file_name' => array(
                'type' => 'TEXT'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('notary_id');
        $this->dbforge->create_table(NOTARY_DOCUMENTS);
        $this->dbforge->add_column(NOTARY_DOCUMENTS,array(
            'CONSTRAINT notary_documents_notary_fk_id FOREIGN KEY(`notary_id`) REFERENCES `'.NOTARIES.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*                  */
        /* NOTARY FAVORITED */ 
        /*                  */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'notary_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'is_favorite' => array(
                'type' => 'TINYINT',
                'constraint' => 11,
                'default' => 1,
                'comment' => '1 = not favorite/heartbroken, 2 = favorite'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('notary_id');
        $this->dbforge->create_table(NOTARY_FAVORITED);
        $this->dbforge->add_column(NOTARY_FAVORITED,array(
            'CONSTRAINT notary_favorited_notary_fk_id FOREIGN KEY(`notary_id`) REFERENCES `'.NOTARIES.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*              */
        /* NOTARY NOTES */ 
        /*              */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'notary_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'message' => array(
                'type' => 'TEXT'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('notary_id');
        $this->dbforge->add_key('user_id');
        $this->dbforge->create_table(NOTARY_NOTES);
        $this->dbforge->add_column(NOTARY_NOTES,array(
            'CONSTRAINT notary_notes_notary_fk_id FOREIGN KEY(`notary_id`) REFERENCES `'.NOTARIES.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION',
            'CONSTRAINT notary_notes_user_fk_id FOREIGN KEY(`user_id`) REFERENCES `'.USERS.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*                */
        /* NOTARY REVIEWS */ 
        /*                */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'notary_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'review' => array(
                'type' => 'TEXT'
            ),
            'rating' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('user_id');
        $this->dbforge->add_key('notary_id');
        $this->dbforge->create_table(NOTARY_REVIEWS);
        $this->dbforge->add_column(NOTARY_REVIEWS,array(
            'CONSTRAINT notary_reviews_user_fk_id FOREIGN KEY(`user_id`) REFERENCES `'.USERS.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION',
            'CONSTRAINT notary_reviews_notary_fk_id FOREIGN KEY(`notary_id`) REFERENCES `'.NOTARIES.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*                      */
        /* STATUS CUSTOM  */ 
        /*                      */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'company_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'status_default_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'label' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'color' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'border_color' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'importance' => array(
                'type' => 'INT',
                'constraint' => 5
            )
        ));
        $this->dbforge->add_field("`date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('company_id');
        $this->dbforge->add_key('status_default_id');
        $this->dbforge->create_table(STATUS_CUSTOM);
        $this->dbforge->add_column(STATUS_CUSTOM,array(
            'CONSTRAINT status_custom_company_fk_id FOREIGN KEY(`company_id`) REFERENCES `'.COMPANY.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION',
            'CONSTRAINT status_custom_status_default_fk_id FOREIGN KEY(`status_default_id`) REFERENCES `'.STATUS_DEFAULT.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*                 */
        /* FOLDERS CUSTOM  */ 
        /*                 */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'company_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'folder_default_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            )
        ));
        $this->dbforge->add_field("`date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('company_id');
        $this->dbforge->add_key('folder_default_id');
        $this->dbforge->create_table(FOLDERS_CUSTOM);
        $this->dbforge->add_column(FOLDERS_CUSTOM,array(
            'CONSTRAINT folders_custom_company_fk_id FOREIGN KEY(`company_id`) REFERENCES `'.COMPANY.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION',
            'CONSTRAINT folders_custom_folder_default_fk_id FOREIGN KEY(`folder_default_id`) REFERENCES `'.FOLDERS_DEFAULT.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*                      */
        /* USER GROUP REFERENCE */ 
        /*                      */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'user_group_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('user_id');
        $this->dbforge->add_key('user_group_id');
        $this->dbforge->create_table(USER_GROUP_REFERENCE);
        $this->dbforge->add_column(USER_GROUP_REFERENCE,array(
            'CONSTRAINT user_group_reference_user_fk_id FOREIGN KEY(`user_id`) REFERENCES `'.USERS.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION',
            'CONSTRAINT user_group_reference_user_group_fk_id FOREIGN KEY(`user_group_id`) REFERENCES `'.USER_GROUPS.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*               */
        /* COMPANY USERS */ 
        /*               */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'company_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('company_id');
        $this->dbforge->add_key('user_id');
        $this->dbforge->create_table(COMPANY_USERS);
        $this->dbforge->add_column(COMPANY_USERS,array(
            'CONSTRAINT company_users_company_fk_id FOREIGN KEY(`company_id`) REFERENCES `'.COMPANY.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION',
            'CONSTRAINT company_users_user_fk_id FOREIGN KEY(`user_id`) REFERENCES `'.USERS.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*          */
        /* SIGNINGS */ 
        /*          */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'company_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'product_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'date_scheduled' => array(
                'type' => 'DATETIME'
            ),
            'timezone' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'title_order_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 20
            ),
            'loan_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 20
            ),
            'property_location' => array(
                'type' => 'VARCHAR',
                'constraint' => 15
            ),
            'unit_number' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'street' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'city' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'state' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'country' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'zipcode' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'latitude' => array(
                'type' => 'DOUBLE'
            ),
            'longitude' => array(
                'type' => 'DOUBLE'
            ),
            'status_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => 0
            ),
            'folder_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => 0
            ),
            'signing_status_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => 0
            ),
            'loan_officer_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => 0
            ),
            'processor_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => 0
            ),
            'underwriter_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => 0
            ),
            'special_instructions' => array(
                'type' => 'TEXT'
            ),
            'is_deleted' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 0
            ),
            'last_updated' => array(
                'type' => 'DATETIME'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            ),
            'added_by' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('company_id');
        $this->dbforge->add_key('product_id');
        $this->dbforge->create_table(SIGNINGS);
        $this->dbforge->add_column(SIGNINGS,array(
            'CONSTRAINT signings_company_fk_id FOREIGN KEY(`company_id`) REFERENCES `'.COMPANY.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION',
            'CONSTRAINT signings_product_fk_id FOREIGN KEY(`product_id`) REFERENCES `'.PRODUCTS.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*                    */
        /* SIGNING DOCUMENTS  */ 
        /*                    */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'signing_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'file_icon' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'file_title' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'file_name' => array(
                'type' => 'TEXT'
            ),
            'size' => array(
                'type' => 'DOUBLE'
            ),
            'type' => array(
                'type' => 'VARCHAR',
                'constraint' => 20
            ),
            'is_default' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 0
            ),
            'last_downloaded' => array(
                'type' => 'DATETIME'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('signing_id');
        $this->dbforge->create_table(SIGNING_DOCUMENTS);
        $this->dbforge->add_column(SIGNING_DOCUMENTS,array(
            'CONSTRAINT signing_documents_signing_fk_id FOREIGN KEY(`signing_id`) REFERENCES `'.SIGNINGS.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*                    */
        /* SIGNING FEDEX INFO */ 
        /*                    */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'signing_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'tracking_number_unique_identifier' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => TRUE
            ),
            'location' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'status_description' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'operating_company' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => TRUE
            ),
            'service_type' => array(
                'type' => 'VARCHAR',
                'constraint' => 50,
                'null' => TRUE
            ),
            'service_commit_message' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'packaging' => array(
                'type' => 'VARCHAR',
                'constraint' => 50,
                'null' => TRUE
            ),
            'package_weight' => array(
                'type' => 'VARCHAR',
                'constraint' => 50,
                'null' => TRUE
            ),
            'package_dimensions' => array(
                'type' => 'VARCHAR',
                'constraint' => 50,
                'null' => TRUE
            ),
            'package_count' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            ),
            'shipper_address' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => TRUE
            ),
            'destination_address' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => TRUE
            ),
            'original_location_address' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => TRUE
            ),
            'operating_company' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => TRUE
            ),
            'delivery_attempts' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            ),
            'delivery_signature_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => TRUE
            ),
            'other_identifiers' => array(
                'type' => 'LONGTEXT',
                'null' => TRUE
            ),
            'tracking_history' => array(
                'type' => 'LONGTEXT',
                'null' => TRUE
            ),
            'notification_message' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'date_created' => array(
                'type' => 'DATETIME'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('signing_id');
        $this->dbforge->create_table(SIGNING_FEDEX_INFO);
        $this->dbforge->add_column(SIGNING_FEDEX_INFO,array(
            'CONSTRAINT signing_fedex_info_signing_fk_id FOREIGN KEY(`signing_id`) REFERENCES `'.SIGNINGS.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*                  */
        /* SIGNING INVOICES */ 
        /*                  */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'signing_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'file_icon' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'file_title' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'file_name' => array(
                'type' => 'TEXT'
            ),
            'size' => array(
                'type' => 'DOUBLE'
            ),
            'type' => array(
                'type' => 'VARCHAR',
                'constraint' => 20
            ),
            'last_downloaded' => array(
                'type' => 'DATETIME'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('signing_id');
        $this->dbforge->create_table(SIGNING_INVOICES);
        $this->dbforge->add_column(SIGNING_INVOICES,array(
            'CONSTRAINT signing_invoices_signing_fk_id FOREIGN KEY(`signing_id`) REFERENCES `'.SIGNINGS.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*                */
        /* SIGNING NOTARY */ 
        /*                */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'signing_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'notary_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'status' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 0,
                'comment' => '0 = pending, 1 = confirmed, 2 = declined'
            ),
            'is_automated' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 0,
                'comment' => '1 = if automated notary placement is used'
            ),
            'date_confirmed' => array(
                'type' => 'DATETIME',
                'null' => TRUE
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('signing_id');
        $this->dbforge->add_key('notary_id');
        $this->dbforge->create_table(SIGNING_NOTARY);
        $this->dbforge->add_column(SIGNING_NOTARY,array(
            'CONSTRAINT signing_notary_signing_fk_id FOREIGN KEY(`signing_id`) REFERENCES `'.SIGNINGS.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION',
            'CONSTRAINT signing_notary_notary_fk_id FOREIGN KEY(`notary_id`) REFERENCES `'.NOTARIES.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*                 */
        /* SIGNING CLOSING */ 
        /*                 */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'signing_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'closing_status_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'notes' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'shipment_type_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => 0
            ),
            'tracking_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => TRUE
            ),
            'estimated_delivery_date' => array(
                'type' => 'DATETIME',
                'null' => TRUE
            ),
            'is_delivered' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 0,
                'comment' => '0 = no, 1 = yes'
            ),
            'is_latest' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 1,
                'comment' => '0 = no, 1 = yes'
            ),
            'date_closed' => array(
                'type' => 'DATETIME',
                'null' => TRUE
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('signing_id');
        $this->dbforge->add_key('closing_status_id');
        $this->dbforge->create_table(SIGNING_CLOSING);
        $this->dbforge->add_column(SIGNING_CLOSING,array(
            'CONSTRAINT signing_closing_signing_fk_id FOREIGN KEY(`signing_id`) REFERENCES `'.SIGNINGS.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION',
            'CONSTRAINT signing_closing_closing_status_fk_id FOREIGN KEY(`closing_status_id`) REFERENCES `'.CLOSING_STATUS.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*                  */
        /* SIGNING PAYMENTS */ 
        /*                  */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'signing_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'payment_fee_type_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'description' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'amount' => array(
                'type' => 'DOUBLE',
                'default' => 0
            ),
            'to_be_paid' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 0,
                'comment' => '0 = not considered as payable, 1 = will be included as payable'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('signing_id');
        $this->dbforge->add_key('payment_fee_type_id');
        $this->dbforge->create_table(SIGNING_PAYMENTS);
        $this->dbforge->add_column(SIGNING_PAYMENTS,array(
            'CONSTRAINT signing_payments_signing_fk_id FOREIGN KEY(`signing_id`) REFERENCES `'.SIGNINGS.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION',
            'CONSTRAINT signing_payments_payment_fee_type_fk_id FOREIGN KEY(`payment_fee_type_id`) REFERENCES `'.PAYMENT_FEE_TYPES.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*                  */
        /* SIGNING PAYABLES */ 
        /*                  */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'signing_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'total_amount' => array(
                'type' => 'DOUBLE'
            ),
            'check_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'date_paid' => array(
                'type' => 'DATETIME'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('signing_id');
        $this->dbforge->create_table(SIGNING_PAYABLES);
        $this->dbforge->add_column(SIGNING_PAYABLES,array(
            'CONSTRAINT signing_payables_signing_fk_id FOREIGN KEY(`signing_id`) REFERENCES `'.SIGNINGS.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));

        /*                     */
        /* SIGNING RECEIVABLES */ 
        /*                     */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'signing_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'total_amount' => array(
                'type' => 'DOUBLE'
            ),
            'check_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'date_paid' => array(
                'type' => 'DATETIME'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('signing_id');
        $this->dbforge->create_table(SIGNING_RECEIVABLES);
        $this->dbforge->add_column(SIGNING_RECEIVABLES,array(
            'CONSTRAINT signing_receivables_signing_fk_id FOREIGN KEY(`signing_id`) REFERENCES `'.SIGNINGS.'` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ));


        /*               */
        /* LISTS ADDRESS */ 
        /*               */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'type' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'address' => array(
                'type' => 'TEXT'
            ),
            'city' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'state' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'country' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'zipcode' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'latitude' => array(
                'type' => 'DOUBLE'
            ),
            'longitude' => array(
                'type' => 'DOUBLE'
            ),
            'timezone_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'is_preferred' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 0,
                'comment' => '0 = no, 1 = yes'
            ),
            'is_timezone_update' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 0,
                'comment' => '0 = no, 1 = yes'
            ),
            'user_type' => array(
                'type' => 'VARCHAR',
                'constraint' => 20,
                'default' => 'user',
                'comment' => 'user or notary'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('timezone_id');
        $this->dbforge->create_table(LISTS_ADDRESS);

        /*             */
        /* LISTS EMAIL */ 
        /*             */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'type' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'is_preferred' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 0,
                'comment' => '0 = no, 1 = yes'
            ),
            'user_type' => array(
                'type' => 'VARCHAR',
                'constraint' => 20,
                'default' => 'user',
                'comment' => 'user or notary'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table(LISTS_EMAIL);


        /*                    */
        /* LISTS PHONE NUMBER */ 
        /*                    */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'type' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'phone_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'is_preferred' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 0,
                'comment' => '0 = no, 1 = yes'
            ),
            'user_type' => array(
                'type' => 'VARCHAR',
                'constraint' => 20,
                'default' => 'user',
                'comment' => 'user or notary'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table(LISTS_PHONE_NUMBER);

        /*               */
        /* LISTS WEBSITE */ 
        /*               */
        $this->dbforge->add_field(array(
            'id' => array(
                'type' =>'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'url' => array(
                'type' => 'TEXT'
            ),
            'user_type' => array(
                'type' => 'VARCHAR',
                'constraint' => 20,
                'default' => 'user',
                'comment' => 'user or notary'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table(LISTS_WEBSITE);



        // ADD DEFAULT VALUES



        /*         */
        /* PRODUCT */ 
        /*         */
        $data = array(
            array(
                'name' => 'Closing Package'
            ),
            array(
                'name' => 'Individual Document'
            )
        );
        $this->db->insert_batch(PRODUCTS, $data);

        /*                */
        /* STATUS_DEFAULT */ 
        /*                */
        $data = array(
            array(
                'status' => 'not-clear-to-close',
                'label' => 'Not Clear To Close',
                'color' => '#fa0808',
                'border_color' => '#c80606',
                'importance' => 1
            ),
            array(
                'status' => 'clear-to-close',
                'label' => 'Clear To Close',
                'color' => '#c6f700',
                'border_color' => '#9ec600',
                'importance' => 2
            ),
            array(
                'status' => 'needs-rescheduled',
                'label' => 'Needs Rescheduled',
                'color' => '#ff7a00',
                'border_color' => '#cc6200',
                'importance' => 3
            ),
            array(
                'status' => 'rescheduled',
                'label' => 'Rescheduled',
                'color' => '#f2a205',
                'border_color' => '#c28204',
                'importance' => 4
            ),
            array(
                'status' => 'closing-cancelled',
                'label' => 'Closing Cancelled',
                'color' => '#cf9f26',
                'border_color' => '#a67f1e',
                'importance' => 5
            ),
            array(
                'status' => 'borrower-cancelled',
                'label' => 'Borrower Cancelled',
                'color' => '#f7577e',
                'border_color' => '#c64665',
                'importance' => 6
            ),
            array(
                'status' => 'docs-drawn',
                'label' => 'Docs Drawn',
                'color' => '#2abede',
                'border_color' => '#2abede',
                'importance' => 7
            ),
            array(
                'status' => 'partial-close',
                'label' => 'Partial Close',
                'color' => '#420f94',
                'border_color' => '#350c76',
                'importance' => 8
            ),
            array(
                'status' => 'declined',
                'label' => 'Declined',
                'color' => '#8f070f',
                'border_color' => '#72060c',
                'importance' => 9
            ),
            array(
                'status' => 'error',
                'label' => 'Error',
                'color' => '#faf11c',
                'border_color' => '#c8c116',
                'importance' => 10
            )
        );
        $this->db->insert_batch(STATUS_DEFAULT, $data);

        /*                  */
        /* FOLDERS DEFAULT  */ 
        /*                  */
        $data = array(
            array(
                'name' => 'Scheduling'
            ),
            array(
                'name' => 'Back Proc'
            ),
            array(
                'name' => 'Underwriting'
            ),
            array(
                'name' => 'Processing'
            ),
            array(
                'name' => 'Closing Prep'
            ),
            array(
                'name' => 'Closing'
            ),
            array(
                'name' => 'Back to U/W'
            ),
        );
        $this->db->insert_batch(FOLDERS_DEFAULT, $data);

        /*              */
        /* USER_GROUPS  */ 
        /*              */
        $data = array(
            array(
                'name' => 'admin',
                'description' => 'Admin',
            ),
            array(
                'name' => 'scheduler',
                'description' => 'Scheduler',
            ),
            array(
                'name' => 'title-admin',
                'description' => 'Title Admin',
            ),
            array(
                'name' => 'lender-admin',
                'description' => 'Lender Admin',
            ),
            array(
                'name' => 'loan-officer',
                'description' => 'Loan Officer',
            ),
            array(
                'name' => 'processor',
                'description' => 'Processor',
            ),
            array(
                'name' => 'underwriter',
                'description' => 'Underwriter',
            ),
            array(
                'name' => 'guest',
                'description' => 'Guest',
            )
        );
        $this->db->insert_batch(USER_GROUPS, $data);

        /*                       */
        /* NOTARY DOCUMENT TYPES */ 
        /*                       */
        $data = array(
            array(
                'type' => 'Commission'
            ),
            array(
                'type' => 'W9 Form'
            ),
            array(
                'type' => 'Background Check'
            ),
            array(
                'type' => 'Surety Bond'
            ),
            array(
                'type' => 'Notary Seller Certificate'
            ),
            array(
                'type' => 'Drivers License'
            ),
            array(
                'type' => 'Others'
            )
        );
        $this->db->insert_batch(NOTARY_DOCUMENT_TYPES, $data);

        /*                           */
        /* SIGNING DOCUMENTS DEFAULT */ 
        /*                           */
        $data = array(
            'file_icon' => 'fa-file-word-o',
            'file_title' => 'Signing Instructions.docx',
            'file_name' => '59b5705c0a4cf890b75954b4766446eaf331f695.pdf',
            'size' => 22380,
            'type' => 'application/vnd.open'
        );
        $this->db->insert(SIGNING_DOCUMENTS_DEFAULT, $data);

        /*                */
        /* SHIPMENT TYPES */ 
        /*                */
        $data = array(
            array(
                'type' => 'FedEx'
            ),
            array(
                'type' => 'UPS'
            )
        );
        $this->db->insert_batch(SHIPMENT_TYPES, $data);

        /*                   */
        /* PAYMENT FEE TYPES */ 
        /*                   */
        $data = array(
            array(
                'name' => 'Billing Amount'
            ),
            array(
                'name' => 'Billing Miscellaneous'
            ),
            array(
                'name' => 'Miscellaneous Fee'
            ),
            array(
                'name' => 'Signing Fee'
            )
        );
        $this->db->insert_batch(PAYMENT_FEE_TYPES, $data);

        /*                */
        /* SIGNING STATUS */ 
        /*                */
        $data = array(
            array(
                'name' => 'New'
            ),
            array(
                'name' => 'Schedule Notary'
            ),
            array(
                'name' => 'Waiting on Docs'
            ),
            array(
                'name' => 'Waiting on Closing'
            ),
            array(
                'name' => 'Reschedule'
            ),
            array(
                'name' => 'Closed'
            ),
            array(
                'name' => 'Cancelled'
            ),
            array(
                'name' => 'Signing Complete'
            )
        );
        $this->db->insert_batch(SIGNING_STATUS, $data);

        /*          */
        /* TIMEZONE */ 
        /*          */
        $data = array(
            array(
                'name' => 'PST - Pacific Standard Time',
                'country_code' => 'US',
                'country_name' => 'United States',
                'time_start' => 1469531970,
                'gmt_offset' => -25200,
                'dst' => 0
            ),
            array(
                'name' => 'CST - Central Standard Time',
                'country_code' => 'US',
                'country_name' => 'United States',
                'time_start' => 1469539170,
                'gmt_offset' => -18000,
                'dst' => 0
            ),
            array(
                'name' => 'EST - Eastern Standard Time',
                'country_code' => 'US',
                'country_name' => 'United States',
                'time_start' => 1469542770,
                'gmt_offset' => -14400,
                'dst' => 0
            ),
            array(
                'name' => 'MST - Mountain Standard Time',
                'country_code' => 'US',
                'country_name' => 'United States',
                'time_start' => 1469535570,
                'gmt_offset' => -21600,
                'dst' => 1
            ),
            array(
                'name' => 'MDT - Mountain Daylight Time',
                'country_code' => 'US',
                'country_name' => 'United States',
                'time_start' => 1469535570,
                'gmt_offset' => -21600,
                'dst' => 0
            ),
            array(
                'name' => 'AKST - Alaska Standard Time',
                'country_code' => 'US',
                'country_name' => 'United States',
                'time_start' => 1469528370,
                'gmt_offset' => -28800,
                'dst' => 0
            ),
            array(
                'name' => 'HAST - Hawaii-Aleutian Standard Time',
                'country_code' => 'US',
                'country_name' => 'United States',
                'time_start' => 1469524770,
                'gmt_offset' => -32400,
                'dst' => 1
            ),
            array(
                'name' => 'HADT - Hawaii-Aleutian Daylight Time',
                'country_code' => 'US',
                'country_name' => 'United States',
                'time_start' => 1469521170,
                'gmt_offset' => -36000,
                'dst' => 0
            ),
            array(
                'name' => 'PDT - Pacific Daylight Time',
                'country_code' => 'US',
                'country_name' => 'United States',
                'time_start' => 1469531970,
                'gmt_offset' => -25200,
                'dst' => 0
            ),
            array(
                'name' => 'AKDT - Alaska Daylight Time',
                'country_code' => 'US',
                'country_name' => 'United States',
                'time_start' => 1469532936,
                'gmt_offset' => -28800,
                'dst' => 0
            ),
            array(
                'name' => 'CDT - Central Daylight Time',
                'country_code' => 'US',
                'country_name' => 'United States',
                'time_start' => 1469539170,
                'gmt_offset' => -18000,
                'dst' => 0
            ),
            array(
                'name' => 'EDT - Eastern Daylight Time',
                'country_code' => 'US',
                'country_name' => 'United States',
                'time_start' => 1469542770,
                'gmt_offset' => -14400,
                'dst' => 0
            )
        );
        $this->db->insert_batch(TIMEZONE, $data);

        /*       */
        /* USERS */ 
        /*       */
        $data = array(
           'username' => 'admin',
           'password' => '$2y$10$GZzNBYjRxb.5TkFo26NzeeaFiGwF6vmPwp.dfu7N1X9KN31u0bZwi',
           'first_name' => 'Notary',
           'last_name' => 'Admin',
           'description' => 'A small mind is obstinate. A great mind can lead and be led - Alexander Cannon',
           'status' => 1,
           'date_added' => date('Y-m-d H:i:s')
        );
        $this->db->insert(USERS, $data);


        /*                      */
        /* USER GROUP REFERENCE */ 
        /*                      */
        $data = array(
           'user_id' => 1,
           'user_group_id' => 1,
           'date_added' => date('Y-m-d H:i:s')
        );
        $this->db->insert(USER_GROUP_REFERENCE, $data);

        /*             */
        /* LISTS EMAIL */ 
        /*             */
        $data = array(
           'user_id' => 1,
           'type' => 'Personal Email',
           'email' => 'brandon@milestonesettlement.com',
           'is_preferred' => 1,
           'date_added' => date('Y-m-d H:i:s')
        );
        $this->db->insert(LISTS_EMAIL, $data);

    }

    public function down()
    {
        $this->dbforge->drop_table(PRODUCTS);
        $this->dbforge->drop_table(BORROWERS);
        $this->dbforge->drop_table(COMPANY);
        $this->dbforge->drop_table(NOTARYCLOUD_INFO);
        $this->dbforge->drop_table(CLOSING_STATUS);
        $this->dbforge->drop_table(STATUS_DEFAULT);
        $this->dbforge->drop_table(FOLDERS_DEFAULT);
        $this->dbforge->drop_table(USER_GROUPS);
        $this->dbforge->drop_table(NOTARY_DOCUMENT_TYPES);
        $this->dbforge->drop_table(SIGNING_DOCUMENTS_DEFAULT);
        $this->dbforge->drop_table(SHIPMENT_TYPES);
        $this->dbforge->drop_table(PAYMENT_FEE_TYPES);
        $this->dbforge->drop_table(SIGNING_STATUS);
        $this->dbforge->drop_table(TIMEZONE);

        $this->dbforge->drop_table(USERS);
        $this->dbforge->drop_table(NOTARIES);
        $this->dbforge->drop_table(NOTARY_DOCUMENTS);
        $this->dbforge->drop_table(NOTARY_FAVORITED);
        $this->dbforge->drop_table(NOTARY_NOTES);
        $this->dbforge->drop_table(NOTARY_REVIEWS);
        $this->dbforge->drop_table(STATUS_CUSTOM);
        $this->dbforge->drop_table(FOLDERS_CUSTOM);
        $this->dbforge->drop_table(USER_GROUP_REFERENCE); 
        $this->dbforge->drop_table(COMPANY_USERS);
        $this->dbforge->drop_table(SIGNINGS);
        $this->dbforge->drop_table(SIGNING_DOCUMENTS);
        $this->dbforge->drop_table(SIGNING_FEDEX_INFO);
        $this->dbforge->drop_table(SIGNING_INVOICES);
        $this->dbforge->drop_table(SIGNING_NOTARY);
        $this->dbforge->drop_table(SIGNING_CLOSING);
        $this->dbforge->drop_table(SIGNING_PAYMENTS);
        $this->dbforge->drop_table(SIGNING_PAYABLES);
        $this->dbforge->drop_table(SIGNING_RECEIVABLES);

        $this->dbforge->drop_table(LISTS_ADDRESS);
        $this->dbforge->drop_table(LISTS_EMAIL);
        $this->dbforge->drop_table(LISTS_PHONE_NUMBER);
        $this->dbforge->drop_table(LISTS_WEBSITE);
    }

}