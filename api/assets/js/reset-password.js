$(document).ready(function(){
	$('#reset-password-form').ajaxForm({
		dataType: 'json',
		beforeSend:function(){
			$('.change-btn').html("Changing Password ...");
			$('.change-btn').prop("disabled",true);
		},
		success:function(response){
			$('.change-btn').html('Change Password <i class="fa fa-arrow-right"></i>');
			$('.change-btn').prop("disabled",false);
			if(response.error==0)
			{
				$("#reset-password-form").find(".message").html("<div class='alert alert-success alert-dismissable text-left'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+response.message+"</div>");

				setTimeout(function(){
					location.href = base_url+"?action=login";
				},4000);
			}
			else
			{
				$("#reset-password-form").find("input[type='new_password']").val("");
				$("#reset-password-form").find("input[type='confirm_new_password']").val("");
				$("#reset-password-form").find(".message").html("<div class='alert alert-danger alert-dismissable text-left'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+response.message+"</div>");
			}
			setTimeout(function(){
				$("#reset-password-form").find(".message").find('.alert').fadeOut("fast");
				$(this).remove();
			},3000);
		}
	});
});